
#include <cassert>
#include <iostream>
#include <unordered_set>

#include <ASTNodes.h>
#include <SemanticAnalysis.h>
#include <SemanticException.h>
#include <StringConstants.h>

using namespace lcpl;

SemanticAnalysis::SemanticAnalysis(Program *p)
    : program(p), typeTable(p), symbolTable() {
  assert(program && "Expected non-null program");
}

void SemanticAnalysis::runAnalysis() {
  assert(program && "Expected non-null program");
  visit(program);
}

bool SemanticAnalysis::visit(Program *p) {
  checkMainClassAndMethod();
  checkInheritanceGraph();

  std::unordered_set<Class *> processed;
  for (auto c : *p) {
    if (!processed.emplace(c).second) {
      throw DuplicateClassException(c);
    }

    if (!visit(c)) {
      return false;
    }
  }

  return true;
}

bool SemanticAnalysis::visit(Class *c) {
  if (typeTable.isBuiltinClass(c)) {
    return true;
  }

  // create a scope and add all attributes to it
  SymbolTable::Scope classScope(symbolTable);
  for (auto currentClass = c; currentClass;
       currentClass = typeTable.getParentClass(currentClass)) {
    for (auto f : *currentClass) {
      if (f->isAttribute()) {
        auto attr = static_cast<Attribute *>(f);
        symbolTable.insert(attr);

        auto attrType = typeTable.getType(attr->getType());
        typeTable.setType(attr, attrType);
      }
    }
  }

  // add self to symbol table
  std::unique_ptr<LocalDefinition> self(
      new LocalDefinition(0, "self", c->getName()));
  symbolTable.insert(self.get());
  typeTable.setType(self.get(), typeTable.getType(c));

  checkFeatures(c);

  return ASTVisitor::visit(c);
}

void SemanticAnalysis::checkFeatures(Class *c) {
  for (auto f : *c) {
    if (f->isAttribute()) {
      auto attr = static_cast<Attribute *>(f);
      if (typeTable.getAttribute(typeTable.getParentClass(c), f->getName())) {
        throw DuplicateAttrException(attr, c);
      }
    } else {
      assert(f->isMethod() && "Unknown feature kind");
      auto method = static_cast<Method *>(f);
      auto hidden =
          typeTable.getMethod(typeTable.getParentClass(c), f->getName());

      if (hidden) {
        // check that the signatures match
        if (method->getReturnType() != hidden->getReturnType()) {
          throw InvalidMethodSignatureException(c, method);
        }

        auto methodB = method->begin(), methodE = method->end();
        auto hiddenB = hidden->begin(), hiddenE = hidden->end();

        for (; methodB != methodE && hiddenB != hiddenE; ++methodB, ++hiddenB) {
          auto paramType = typeTable.getType((*methodB)->getType());
          auto hiddenParamType = typeTable.getType((*hiddenB)->getType());

          if (paramType != hiddenParamType) {
            throw InvalidMethodSignatureException(c, method);
          }
        }

        if (methodB != methodE || hiddenB != hiddenE) {
          throw InvalidMethodSignatureException(c, method);
        }
      }
    }
  }
}

bool SemanticAnalysis::visit(Feature *f) { return ASTVisitor::visit(f); }

bool SemanticAnalysis::visit(Attribute *a) {
  auto attrType = typeTable.getType(a->getType());

  auto init = a->getInit();
  if (!init) {
    return true;
  }

  if (!visit(init)) {
    return false;
  }

  // TODO: Check type compatibility, throw WrongTypeException if anything wrong

  return true;
}

void SemanticAnalysis::checkMainClassAndMethod() {
  auto classIterator = std::find_if(
    program->begin(),
    program->end(),
    [](Class *c) { return c->getName() == "Main"; }
  );
  if (classIterator == program->end())
    throw TypeNotFoundException("Main");

  auto mainClass = *classIterator;
  while (!typeTable.isBuiltinClass(mainClass)) {
    auto mainFunc = std::find_if(
      mainClass->begin(),
      mainClass->end(),
      [](Feature *f) { return f->isMethod() && f->getName() == "main"; }
    );
    if (mainFunc != mainClass->end()) {
      auto retType = typeTable.getType(static_cast<Method *>(*mainFunc)->getReturnType());
      if (retType != typeTable.getVoidType()) {
        throw WrongTypeException(retType, typeTable.getVoidType(), *mainFunc);
      }
      return;
    }
    mainClass = typeTable.getParentClass(mainClass);
  }

  throw MethodNotFoundException("main", mainClass);
}

void SemanticAnalysis::checkInheritanceGraph() {
  std::unordered_set<Class *> visited;
  for (auto c : *program) {
    visited.clear();

    auto iter = c;
    while (!typeTable.isBuiltinClass(iter)) {
      auto parent = typeTable.getParentClass(iter);
      auto isBuiltinClass = typeTable.isBuiltinClass(parent);

      auto parentType = typeTable.getType(parent->getName());
      auto isBuiltinType = typeTable.isBuiltinType(parentType);

      if (parentType == typeTable.getStringType()) {
        throw BadInheritanceException(iter, parent->getName());
      }
      if (isBuiltinType && !isBuiltinClass) {
        throw BadInheritanceException(iter, parent->getName());
      }
      if (!visited.insert(parent).second) {
        throw BadInheritanceException(iter, parent->getName());
      }
      iter = parent;
    }
  }
}

bool SemanticAnalysis::visit(Method *m) {
  std::unordered_set<std::string> paramNames;

  SymbolTable::Scope methodScope(symbolTable);
  for (auto param : *m) {
    auto res = paramNames.insert(param->getName());
    if (!res.second)
      throw DuplicateParamException(param, m);

    if (!visit(param)) {
      return false;
    }
  }

  auto body = m->getBody();
  if (body) {
    if (!visit(body)) {
      return false;
    }

    // TODO: check for return type compatibility and throw WrongTypeException
    auto bodyType = typeTable.getType(body);
    auto retType = typeTable.getType(m->getReturnType());
    if (!typeTable.isEqualOrImplicitlyConvertibleTo(bodyType, retType)) {
      throw WrongTypeException(bodyType, retType, body);
    }
  }

  return true;
}

bool SemanticAnalysis::visit(FormalParam *param) {
  symbolTable.insert(param);
  typeTable.setType(param, typeTable.getType(param->getType()));
  return true;
}

bool SemanticAnalysis::visit(Expression *e) { return ASTVisitor::visit(e); }

bool SemanticAnalysis::visit(IntConstant *ic) {
  typeTable.setType(ic, typeTable.getIntType());
  return true;
}

bool SemanticAnalysis::visit(StringConstant *sc) {
  typeTable.setType(sc, typeTable.getStringType());
  return true;
}

bool SemanticAnalysis::visit(NullConstant *nc) {
  typeTable.setType(nc, typeTable.getNullType());
  return true;
}

bool SemanticAnalysis::visit(Symbol *s) {
  auto who = symbolTable.lookup(s->getName());
  typeTable.setType(s, typeTable.getType(who));

  return true;
}

bool SemanticAnalysis::visit(Block *b) {
  SymbolTable::Scope blockScope(symbolTable);

  // visit all expressions first
  if (!ASTVisitor::visit(b)) {
    return false;
  }

  // set the type to match the type of the last expression
  if (b->begin() != b->end()) {
    typeTable.setType(b, typeTable.getType(b->back()));
  } else {
    typeTable.setType(b, typeTable.getVoidType());
  }

  return true;
}

bool SemanticAnalysis::visit(Assignment *a) {
  auto rhs = a->getExpression();

  if (!rhs) {
    throw MissingOperandException(a);
  }

  if (!visit(rhs)) {
    return false;
  }

  // TODO: the setType is wrong! Also throw IncompatibleOperandsException if the operands don't match
  auto lhs = symbolTable.lookup(a->getSymbol());
  auto rhsType = typeTable.getType(rhs);
  auto lhsType = typeTable.getType(lhs);
  if (!typeTable.isEqualOrImplicitlyConvertibleTo(rhsType, lhsType))
    throw IncompatibleOperandsException(a, lhsType, rhsType);

  typeTable.setType(a, typeTable.getCommonType(lhsType, rhsType));

  return true;
}

bool SemanticAnalysis::visit(BinaryOperator *bo) {
  auto lhs = bo->getLHS();
  auto rhs = bo->getRHS();

  if (!lhs || !rhs) {
    throw MissingOperandException(bo);
  }

  if (!visit(lhs) || !visit(rhs)) {
    return false;
  }

  auto lhsType = typeTable.getType(lhs);
  auto rhsType = typeTable.getType(rhs);
  Type *retType = nullptr;

  if (bo->getOperatorKind() == BinaryOperator::BinOpKind::Add) {
    if ((lhsType == typeTable.getIntType() && rhsType == typeTable.getStringType()) ||
        (rhsType == typeTable.getIntType() && lhsType == typeTable.getStringType()) ||
        (lhsType == typeTable.getStringType() && rhsType == typeTable.getStringType())) {
      retType = typeTable.getStringType();
    }
  }

  if (bo->getOperatorKind() == BinaryOperator::BinOpKind::Equal) {
    if (!typeTable.isEqualOrImplicitlyConvertibleTo(lhsType, rhsType) &&
        !typeTable.isEqualOrImplicitlyConvertibleTo(rhsType, lhsType)) {
      throw WrongTypeException(lhsType, rhsType, lhs);
    }
    retType = typeTable.getIntType();
  }

  if (!retType) {
    if (lhsType != typeTable.getIntType())
      throw WrongTypeException(lhsType, typeTable.getIntType(), lhs);
    if (rhsType != typeTable.getIntType())
      throw WrongTypeException(rhsType, typeTable.getIntType(), rhs);
    retType = typeTable.getIntType();
  }

  // TODO: This is wrong! check for type compatiblity and set the right type!
  // throw WrongTypeException or IncompatibleOperandsException if anything wrong
  typeTable.setType(bo, retType);

  return true;
}

bool SemanticAnalysis::visit(UnaryOperator *uo) {
  auto operand = uo->getOperand();

  if (!operand) {
    throw MissingOperandException(uo);
  }

  assert(uo->getOperatorKind() != UnaryOperator::Invalid &&
         "Can't have invalid unary operators");

  if (!visit(operand)) {
    return false;
  }

  if (typeTable.getType(operand) != typeTable.getIntType()) {
    throw WrongTypeException(typeTable.getType(operand), typeTable.getIntType(),
                             uo);
  }

  typeTable.setType(uo, typeTable.getIntType());

  return true;
}

bool SemanticAnalysis::visit(Cast *c) {
  auto toCast = c->getExpressionToCast();

  if (!toCast) {
    throw MissingOperandException(c);
  }

  if (!visit(toCast)) {
    return false;
  }

  typeTable.setType(c, typeTable.getType(c->getType()));

  return true;
}

bool SemanticAnalysis::visit(Substring *s) {
  auto str = s->getString();
  auto start = s->getStart();
  auto end = s->getEnd();

  if (!str || !start || !end) {
    throw MissingOperandException(s);
  }

  if (!visit(str) || !visit(start) || !visit(end)) {
    return false;
  }

  if (typeTable.getType(str) != typeTable.getStringType()) {
    throw WrongTypeException(typeTable.getType(str), typeTable.getStringType(),
                             s);
  }

  if (typeTable.getType(start) != typeTable.getIntType()) {
    throw WrongTypeException(typeTable.getType(start), typeTable.getIntType(),
                             s);
  }

  if (typeTable.getType(end) != typeTable.getIntType()) {
    throw WrongTypeException(typeTable.getType(end), typeTable.getIntType(), s);
  }

  typeTable.setType(s, typeTable.getStringType());

  return true;
}

bool SemanticAnalysis::visit(Dispatch *d) {
  auto obj = d->getObject();

  if (obj) {
    if (!visit(obj)) {
      return false;
    }
  } else {
    auto self = symbolTable.lookup("self");
    obj = dynamic_cast<Expression *>(self);
    assert(obj && "Invalid lookup");
  }

  auto objType = typeTable.getType(obj);
  auto objClass = objType->getClass();
  if (!objClass) {
    throw DispatchOnInvalidObjException(d->getName(), objType);
  }

  auto method = typeTable.getMethod(objClass, d->getName());
  if (!method) {
    throw MethodNotFoundException(d->getName(), objClass);
  }

  // TODO: detect the type that should be returned by the dispatch
  // check the number and type of the arguments
  // throw TooManyArgsException, NotEnoughArgsException
  // or WrongTypeException according to the error type

  auto paramIt = method->begin();

  for (auto arg : *d) {
    if (paramIt == method->end()) {
      throw TooManyArgsException(d->getName(), d);
    }
    if (!visit(arg)) {
      return false;
    }
    auto argType = typeTable.getType(arg);
    auto paramType = typeTable.getType((*paramIt)->getType());
    if (!typeTable.isEqualOrImplicitlyConvertibleTo(argType, paramType)) {
      throw WrongTypeException(argType, paramType, arg);
    }
    ++paramIt;
  }
  if (paramIt != method->end()) {
    throw NotEnoughArgsException(d->getName(), d);
  }

  typeTable.setType(d, typeTable.getType(method->getReturnType()));

  return true;
}

bool SemanticAnalysis::visit(StaticDispatch *d) {
  auto targetType = typeTable.getType(d->getType());
  if (!targetType)
    throw TypeNotFoundException(d->getType());

  auto obj = d->getObject();

  if (obj) {
    if (!visit(obj)) {
      return false;
    }
  } else {
    auto self = symbolTable.lookup("self");
    obj = dynamic_cast<Expression *>(self);
    assert(obj && "Invalid lookup");
  }

  auto objType = typeTable.getType(obj);
  auto objClass = objType->getClass();
  if (!objClass) {
    throw DispatchOnInvalidObjException(d->getName(), objType);
  }
  if (!typeTable.isEqualOrImplicitlyConvertibleTo(objType, targetType)) {
    throw DispatchOnInvalidObjException(d->getName(), targetType);
  }

  // TODO:
  // detect which method should be called
  // visit all parameters of that methos
  // detect the type that should be returned by the dispatch
  // throw DispatchOnInvalidObjException , MethodNotFoundException, TooManyArgsException, NotEnoughArgsException
  // or WrongTypeException according to the error type
  auto method = typeTable.getMethod(objClass, d->getName());
  if (!method) {
    throw MethodNotFoundException(d->getName(), objClass);
  }

  typeTable.setType(d, typeTable.getType(method->getReturnType()));

  return true;
}

bool SemanticAnalysis::visit(NewObject *n) {
  auto type = typeTable.getType(n->getType());

  if (!type->getClass()) {
    throw WrongTypeException(type, n);
  }

  typeTable.setType(n, type);
  return true;
}

bool SemanticAnalysis::visit(IfStatement *i) {
  auto condExpr = i->getCond();
  auto thenExpr = i->getThen();
  auto elseExpr = i->getElse();
  Type *retType = nullptr;

  // TODO: Create new symbol table scopes, check type compatibility, return the right type
  // throw WrongTypeException if anything wrong

  if (!condExpr) {
    throw MissingIfCondException(i);
  }
  if (!visit(condExpr)) {
    return false;
  }

  if (thenExpr) {
    SymbolTable::Scope thenScope(symbolTable);
    if (!visit(thenExpr)) {
      return false;
    }
  } else {
    throw MissingIfThenException(i);
  }

  if (elseExpr) {
    SymbolTable::Scope elseScope(symbolTable);
    if (!visit(elseExpr)) {
      return false;
    }
  } else {
    retType = typeTable.getVoidType();
  }

  if (!retType) {
    auto thenType = typeTable.getType(thenExpr);
    auto elseType = typeTable.getType(elseExpr);
    retType = typeTable.getCommonType(thenType, elseType);
  }
  typeTable.setType(i, retType);

  return true;
}

bool SemanticAnalysis::visit(WhileStatement *w) {
  auto cond = w->getCond();

  if (!cond) {
    throw MissingWhileCondException(w);
  }

  if (!visit(cond)) {
    return false;
  }

  if (typeTable.getType(cond) != typeTable.getIntType()) {
    throw WrongTypeException(typeTable.getType(cond), typeTable.getIntType(),
                             cond);
  }

  auto body = w->getBody();
  if (body) {
    SymbolTable::Scope whileScope(symbolTable);
    if (!visit(body)) {
      return false;
    }
  }

  typeTable.setType(w, typeTable.getVoidType());

  return true;
}

bool SemanticAnalysis::visit(LocalDefinition *local) {
  symbolTable.insert(local);
  typeTable.setType(local, typeTable.getType(local->getType()));

  if (local->getInit()) {
    if (!visit(local->getInit())) {
      return false;
    }
  }

  return true;
}
