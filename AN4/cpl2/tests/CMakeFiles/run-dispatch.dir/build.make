# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/user/cpl/cpl-reference/lcpl-semant

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/user/cpl/cpl-reference/lcpl-semant

# Utility rule file for run-dispatch.

# Include the progress variables for this target.
include tests/CMakeFiles/run-dispatch.dir/progress.make

tests/CMakeFiles/run-dispatch: tests/test-error-free
	cd /home/user/cpl/cpl-reference/lcpl-semant/tests && ./test-error-free /home/user/cpl/cpl-reference/lcpl-semant/tests/simple/dispatch.lcpl.json

run-dispatch: tests/CMakeFiles/run-dispatch
run-dispatch: tests/CMakeFiles/run-dispatch.dir/build.make
.PHONY : run-dispatch

# Rule to build all files generated by this target.
tests/CMakeFiles/run-dispatch.dir/build: run-dispatch
.PHONY : tests/CMakeFiles/run-dispatch.dir/build

tests/CMakeFiles/run-dispatch.dir/clean:
	cd /home/user/cpl/cpl-reference/lcpl-semant/tests && $(CMAKE_COMMAND) -P CMakeFiles/run-dispatch.dir/cmake_clean.cmake
.PHONY : tests/CMakeFiles/run-dispatch.dir/clean

tests/CMakeFiles/run-dispatch.dir/depend:
	cd /home/user/cpl/cpl-reference/lcpl-semant && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/user/cpl/cpl-reference/lcpl-semant /home/user/cpl/cpl-reference/lcpl-semant/tests /home/user/cpl/cpl-reference/lcpl-semant /home/user/cpl/cpl-reference/lcpl-semant/tests /home/user/cpl/cpl-reference/lcpl-semant/tests/CMakeFiles/run-dispatch.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : tests/CMakeFiles/run-dispatch.dir/depend

