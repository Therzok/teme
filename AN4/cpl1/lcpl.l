%{

#include "lcpl.tab.h"
#include <ASTNodes.h>
#include <string>

static void countColumns();

static int gColumnNumber = 1;

#define YY_USER_ACTION { \
	yylloc.first_line = yylloc.last_line = yylineno; \
	yylloc.first_column = gColumnNumber; \
	yylloc.last_column = gColumnNumber+yyleng; \
}

#define WRAP_SYM(symbol) { \
	countColumns(); \
	return symbol; \
}

#define WRAP_OP(binOp, symbol) { \
	yylval.binOpKind = binOp; \
	WRAP_SYM(symbol); \
}

%}

%option yylineno nounput

%%

	/* Keywords */
"class"		WRAP_SYM(KW_CLASS);
"inherits"	WRAP_SYM(KW_INHERITS);
"end" 		WRAP_SYM(KW_END);
"var"		WRAP_SYM(KW_VAR);
"local"		WRAP_SYM(KW_LOCAL);
"null"		WRAP_SYM(KW_NULL);
"new"		WRAP_SYM(KW_NEW);
"if"		WRAP_SYM(KW_IF);
"then"		WRAP_SYM(KW_THEN);
"else"		WRAP_SYM(KW_ELSE);
"while"		WRAP_SYM(KW_WHILE);
"loop"		WRAP_SYM(KW_LOOP);

	/* Operators */
"<"			WRAP_OP(lcpl::BinaryOperator::BinOpKind::LessThan, LT);
"<="		WRAP_OP(lcpl::BinaryOperator::BinOpKind::LessThanEqual, LTE);
"=="		WRAP_OP(lcpl::BinaryOperator::BinOpKind::Equal, EQ);
"+"			WRAP_OP(lcpl::BinaryOperator::BinOpKind::Add, ADD);
"-"			WRAP_OP(lcpl::BinaryOperator::BinOpKind::Sub, SUB);
"*"			WRAP_OP(lcpl::BinaryOperator::BinOpKind::Mul, MUL);
"/"			WRAP_OP(lcpl::BinaryOperator::BinOpKind::Div, DIV);
"!"			WRAP_SYM(NOT);
"->"		WRAP_SYM(RET);
"::"		WRAP_SYM(SD);

	/* Comments */
"#"[^\r\n]*	{}

	/* Constants */
\"(\\.|[^\\"])*\" {
	/* Trim leading and trailing quotes */
	yylval.stringValue = new std::string(yytext + 1, strlen(yytext + 1) - 1);
	WRAP_SYM(STRING_CONSTANT);
}

[0]|([1-9][0-9]*) {
	yylval.intValue = atoi(yytext);
	WRAP_SYM(INTEGER_CONSTANT);
}

"self" {
	yylval.stringValue = new std::string(yytext);
	WRAP_SYM(KW_SELF);
}

	/* Identifiers */
[a-zA-Z]([0-9a-zA-Z_])* {
	yylval.stringValue = new std::string(yytext);
	WRAP_SYM(IDENTIFIER);
}

	/* Whitespace */
[ \t\r\n] {
	countColumns();
}

	/* Anything else */
.	WRAP_SYM(yytext[0]);

%%

int yywrap(void)
{
	return 1;
}

static void countColumns()
{
	for (int i = 0; yytext[i]; i++) {
		if (yytext[i] == '\n') {
			gColumnNumber = 1;
		} else if (yytext[i] == '\t') {
			gColumnNumber += 8 - (gColumnNumber % 8);
		} else {
			gColumnNumber ++;
		}
	}
}
