Tema 1 - Analiza Sintactica
Ungureanu Marius Stefan 343C1
=======================

Tema a cerut implementarea unei parser ce va analiza sintactic limbajul LCPL si
generarea unui AST folosind codul in directorul lcpl-AST.

Au fost implementate toate partile cerute de tema, trecand toate testele.

lcpl.l:
-------

Am definit doua macrodefinitii pentru lizibilitatea codului in lexer.

WRAP_SYM, ce numara coloanele si intoarce valoarea data ca parametru pentru
parser.

WRAP_OP, ce numara coloanele, seteaza tipul operatiei si intoarce valoarea data
ca parametru pentru parser.

In lexer sunt tratate keyword-urile, operatorii, apoi constructiile din expresii
regulate.

Self este tratat ca un keyword pentru a putea rezolva diferentele pentru nodurile
dispatch si assignment in AST.

Singura expresie regulata complicata este cea pentru sirurile constante, ce se
poate explica in cuvinte ca: orice caractere intre doua perechi de ghilimele ce
este ori un caracter escapat, ori nu este o pereche de ghilimele escapata de 0
sau mai multe ori.

lcpl.y:
-------

Am introdus doua variabile noi, emptyString si symbolVoid pentru a le putea
duplica in momentul in care avem nevoie de ele.

Am adaugat o functie de pretty_print_error() ce va afisa pe ecran contextul
in care apare eroarea:

../stack.lcpl:10:18 syntax error
         [hello[3, 5].length];
                 ^

Am introdus un nod de tip binOpKind in uniune pentru a putea scrie mai usor
regulile gramaticii in cazul operatiilor binare.

Am folosit un stil de a prefixa tokenii posibili "maybe_" unde match-ul pe
un continut valid aloca un nou pointer, iar %empty va seta match-ul pe nullptr.

Am abuzat de faptul ca $$ va fi mereu setat la ultima valoare in parser,
deci s-a putut sterge mult cod duplicat.

Regulile sunt foarte usor de citit, am introdus doua reguli noi pentru
lizibilitate, 'name' si 'type' pentru claritatea definitiei lor.

lcpl-AST:
---------

* In commit-ul 239b0778f2068a15e9ab5f2c8e035d3808187890, am adaugat std::move
unde lipsea.

* Am setat constructorul NullConstant sa fie explicit.

Implementation details:
-----------------------

Ambiguitatea intr-un caz de genul:
 * [hello[3, 5].length] este rezolvata folosind precedenta mai mare pentru
 identificator, astfel rezolutia este [IDENTIFIER args].

Notes:
 * Bison 3.0.4, Flex 2.5.39.
 * Tema este leak free.
 * Coding style: tab de 4 spatii, 0 erori cppcheck in cod personal.
 * Changelog-ul tuturor modificarilor este atasat in fisierul changelog
(git log -p . > changelog).
 * Pentru testul compiler.lcpl, a fost nevoie de inserarea unui nou block gol
 in ramura 'then' a nodului 'if'.
