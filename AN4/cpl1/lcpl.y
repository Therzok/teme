%code requires {
	#include <cstdlib>
	#include <fstream>
	#include <iostream>
	#include <limits>
	#include <string>
	#include <vector>

	#include <ASTNodes.h>
}

%code {
	#include <ASTSerialization.h>

	// Used for duplicating.
	const auto emptyString = std::string();
	const auto symbolVoid = std::string("Void");

	const char *gInputFileName = NULL;

	extern FILE *yyin;

	static lcpl::Program *gLcplProgram;

	int  yylex_destroy();
	int  yylex();

	/* Pretty prints the line on which the error occured
	 * With ^ characters below the tokens which caused the error.
	 */
	static void pretty_print_error()
	{
		std::ifstream file(gInputFileName);
		/* Start from 1, skip the line we want to get. */
		for (int i = 1; i < yylloc.first_line; ++i)
			file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		std::string line;
		std::getline(file, line);
		std::cout
			<< line
			<< std::endl
			<< std::string(yylloc.first_column - 1, ' ')
			<< std::string(yylloc.last_column - yylloc.first_column, '^')
			<< std::endl;
	}

	void yyerror(const char *error)
	{
		std::cout
			<< gInputFileName << ":"
			<< yylloc.first_line << ":"
			<< yylloc.first_column << " "
			<< error
			<< std::endl;

		pretty_print_error();
	}
}

%locations

%union {
	std::string *stringValue;
	int intValue;
	lcpl::BinaryOperator::BinOpKind binOpKind;

	lcpl::Class *lcplClass;
	std::vector<lcpl::Class *> *lcplClasses;

	lcpl::Expression *expression;
	std::vector<lcpl::Expression *> *expressions;

	lcpl::Feature *feature;
	std::vector<lcpl::Feature *> *features;

	lcpl::FormalParam *formal;
	std::vector<lcpl::FormalParam *> *formals;

	lcpl::Block *block;
}

%start lcpl_program

/* Constant values */
%token <stringValue> IDENTIFIER STRING_CONSTANT KW_SELF
%token <intValue> INTEGER_CONSTANT

/* Class header parsing */
%token KW_CLASS KW_INHERITS KW_END KW_VAR
%token RET SD

%type <stringValue> type name
%type <lcplClasses> lcpl_classes
%type <lcplClass> lcpl_class
%type <stringValue> maybe_parent_class maybe_method_return
%type <formals> maybe_formals
%type <formal> formal
%type <features> features attributes attribute_definitions maybe_features
%type <feature> method attribute
%type <expression> maybe_assignment_rhs assignment_rhs

/* Method parsing */
%token KW_LOCAL KW_NULL KW_NEW
%token KW_IF KW_THEN KW_ELSE KW_WHILE KW_LOOP
%token <binOpKind> LT LTE EQ ADD SUB MUL DIV
%token NOT

%type <binOpKind> relational_operation additive_operation multiplicative_operation
%type <block> maybe_block block_content maybe_else_block
%type <expressions> arguments maybe_arguments local_definitions block_content_item
%type <expression> expression conditional_expression assignment_expression
%type <expression> relational_expression additive_expression
%type <expression> multiplicative_expression unary_expression local_definition
%type <expression> basic_expression leaf_expression dispatch_expression

%left IDENTIFIER
%left '['

%%

/* Program definition */
lcpl_program
	: lcpl_classes {
		gLcplProgram = new lcpl::Program(@1.first_line, *$1);
		delete $1;
	}
	;

lcpl_classes
	: lcpl_classes lcpl_class {
		$$->push_back($2);
	}
	| lcpl_class {
		$$ = new std::vector<lcpl::Class *>({ $1 });
	}
	;

/* Class definition. */
lcpl_class
	: KW_CLASS name maybe_parent_class maybe_features KW_END ';' {
		$$ = new lcpl::Class(@1.first_line, *$2, *$3, *$4);
		delete $2;
		delete $3;
		delete $4;
	}
	;

/* Class parent class. */
maybe_parent_class
	: KW_INHERITS name {
		$$ = $2;
	}
	| %empty {
		$$ = new std::string(emptyString);
	}
	;

/* Class variables and methods. */
maybe_features
	: features
	| %empty {
		$$ = new std::vector<lcpl::Feature *>();
	}
	;

features
	: features method {
		$$->push_back($2);
	}
	| method {
		$$ = new std::vector<lcpl::Feature *>({ $1 });
	}
	| attributes
	| features attributes {
		$$->insert($$->end(), $2->begin(), $2->end());
		delete $2;
	}
	;

/* Class variable definitions. */
attributes
	: KW_VAR attribute_definitions KW_END ';' {
		$$ = $2;
	}
	;

attribute_definitions
	: attribute_definitions attribute {
		$$->push_back($2);
	}
	| attribute {
		$$ = new std::vector<lcpl::Feature *>({ $1 });
	}
	;

attribute
	: type name maybe_assignment_rhs ';' {
		$$ = new lcpl::Attribute(@1.first_line, *$2, *$1, lcpl::ExpressionPtr($3));
		delete $1;
		delete $2;
	}
	;

/* Method definition */
method
	: name maybe_formals maybe_method_return ':' maybe_block KW_END ';' {
		$$ = new lcpl::Method(@1.first_line, *$1, *$3, lcpl::ExpressionPtr($5), *$2);
		delete $1;
		delete $2;
		delete $3;
	}
	;

/* Method parameters. */
maybe_formals
	: maybe_formals ',' formal {
		$$->push_back($3);
	}
	| formal {
		$$ = new std::vector<lcpl::FormalParam *>({ $1 });
	}
	| %empty {
		$$ = new std::vector<lcpl::FormalParam *>();
	}
	;

formal
	: type name {
		$$ = new lcpl::FormalParam(@1.first_line, *$2, *$1);
		delete $1;
		delete $2;
	}
	;

/* Method return type. */
maybe_method_return
	: %empty {
		$$ = new std::string(symbolVoid);
	}
	| RET type {
		$$ = $2;
	}
	;

/* A mix of expressions and local definitions. */
maybe_block
	: block_content
	| %empty {
		$$ = nullptr;
	}
	;

block_content
	: block_content_item {
		$$ = new lcpl::Block(@1.first_line, *$1);
		delete $1;
	}
	| block_content block_content_item {
		for (auto it : *$2)
			$$->addExpression(lcpl::ExpressionPtr(it));
		delete $2;
	}
	;

/* An expression or a local definition segment. */
block_content_item
	: expression ';' {
		$$ = new std::vector<lcpl::Expression *>({ $1 });
	}
	| KW_LOCAL local_definitions KW_END ';' {
		$$ = $2;
	}
	;

/* Local variable definition segment. */
local_definitions
	: local_definitions local_definition {
		$$->push_back($2);
	}
	| local_definition {
		$$ = new std::vector<lcpl::Expression *>({ $1 });
	}
	;

local_definition
	: type name maybe_assignment_rhs ';' {
		$$ = new lcpl::LocalDefinition(@1.first_line, *$2, *$1, lcpl::ExpressionPtr($3));
		delete $1;
		delete $2;
	}
	;

/* Any kind of expression. */
expression
	: assignment_expression
	| conditional_expression
	;

/* Any assignment expression to variable or attribute. */
assignment_expression
	: name assignment_rhs {
		$$ = new lcpl::Assignment(@1.first_line, *$1, lcpl::ExpressionPtr($2));
		delete $1;
	}
	| KW_SELF '.' name assignment_rhs {
		$$ = new lcpl::Assignment(@1.first_line, *$1 + "." + *$3, lcpl::ExpressionPtr($4));
		delete $1;
		delete $3;
	}
	;

/* Any conditional expression */
conditional_expression
	: relational_expression
	| NOT expression {
		$$ = new lcpl::UnaryOperator(@1.first_line, lcpl::UnaryOperator::UnaryOpKind::Not, lcpl::ExpressionPtr($2));
	}
	;

/* Any relational expression */
relational_operation
	: LT | LTE | EQ
	;

relational_expression
	: additive_expression
	| relational_expression relational_operation additive_expression {
		$$ = new lcpl::BinaryOperator(@1.first_line, $2, lcpl::ExpressionPtr($1), lcpl::ExpressionPtr($3));
	}
	;

/* Any additive expression */
additive_operation
	: ADD | SUB
	;

additive_expression
	: multiplicative_expression
	| additive_expression additive_operation multiplicative_expression {
		$$ = new lcpl::BinaryOperator(@1.first_line, $2, lcpl::ExpressionPtr($1), lcpl::ExpressionPtr($3));
	}
	;

/* Any multiplicative expression */
multiplicative_operation
	: MUL | DIV
	;

multiplicative_expression
	: unary_expression
	| multiplicative_expression multiplicative_operation unary_expression {
		$$ = new lcpl::BinaryOperator(@1.first_line, $2, lcpl::ExpressionPtr($1), lcpl::ExpressionPtr($3));
	}
	;

/* Any unary expression. */
unary_expression
	: basic_expression
	| SUB unary_expression {
		$$ = new lcpl::UnaryOperator(@1.first_line, lcpl::UnaryOperator::UnaryOpKind::Minus, lcpl::ExpressionPtr($2));
	}
	;

/* Any terminal or non-terminal highest precedence expression. */
basic_expression
	: leaf_expression
	| '(' expression ')' {
		$$ = $2;
	}
	| basic_expression '[' expression ',' expression ']' {
		$$ = new lcpl::Substring(@1.first_line, lcpl::ExpressionPtr($1), lcpl::ExpressionPtr($3), lcpl::ExpressionPtr($5));
	}
	| '[' dispatch_expression ']' {
		$$ = $2;
	}
	| '{' type expression '}' {
		$$ = new lcpl::Cast(@1.first_line, *$2, lcpl::ExpressionPtr($3));
		delete $2;
	}
	| KW_IF expression KW_THEN maybe_block maybe_else_block KW_END {
		auto block = $4 == nullptr ? new lcpl::Block(@1.first_line) : $4;
		$$ = new lcpl::IfStatement(@1.first_line, lcpl::ExpressionPtr($2), lcpl::ExpressionPtr(block), lcpl::ExpressionPtr($5));
	}
	| KW_WHILE expression KW_LOOP maybe_block KW_END {
		$$ = new lcpl::WhileStatement(@1.first_line, lcpl::ExpressionPtr($2), lcpl::ExpressionPtr($4));
	}
	;

/* Any terminal basic expression. */
leaf_expression
	: name %prec IDENTIFIER {
		$$ = new lcpl::Symbol(@1.first_line, *$1);
		delete $1;
	}
	| KW_SELF {
		$$ = new lcpl::Symbol(@1.first_line, *$1);
		delete $1;
	}
	| KW_NULL {
		$$ = new lcpl::NullConstant(@1.first_line);
	}
	| KW_NEW type {
		$$ = new lcpl::NewObject(@1.first_line, *$2);
		delete $2;
	}
	| INTEGER_CONSTANT {
		$$ = new lcpl::IntConstant(@1.first_line, $1);
	}
	| STRING_CONSTANT {
		$$ = new lcpl::StringConstant(@1.first_line, *$1);
		delete $1;
	}
	;

/* Inner content of a method dispatch. */
dispatch_expression
	: basic_expression '.' name maybe_arguments {
		$$ = new lcpl::Dispatch(@1.first_line, *$3, lcpl::ExpressionPtr($1), *$4);
		delete $3;
		delete $4;
	}
	| basic_expression SD type '.' name maybe_arguments {
		$$ = new lcpl::StaticDispatch(@1.first_line, lcpl::ExpressionPtr($1), *$3, *$5, *$6);
		delete $3;
		delete $5;
		delete $6;
	}
	| name maybe_arguments {
		$$ = new lcpl::Dispatch(@1.first_line, *$1, nullptr, *$2);
		delete $1;
		delete $2;
	}
	;

/* Dispatch arguments. */
maybe_arguments
	: %empty {
		$$ = new std::vector<lcpl::Expression *>();
	}
	| arguments {
		$$ = $1;
	}
	;

arguments
	: expression {
		$$ = new std::vector<lcpl::Expression *>({ $1 });
	}
	| arguments ',' expression {
		$$->push_back($3);
	}
	;

/* An else block in an if statement. */
maybe_else_block
	: KW_ELSE maybe_block {
		$$ = $2;
	}
	| %empty {
		$$ = nullptr;
	}
	;

/* An assignment rvalue. */
maybe_assignment_rhs
	: assignment_rhs
	| %empty {
		$$ = nullptr;
	}
	;

assignment_rhs
	: '=' expression {
		$$ = $2;
	}
	;

/* Helpers to make inline rule look nice. */
type
	: IDENTIFIER
	;

name
	: IDENTIFIER
	;

%%

static void cleanup()
{
	free((void *)gInputFileName);
	fclose(yyin);
	delete gLcplProgram;

	yylex_destroy();
}

int main(int argc, char **argv)
{
	if (argc != 3) {
		std::cout << "Usage: ./lcpl-parser <inputFile> <outputFile>" << std::endl;
		return 0;
	}

	gInputFileName = strdup(argv[1]);
	yyin = fopen(argv[1], "r");

	if (yyparse())
		return 1;

	/* Serialize the AST */
	lcpl::ASTSerializer serializer(argv[2]);
	serializer.visit(gLcplProgram);

	cleanup();

	return 0;
}

