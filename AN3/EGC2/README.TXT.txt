Elemente de grafica pe calculator - Tema 2

	World Builder
	Ungureanu Marius 333CC

1. Cerinta
Implementarea unui joc propriu de tip World Builder in care construiesti
o lume proprie.

2. Utilizare
Programul se compileaza folosind Visual Studio 2013 (probabil ultimul update
de la 2012 aduce compatibilitate). Apoi se ruleaza executabilul ori din
interfata Visual Studio, ori din folderul Debug, aplicatia EGC2.exe.

2.1 Fisiere
Nu are parametri de intrare, nici fisiere de intrare.

2.2 Consola
Nu este implementata interfatare cu consola.

2.3 Input Tastatura
[Up][Down][Left][Right] - Scrolleaza harta.
[i][k][j][l] - Misca cursorul de actiune.
[ESC] - Iese din joc.
[m] - Intra in modul de editare.
 - [0][1][2][3][4] - Seteaza tipul celulei in modulul de editare
 * [0] - Pamant.
 * [1] - Drum.
 * [2] - Apa.
 * [3] - Casa.
 * [4] - Piata.
[o][p] - Roteste obiectul care va fi pus cu 90 de grade.
[Up][Down] - Schimba dificultatea intre hard si easy in modul menu.
[ESC] - Iese din joc.

2.4 Interfata Grafica
Text care spune daca te afli in modului de edit sau nu.

3. Implementare
Tema a fost realizata pe Windows 8.1, Visual Studio 2013, cl.exe versiunea 18.00.30723.

Nu au fost folosite alte biblioteci/framework-uri in afara de si freeglut.

Schema generala:
[Deschidere aplicatie]
[Generare elemente de interfata grafica initiale]
[Asteptare input pentru joc] (1)
[Modifica lumea jocului]
[(1)]

Clasele sunt dupa tiparul Framework-ului de laborator, de forma:

Color - Un triplet RGB

Point2 - Tuplu X,Y
Point3 - Tuplu X,Y,Z

Text - Wrapper pentru a face display al unui text

Object3 - Obiect de baza pentru GridCell, container-ul de puncte initialize si transformate

GridCell - Obiect pentru a defini o celula din grid. Contine informatii despre rotatia obiectului
           si pozitia in grid

Visual2 defineste un container de obiecte Object3 si Text care vor fi folosite pentru desenare.

transform contine un namespace cu functii dedicate pentru a modifica matricea de modelare.
 * Similar lui Transform2D din Framework.

drawing contine un namespace cu functii dedicate managementului datelor ce vor fi desenate.
 * Similar lui DrawingWindow din Framework.

game contine un namespace dedicat engine-ului de joc propriu zis.
 * Contine callback-urile din DrawingWindow din Framework.

main contine functiile de glue intre engine si freeglut.

Framework-ul construit de mine contine doar strictul necesar realizarii temei si o
idiomizare mai buna a logicii. Nu am considerat necesara o clasa pentru engine-ul in sine.

4. Testare
Tema a fost testata pe Windows 8.1 Pro, Visual Studio 2013.

5. Probleme aparute
Modificarea cursorului astfel incat sa apara modelul obiectului ce va fi plasat.

Modificarea componentei alpha a culorilor. Am incercat si GL_BLEND si bindarea lui glBlendFunc.

6. Continutul Arhivei
EGC1
| Debug - freeglut.dll - Target-ul de copy nu merge.
|
| EGC1
|  |
|  |---- dependente
|  |     |
|  |     |---- Fisierele freeglut din Framework
|  |     |---- 11 fisier .wav dummy.
|  |
|  |---- Framework
|  |     |
|  |     |---- Color.h
|  |     |---- GridCell.h
|  |     |---- Object3.h
|  |     |---- Point2.h
|  |     |---- Point3.h
|  |     |---- Text.h
|  |     |---- Visual2.h
|  | 
|  |---- .gitignore
|  |---- drawing.cpp
|  |---- drawing.h
|  |---- EGC2.vcxproj
|  |---- EGC2.vcxproj.filters
|  |---- game.cpp
|  |---- game.h
|  |---- main.cpp
|  |---- transform.cpp
|  |---- transform.h
|
| EGC2.sln
| README.TXT

7. Functionalitati

Toate cerintele standard.
- Desenare grid + scroll.
- Editare teren.
- Amplasare piete si zona de acoperire.
- Amplasare si desenare case cu rotatie.
- Editare drumuri si indicare case neconectate.

8.
Codul este comentat si se poate citi cu usurinta, incercand sa creez un API
inteligibil.