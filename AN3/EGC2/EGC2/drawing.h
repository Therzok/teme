#include <vector>
#include "Framework/Visual2.h"

class Text;
class Object3;

// Apeleaza functia pe fiecare element din vector cu argumentele date.
#define FOREACH_VECTOR_PARAMS(FUNC, TYPE, VECTOR, ...) \
	for (vector<TYPE>::iterator i = VECTOR.begin(); i != VECTOR.end(); ++i) \
		FUNC(*i , ##__VA_ARGS__)

namespace drawing
{
	const int HEIGHT = 600;
	const int WIDTH = 800;

	// Context principal.
	extern Visual2 *main_visual;
	// Contexte secundare.
	extern vector<Visual2 *> visuals;

	// Deseneaza obiectele.
	void drawObjects(Visual2 *v = main_visual);

	// Adauga un text in context.
	void addText(Text *text, Visual2 *v = main_visual);
	// Sterge un text din context.
	void removeText(Text *text, Visual2 *v = main_visual);
	// Adauga un obiect in context.
	void addObject3(Object3 *o, Visual2 *v = main_visual);
	// Sterge un obiect din context.
	void removeObject3(Object3 *o, Visual2 *v = main_visual);

	// Adauga un context.
	void addVisual2D(Visual2 *v);
	// Sterge un context.
	void removeVisual2D(Visual2 *v);
}