#pragma once
#include "Framework/Visual2.h"

#ifdef _MSC_VER
#include "..\dependente\freeglut.h"
#else
#include "GL/freeglut.h"
#endif

namespace constants
{
#define FONT GLUT_BITMAP_TIMES_ROMAN_24
}

namespace game
{
	// Callbacks
	void onInit();
	void onIdle();
	void onDisplay();
	void onKey(unsigned char key);
}
