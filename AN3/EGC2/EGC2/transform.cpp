#include "Framework/Object3.h"
#include "transform.h"
#include <math.h>

namespace transform
{
	//initial matricea de transformare e matricea identitate
	// 1 0 0
	// 0 1 0
	// 0 0 1

	float ModelMatrix[4][4] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
	void loadIdentityMatrix()
	{
		//matricea de modelare se intoarce la matricea identitate
		// 1 0 0 0
		// 0 1 0 0
		// 0 0 1 0
		// 0 0 0 1
		ModelMatrix[0][0] = 1;	ModelMatrix[0][1] = 0;	ModelMatrix[0][2] = 0;	ModelMatrix[0][3] = 0;
		ModelMatrix[1][0] = 0;	ModelMatrix[1][1] = 1;	ModelMatrix[1][2] = 0;	ModelMatrix[1][3] = 0;
		ModelMatrix[2][0] = 0;	ModelMatrix[2][1] = 0;	ModelMatrix[2][2] = 1;	ModelMatrix[2][3] = 0;
		ModelMatrix[3][0] = 0;	ModelMatrix[3][1] = 0;	ModelMatrix[3][2] = 0;	ModelMatrix[3][3] = 1;
	}

	void multiplyMatrix(float matrix[4][4])
	{//								m00   m01  m02  m03     tm00  tm01  tm02  tm03
	 // matrix * ModelMatrix =      m10   m11  m12  m13  *  tm10  tm11  tm12  tm13
	 //								m20   m21  m22  m23     tm20  tm21  tm22  tm23
	 //                             m30   m31  m32  m33     tm30  tm31  tm32  tm33
		int i, j, k;
		float aux_matrix[4][4];
		for (i = 0; i < 4; i++)
			for (j = 0; j < 4; j++)
			{
				aux_matrix[i][j] = 0;
				for (k = 0; k < 4; k++)
					aux_matrix[i][j] = aux_matrix[i][j] + matrix[i][k] * ModelMatrix[k][j];
			}

		for (i = 0; i < 4; i++)
			for (j = 0; j < 4; j++)
				ModelMatrix[i][j] = aux_matrix[i][j];
	}

	void translateMatrix(float tx, float ty, float tz)
	{
		float TranslateMatrix[4][4] = {};

		//se construieste matricea de translatie
		//1  0  0  tx 
		//0  1  0  ty
		//0  0  1  tz
		//0  0  0  1

		TranslateMatrix[0][0] = TranslateMatrix[1][1] = TranslateMatrix[2][2] = TranslateMatrix[3][3] = 1;
		TranslateMatrix[0][3] = tx;
		TranslateMatrix[1][3] = ty;
		TranslateMatrix[2][3] = tz;

		//se inmulteste matricea de translatie cu matricea curenta de transformari
		//folosim scrierea vectori coloana
		multiplyMatrix(TranslateMatrix);
	}

	void rotateMatrixOx(float u)
	{
		float RotateMatrix[4][4] = {};

		//se construieste matricea de rotatie fata de Ox
		//1        0       0        0
		//0       cos(u)   -sin(u)  0
		//0       sin(u)   cos(u)   0
		//0        0       0        1

		RotateMatrix[0][0] = RotateMatrix[3][3] = 1;
		RotateMatrix[1][1] = RotateMatrix[2][2] = cos(u);
		RotateMatrix[1][2] = -sin(u);
		RotateMatrix[2][1] = sin(u);

		//se inmulteste matricea de rotatie cu matricea curenta de modelare
		//folosim scrierea vectori coloana
		multiplyMatrix(RotateMatrix);
	}

	void rotateMatrixOy(float u)
	{
		float RotateMatrix[4][4] = {};

		//se construieste matricea de rotatie fata de Oy
		//cos(u)   0      -sin(u)  0
		//0		   1       0       0
		//sin(u)   0       cos(u)  0
		//0        0       0       1

		RotateMatrix[1][1] = RotateMatrix[3][3] = 1;
		RotateMatrix[0][0] = RotateMatrix[2][2] = cos(u);
		RotateMatrix[0][2] = -sin(u);
		RotateMatrix[2][0] = sin(u);

		//se inmulteste matricea de rotatie cu matricea curenta de modelare
		//folosim scrierea vectori coloana
		multiplyMatrix(RotateMatrix);
	}

	void rotateMatrixOz(float u)
	{
		float RotateMatrix[4][4] = {};

		//se construieste matricea de rotatie Oz
		//cos(u)  -sin(u)  0  0
		//sin(u)  cos(u)   0  0
		//0        0       1  0
		//0        0       0  1

		RotateMatrix[2][2] = RotateMatrix[3][3] = 1;
		RotateMatrix[0][0] = RotateMatrix[1][1] = cos(u);
		RotateMatrix[0][1] = -sin(u);
		RotateMatrix[1][0] = sin(u);

		//se inmulteste matricea de rotatie cu matricea curenta de modelare
		//folosim scrierea vectori coloana
		multiplyMatrix(RotateMatrix);
	}

	void applyTransform(Point3 *p, Point3 *transf_p)
	{
		//se inmulteste matricea curenta de transformari cu pozitia punctului
		// tm00  tm01  tm02    p.x
		// tm10  tm11  tm12  * p.y
		// tm20  tm21  tm22     1
		transf_p->x = ModelMatrix[0][0] * p->x + ModelMatrix[0][1] * p->y + ModelMatrix[0][2] * p->z + ModelMatrix[0][3];
		transf_p->y = ModelMatrix[1][0] * p->x + ModelMatrix[1][1] * p->y + ModelMatrix[1][2] * p->z + ModelMatrix[1][3];
		transf_p->z = ModelMatrix[2][0] * p->x + ModelMatrix[2][1] * p->y + ModelMatrix[2][2] * p->z + ModelMatrix[2][3];
	}

	void applyTransform(Object3 *o)
	{
		//se aplica transformarea pe toate punctele obiectului
		for (unsigned int i = 0; i < o->vertices.size(); i++)
			applyTransform(o->vertices[i], o->transf_vertices[i]);
	}
}