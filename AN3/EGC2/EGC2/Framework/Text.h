#pragma once
#include <string>

#include "Color.h"
#include "Point2.h"

#ifdef _MSC_VER
#include "..\dependente\freeglut.h"
#else
#include "GL/freeglut.h"
#endif


using namespace std;

// Clasa de baza pentru text.
class Text
{
public:
	Point2 pos;
	Color color;
	void *font;
	string text;

	Text(string _text) :
		pos(Point2()), color(Color()), font(GLUT_BITMAP_8_BY_13), text(_text) {}
	Text(string _text, Point2 _pos, Color _color, void *_font) :
		pos(_pos), color(_color), font(_font), text(_text) {}
	~Text() {};
};
