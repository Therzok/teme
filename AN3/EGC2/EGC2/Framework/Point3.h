#pragma once

// Clasa pentru un Punct in 3D.
class Point3
{
public:
	// Componente 3D.
	float x, y, z;

	Point3() :
		x(0.f), y(0.f), z(0.f) {}
	Point3(float _x, float _y, float _z) :
		x(_x), y(_y), z(_z) {}
	~Point3() {}
};