#pragma once

// Clasa pentru culoare.
class Color
{
public:
	// Componenta culoare.
	float r, g, b;

	Color() :
		r(0.f), g(0.f), b(0.f) {}
	Color(float _r, float _g, float _b) :
		r(_r), g(_g), b(_b) {}

	~Color() {}
};