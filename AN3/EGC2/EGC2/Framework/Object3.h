#pragma once
#include <vector>
#include "Point3.h"
#include "Color.h"

using namespace std;

class Face
{
public:
	vector<int> contour;

public:
	Face(vector <int> list)
	{
		for (unsigned int i = 0; i < list.size(); i++)
			contour.push_back(list[i]);
	}

	Face(int i1, int i2, int i3, int i4)
	{
		contour.push_back(i1);
		contour.push_back(i2);
		contour.push_back(i3);
		contour.push_back(i4);
	}

	Face()
	{
		contour.push_back(0);
		contour.push_back(1);
		contour.push_back(2);
		contour.push_back(3);
	}
};

class Object3
{
public:
	vector<Point3*> vertices, transf_vertices;
	vector<Face*> faces;
	Color color;
	bool fill;

public:
	Object3(vector <Point3*> listV, vector <Face*> listF, Color _color, bool _fill)
	{
		unsigned int i;
		for (i = 0; i < listV.size(); i++)
		{
			vertices.push_back(new Point3(listV[i]->x, listV[i]->y, listV[i]->z));
			transf_vertices.push_back(new Point3(listV[i]->x, listV[i]->y, listV[i]->z));
		}
		for (i = 0; i < listF.size(); i++)
			faces.push_back(new Face(listF[i]->contour));

		color.r = _color.r;
		color.g = _color.g;
		color.b = _color.b;

		fill = _fill;
	}

	virtual ~Object3() {}
};