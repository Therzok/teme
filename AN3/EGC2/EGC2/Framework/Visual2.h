#pragma once
#ifdef _MSC_VER
#include "..\dependente\freeglut.h"
#else
#include "GL/freeglut.h"
#endif

#include "Object3.h"
#include "Text.h"

// Clasa pentru context visual 2D. Din Framework.
class Visual2
{
public:
	// Containers.
	vector <Object3 *> objects;
	vector <Text *> texts;

	Visual2() : XPm(0), YPm(0), XPM(0), YPM(0),
		XFm(0), YFm(0), XFM(0), YFM(0) {}
	Visual2(float xf1, float yf1, float xf2, float yf2, int xp1, int yp1, int xp2, int yp2) :
		XFm(xf1), XFM(xf2), YFm(yf1), YFM(yf2), XPm(xp1), XPM(xp2), YPm(yp1), YPM(yp2) {}

	~Visual2()
	{
		while (!objects.empty())
		{
			delete objects.back();
			objects.pop_back();
		}

		while (!texts.empty())
		{
			delete texts.back();
			texts.pop_back();
		}
	}

	// Extrage o noua poarta de vizualizare.
	void decupare(int height)
	{
		//glViewport stabileste transformarea in poarta de vizualizare
		glViewport(XPm, height - YPM, XPM - XPm, YPM - YPm);

		//se stabileste transformarea de proiectie
		glMatrixMode(GL_PROJECTION);

		//se porneste de la matricea identitate
		glLoadIdentity();

		//glOrtho este o proiectie ortografica - ea stabileste volumul de vizualizare
		//in cazul nostru, stabileste fereastra de vizualizare
		glOrtho(XFm - 0.01, XFM + 0.01, YFm - 0.01, YFM + 0.01, -10000, 10000);

		// ne intoarcem la matricea de modelare vizualizare
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glRotatef(35.264f, 1.0f, 0.0f, 0.0f);
		glRotatef(-45.0f, 0.0f, 1.0f, 0.0f);
	}

private:
	float XFm, XFM, YFm, YFM;
	int XPm, XPM, YPm, YPM;
};
