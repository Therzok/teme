#pragma once
#include "Object3.h"

enum CellType
{
	Empty,
	Road,
	Water,
	House,
	Market,
	Line,
};

class GridCell : public Object3
{
public:
	int i, j;
	float rotation;
	CellType type;

	GridCell(int _i, int _j, vector <Point3*> listV, vector <Face*> listF, Color _color, bool _fill) :
		Object3(listV, listF, _color, _fill), i(_i), j(_j), rotation(0.f), type(Empty) {}
};