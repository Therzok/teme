#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <ctime>
#ifdef _MSC_VER
#include <windows.h>
#endif

#include "Framework/Text.h"
#include "Framework/GridCell.h"

#include "drawing.h"
#include "game.h"
#include "transform.h"

#define PI 3.14159265358979323846

using namespace constants;
using namespace drawing;

#define FREE_VECTOR(v) \
	while (!v.empty()) { delete v.back(); v.pop_back(); }

const Color ground = Color(155.f / 255, 69.f / 255, 19.f / 255);
const Color road = Color(112.f / 255, 138.f / 255, 144.f / 255);
const Color water = Color(30.f / 255, 144.f / 255, 255.f / 255);
const Color house = Color(152.f / 255, 251.f / 255, 154.f / 255);
const Color market = Color(255.f / 255, 152.f / 255, 154.f / 255);
const Color yellow = Color(1.f, 1.f, 0.f);
const Color red = Color(1.f, 0.f, 0.f);

namespace game
{
	vector<GridCell *> grid, grid_lines;
	GridCell *cursor;
	CellType toplace;
	Text textEdit("", Point2(-8.5, 7), Color(1, 0, 0), FONT);;
	const int rows = 30, cols = 30, market_radius = 6;
	float translation_x = -15, translation_z = -15;
	bool edit;

	GridCell *createCell(CellType type, int i, int j)
	{
		vector<Point3 *> vertices;
		vector<Face *> faces;
		GridCell *cell = NULL;
#define VERTICES(x, y, z) \
	vertices.push_back(new Point3(x, y, z)); \
	vertices.push_back(new Point3(x + 1, y, z)); \
	vertices.push_back(new Point3(x + 1, y, z + 1)); \
	vertices.push_back(new Point3(x, y, z + 1)); \
	faces.push_back(new Face());

#define VERTICES2(x, y, z) \
	vertices.push_back(new Point3(x,		y,			z)); \
	vertices.push_back(new Point3(x + 1,	y,			z)); \
	vertices.push_back(new Point3(x + 1,	y,			z + 1)); \
	vertices.push_back(new Point3(x,		y,			z + 1)); \
	\
	vertices.push_back(new Point3(x,		y + 1,		z)); \
	vertices.push_back(new Point3(x + 1,	y + 1,		z)); \
	vertices.push_back(new Point3(x + 1,	y + 1,		z + 0.5)); \
	vertices.push_back(new Point3(x,		y + 1,		z + 0.5)); \
	\
	vertices.push_back(new Point3(x,		y + 0.5,	z + 0.5)); \
	vertices.push_back(new Point3(x + 1,	y + 0.5,	z + 0.5)); \
	vertices.push_back(new Point3(x + 1,	y + 0.5,	z + 1)); \
	vertices.push_back(new Point3(x,		y + 0.5,	z + 1)); \
	\
	vertices.push_back(new Point3(x + 1,	y,			z + 0.5)); \
	vertices.push_back(new Point3(x,		y,			z + 0.5)); \
	\
	faces.push_back(new Face(0, 1, 2, 3)); \
	faces.push_back(new Face(4, 5, 6, 7)); \
	faces.push_back(new Face(8, 9, 10, 11)); \
	\
	faces.push_back(new Face(0, 1, 5, 4)); \
	faces.push_back(new Face(6, 7, 13, 12)); \
	faces.push_back(new Face(0, 13, 7, 4)); \
	faces.push_back(new Face(1, 12, 6, 5)); \
	faces.push_back(new Face(1, 2, 10, 9)); \
	faces.push_back(new Face(0, 3, 11, 8)); \
	faces.push_back(new Face(10, 11, 13, 12));

		switch (type)
		{
		case Empty:
			VERTICES(j, 0, i);
			cell = new GridCell(i, j, vertices, faces, ground, true);
			break;
		case Road:
			VERTICES(j, 0, i);
			cell = new GridCell(i, j, vertices, faces, road, true);
			break;
		case Water:
			VERTICES(j, 0, i);
			cell = new GridCell(i, j, vertices, faces, water, true);
			break;
		case House:
			VERTICES2(j, 0 ,i);
			cell = new GridCell(i, j, vertices, faces, house, true);
			break;
		case Market:
			VERTICES2(j, 0, i);
			cell = new GridCell(i, j, vertices, faces, market, true);
			break;
		case Line:
			VERTICES(j, 0, i);
			cell = new GridCell(i, j, vertices, faces, yellow, false);
			break;
		}
		cell->type = type;

		FREE_VECTOR(vertices);
		FREE_VECTOR(faces);
		return cell;
	}

	void makeGrid()
	{
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < cols; ++j)
			{
				grid.push_back(createCell(Empty, i, j));
				grid_lines.push_back(createCell(Line, i, j));
			}

		FOREACH_VECTOR_PARAMS(addObject3, GridCell *, grid);
		FOREACH_VECTOR_PARAMS(addObject3, GridCell *, grid_lines);
	}

	void makeCursor()
	{
		vector <Point3*> vertices;
		vector <Face*> faces;
		vertices.push_back(new Point3(cols / 2, 0, rows / 2));
		vertices.push_back(new Point3(cols / 2 + 1, 0, rows / 2));
		vertices.push_back(new Point3(cols / 2 + 1, 0, rows / 2 + 1));
		vertices.push_back(new Point3(cols / 2, 0, rows / 2 + 1));
		faces.push_back(new Face());
		cursor = new GridCell(rows / 2, cols / 2, vertices, faces, yellow, true);
		FREE_VECTOR(vertices);
		FREE_VECTOR(faces);
		addObject3(cursor);
	}

	bool findMarket(int i, int j, int depth, bool checkRoad)
	{
		if (depth == market_radius)
			return false;

		if (i < 0 || i >= rows || j < 0 || j >= cols)
			return false;

		if (depth != 0)
		{
			if (grid[i * rows + j]->type == Market)
				return true;

			if (checkRoad && grid[i * rows + j]->type != Road)
				return false;
		}

		++depth;
		return findMarket(i + 1, j, depth, checkRoad) ||
			findMarket(i, j + 1, depth, checkRoad) ||
			findMarket(i - 1, j, depth, checkRoad) ||
			findMarket(i, j - 1, depth, checkRoad);
	}

	bool connected(int i, int j)
	{
		return findMarket(i, j, 0, true);
	}

	bool inRange(int i, int j)
	{
		return findMarket(i, j, 0, false);
	}

	void scrollMap(bool checkMarket = false)
	{
		for (vector<GridCell *>::iterator i = grid.begin(); i != grid.end(); ++i)
		{
			transform::loadIdentityMatrix();
			transform::translateMatrix(-(*i)->j - 0.5, -0.5, -(*i)->i - 0.5);
			if ((*i)->type == House || (*i)->type == Market)
				transform::rotateMatrixOy((*i)->rotation);
			transform::translateMatrix((*i)->j + translation_x, 0, (*i)->i + translation_z);
			transform::applyTransform(*i);

			if ((*i)->type == House)
			{
				if (connected((*i)->i, (*i)->j))
					(*i)->color = house;
				else
					(*i)->color = Color(house.r / 2, house.g / 2, house.b / 2);
			}
			else if (checkMarket)
			{
				if ((*i)->type == Empty)
					(*i)->color = inRange((*i)->i, (*i)->j) ? Color(ground.r + 10, ground.g + 10, ground.b + 10) : ground;
			}
		}

		transform::loadIdentityMatrix();
		transform::translateMatrix(translation_x, 0, translation_z);
		FOREACH_VECTOR_PARAMS(transform::applyTransform, GridCell *, grid_lines);
		transform::applyTransform(cursor);
	}

	void moveCursor(int di, int dj, bool checkMarket = false)
	{
		static Color oldColor;
		GridCell *selected = grid[cursor->i * rows + cursor->j];
		if (cursor->i + di >= rows || cursor->i + di < 0 ||
			cursor->j + dj >= cols || cursor->j + dj < 0)
			return;

		cursor->i += di;
		cursor->j += dj;
		GridCell *dest = grid[cursor->i * rows + cursor->j];

		int h = dest->type == House || dest->type == Market;
		for (vector<Point3 *>::iterator i = cursor->vertices.begin(); i != cursor->vertices.end(); ++i)
		{
			(*i)->x += dj;
			(*i)->y = h;
			(*i)->z += di;
		}

		if (edit && 
			(dest->type != Empty || toplace == House && !inRange(dest->i, dest->j)))
		{
			if (!(cursor->color.r == red.r && cursor->color.g == 0 && cursor->color.b == 0))
				oldColor = cursor->color;
			cursor->color = red;
		}
		else if ((cursor->color.r == red.r && cursor->color.g == 0 && cursor->color.b == 0))
			cursor->color = oldColor;

		scrollMap(checkMarket);
	}

	void rotateCursor(float degrees)
	{
		cursor->rotation += degrees;
		scrollMap();
	}

	void replaceCursor(CellType t)
	{
		GridCell *cell = cursor;
		GridCell *new_cell = createCell(t, cursor->i, cursor->j);

		std::replace(main_visual->objects.begin(), main_visual->objects.end(), cell, new_cell);
		cursor = new_cell;
		delete cell;
	}

	void startEdit()
	{
		replaceCursor(toplace);
		cursor->color = Color(cursor->color.r/1.5, cursor->color.g/1.5, cursor->color.b/1.5);
		moveCursor(0, 0);
	}

	void endEdit()
	{
		int i = cursor->i * rows + cursor->j;
		if (cursor->color.r != red.r || cursor->color.g != 0 || cursor->color.b != 0)
		{
			GridCell *cell = grid[i];
			grid[i] = createCell(toplace, cursor->i, cursor->j);
			grid[i]->rotation = cursor->rotation;
			main_visual->objects[i + 1] = grid[i];
			delete cell;
		}
		replaceCursor(toplace);
		cursor->color = yellow;
		cursor->rotation = 0;

		moveCursor(0, 0, toplace == Market);
		toplace = Empty;
	}

	// Display-ul initial al ferestrei.
	void onInit()
	{
		main_visual = new Visual2(-10.f, -10.f, 10, 10, 0, 0, WIDTH, HEIGHT);
		// Force clip.
		addVisual2D(new Visual2(-10, -10, 10, 10, 0, 0, WIDTH, WIDTH));

		addText(&textEdit);
		makeCursor();
		makeGrid();
		scrollMap();
	}

	// Deseneaza obiectele din contextele secundare si apoi obiectele din contextul principal.
	void onDisplay()
	{
		for (vector<Visual2 *>::iterator i = visuals.begin(); i != visuals.end(); ++i)
		{
			(*i)->decupare(HEIGHT);
			// deseneaza obiectele din contextul vizual curent
			drawObjects(*i);
		}
		//main_visual->decupare(HEIGHT);
		//deseneaza obiectele din containerul principal
		drawObjects(main_visual);
	}

	// Logica principala.
	void onIdle()
	{
	}

	// Callback pentru apasare de taste.
	void onKey(unsigned char key)
	{
		bool reverse = false;
		switch (key)
		{
		case 27: exit(0);	// Exit
		case GLUT_KEY_LEFT:
			reverse = true;
		case GLUT_KEY_RIGHT:
			translation_x += reverse ? 1 : -1;
			scrollMap();
			break;

		case GLUT_KEY_UP:
			reverse = true;
		case GLUT_KEY_DOWN:
			translation_z += reverse ? 1 : -1;
			scrollMap();
			break;

		case 'k':
			reverse = true;
		case 'i':
			moveCursor(reverse ? 1 : -1, 0);
			break;

		case 'l':
			reverse = true;
		case 'j':
			moveCursor(0, reverse ? 1 : -1);
			break;

		case 'm':
			edit = !edit;
			if (edit)
				textEdit.text = "Mod Editare";
			else
			{
				textEdit.text = "";
				endEdit();
			}

			break;

		case 'o':
			reverse = true;
		case 'p':
			rotateCursor(reverse ? PI/2 : -PI/2);
			break;

		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
			if (edit)
			{
				toplace = (CellType)(key - '0');
				startEdit();
			}
			break;
		}
	}
}
