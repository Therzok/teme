#pragma once

class Object;

// Din Framework.
namespace transform
{
	// Reseteaza matricea de transformari.
	void loadIdentityMatrix();
	// Translateaza matricea cu tx, ty si tz.
	void translateMatrix(float tx, float ty, float tz);
	// Roteste matricea cu u grade in jurul lui Ox.
	void rotateMatrixOx(float u);
	// Roteste matricea cu u grade in jurul lui Oy.
	void rotateMatrixOy(float u);
	// Roteste matricea cu u grade in jurul lui Oz.
	void rotateMatrixOz(float u);
	// Aplica transformarea pe obiect.
	void applyTransform(Object3 *o);
}