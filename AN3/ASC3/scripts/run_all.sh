#!/bin/bash

cd ..
make clean
make

cd comp
make clean
make
cd ..

for i in {1..4}; do
    echo "Execution $i:"
    for spus in 1 2 4 8; do
        echo "$spus SPUs"
        for mod_vect in 0 1; do
            echo "mod_vect: $mod_vect"
            ./tema3 $mod_vect 0 $spus "../tema3_input/in$i.pgm" "./out$i.cmp" "./out$i.pgm"
            ./compare cmp "./out$i.cmp" "../tema3_output/out$i.cmp"
            ./compare pgm "./out$i.pgm" "../tema3_output/out$i.pgm"
            echo ""
        done
    done
done
