#ifndef STRUCTS_H_
#define STRUCTS_H_

#define BLOCK_SIZE		16
#define NUM_COLORS_PALETTE	16

// macro for easily getting how much time has passed between two events
#define GET_TIME_DELTA(t1, t2) ((t2).tv_sec - (t1).tv_sec + \
		((t2).tv_usec - (t1).tv_usec) / 1000000.0)


struct spu_args {
	int mod_dma, mod_vect, width, height;
} __attribute__((aligned(16)));

//regular image
struct img {
	unsigned char *pixels;
	int width, height;
} __attribute__((aligned(16)));

struct c_img {
	//compressed image
	struct block *blocks;
	int width, height;
};

struct block {
	//index matrix for the pixels in the block
	unsigned char index_matrix[BLOCK_SIZE * BLOCK_SIZE];

	//min and max values for the block
	unsigned char min, max;
} __attribute__((aligned(16)));

struct nibbles {
	unsigned first_nibble : 4;
	unsigned second_nibble: 4;
};

#endif

