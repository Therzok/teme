#include <spu_intrinsics.h>
#include <spu_mfcio.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include "../common/structs.h"

#define wait_tag(tag) { \
	mfc_write_tag_mask(1 << tag); \
	mfc_read_tag_status_all(); \
}

#define req_data() spu_write_out_intr_mbox(0)

int main(unsigned long speid, unsigned long long argp, unsigned long long envp)
{
	unsigned char v[BLOCK_SIZE * BLOCK_SIZE] __attribute__((aligned(16)));
	float buf[BLOCK_SIZE * BLOCK_SIZE] __attribute__((aligned(16)));
	struct block curr_block;
	float *pf, factor, to_add;
	vector float *vbuf, vfactor, vto_add;
	struct spu_args args;
	unsigned long long write_dest;
	uint32_t mbox, tag_id, k, i, dma;
	unsigned char *pc;

	(void)speid;
	(void)envp;

	vbuf = (vector float *)buf;
	dma = 0;

	// Reserve a tag.
	tag_id = mfc_tag_reserve();
	if (tag_id == MFC_TAG_INVALID) {
		printf("Error: no tag.");
		return 1;
	}

	// Get program data.
	mfc_get(&args, (unsigned int)argp, sizeof(args), tag_id, 0, 0);
	wait_tag(tag_id);

	// Compress.
	req_data();
	while ((write_dest = spu_read_in_mbox()) != 0) {
		mbox = spu_read_in_mbox();

		for (i = 0; i < BLOCK_SIZE; ++i) {
			mfc_get(v + i * BLOCK_SIZE, mbox + i * args.width, BLOCK_SIZE, tag_id, 0, 0);
			wait_tag(tag_id);
		}

		unsigned char min, max;
		min = max = v[0];

		for (i = 1; i < BLOCK_SIZE * BLOCK_SIZE; ++i) {
			if (v[i] < min)
				min = v[i];
			if (v[i] > max)
				max = v[i];
		}
		curr_block.max = max;
		curr_block.min = min;

		factor = (curr_block.max - curr_block.min) / (float)(NUM_COLORS_PALETTE - 1);
		if (factor != 0) {
			if (args.mod_vect >= 1) {
				vfactor = spu_splats(factor);
				vto_add = spu_splats((float)min);
				for (i = 0, pf = buf, pc = v; i < BLOCK_SIZE * BLOCK_SIZE; ++i)
					*pf++ = (float)*pc++;

				for (i = 0; i < BLOCK_SIZE * BLOCK_SIZE / 4; ++i) {
					vbuf[i] -= vto_add;
					vbuf[i] /= vfactor;
				}

				for (i = 0, pf = buf, pc = curr_block.index_matrix; i < BLOCK_SIZE * BLOCK_SIZE; ++i)
					*pc++ = (unsigned char)*pf++;
			} else {
				for (i = 0; i < BLOCK_SIZE * BLOCK_SIZE; ++i)
					curr_block.index_matrix[i] = (unsigned char)((v[i] - min) / factor + 0.5f);
			}
		} else
			memset(curr_block.index_matrix, 0, BLOCK_SIZE * BLOCK_SIZE);

		mfc_put(&curr_block, write_dest, sizeof(curr_block), tag_id, 0, 0);
		wait_tag(tag_id);
		req_data();
	}

	// Decompress.
	req_data();

	// Get pixels write destination.
	write_dest = spu_read_in_mbox();
	while ((mbox = spu_read_in_mbox()) != 0) {
		mfc_get(&curr_block, mbox, sizeof(curr_block), tag_id, 0, 0);
		wait_tag(tag_id);
		k = spu_read_in_mbox();

		factor = (curr_block.max - curr_block.min) / (float)(NUM_COLORS_PALETTE - 1);
		to_add = curr_block.min + 0.5f;
		if (args.mod_vect >= 1) {
			vfactor = spu_splats(factor);
			vto_add = spu_splats(to_add);

			for (i = 0, pf = buf, pc = curr_block.index_matrix; i < BLOCK_SIZE * BLOCK_SIZE; ++i)
				*pf++ = (float)*pc++;

			for (i = 0; i < BLOCK_SIZE * BLOCK_SIZE / 4; ++i) {
				vbuf[i] *= vfactor;
				vbuf[i] += vto_add;
			}

			for (i = 0, pf = buf, pc = v; i < BLOCK_SIZE * BLOCK_SIZE; ++i)
				*pc++ = (unsigned char)*pf++;
		} else {
			for (i = 0, pc = curr_block.index_matrix; i < BLOCK_SIZE * BLOCK_SIZE; ++i)
				v[i] = ((float)*pc++) * factor + to_add;
		}

		for (i = 0; i < BLOCK_SIZE * BLOCK_SIZE; i += BLOCK_SIZE, k += args.width) {
			mfc_put(v + i, write_dest + k * sizeof(unsigned char), BLOCK_SIZE, tag_id, 0, 0);
			wait_tag(tag_id);
		}

		req_data();
	}

	return 0;
}
