#ifndef PARALLEL_H_
#define PARALLEL_H_

#include "structs.h"

struct cli_args {
	char *in_file, *out_file, *cmp_file;
	int mod_vect, mod_dma, num_spus;
};

void compress_parallel(struct cli_args *args, struct img *image, struct c_img *c_image);
void decompress_parallel(struct cli_args *args, struct img *image, struct c_img *c_image);

#endif

