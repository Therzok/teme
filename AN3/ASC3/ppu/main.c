#include <libmisc.h>
#include <libspe2.h>
#include <pthread.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "cmp.h"
#include "pgm.h"

extern spe_program_handle_t tema3_spu;

struct spu_event_args {
	int mod_dma, mod_vect, width, height, started;
	spe_context_ptr_t spe;
};

void *ppu_thread_cb(void *thread_arg)
{
	struct spu_event_args *arg;
	struct spu_args spu_args;

	arg = thread_arg;
	spu_args.mod_dma = arg->mod_dma;
	spu_args.mod_vect = arg->mod_vect;
	spu_args.width = arg->width;
	spu_args.height = arg->height;

	unsigned int entry = SPE_DEFAULT_ENTRY;
	if (spe_context_run(arg->spe, &entry, 0, &spu_args, NULL, NULL) < 0) {
		perror("Failed running context");
		exit(1);
	}

	return NULL;
}

static void run(struct cli_args *args, double *scale_time)
{
	spe_event_handler_ptr_t event_handler;
	spe_event_unit_t *events, event;
	spe_context_ptr_t *ctxs;
	pthread_t *threads;
	struct spu_event_args *spu_event_args, *event_arg;
	struct img image, image2;
	struct c_img c_image;
	struct timeval t1, t2;
	unsigned int mbox_data, max_blocks, blocks, active_spus, block_row_start, block_col_start;
	int i;

	read_pgm(args->in_file, &image);

	threads = malloc(args->num_spus * sizeof(pthread_t));
	events = malloc(args->num_spus * sizeof(spe_event_unit_t));
	ctxs = malloc(args->num_spus * sizeof(spe_context_ptr_t));
	spu_event_args = malloc(args->num_spus * sizeof(struct spu_event_args));

	if (!(event_handler = spe_event_handler_create())) {
		perror("Failed creating event handler");
		exit(1);
	}

	for (i = 0; i < args->num_spus; ++i) {
		if (!(ctxs[i] = spe_context_create(SPE_EVENTS_ENABLE, NULL))) {
			perror("Failed to create spu context");
			exit(1);
		}

		if (spe_program_load(ctxs[i], &tema3_spu)) {
			perror("Failed to load program.");
			exit(1);
		}

		spu_event_args[i].spe = ctxs[i];
		spu_event_args[i].mod_dma = args->mod_dma;
		spu_event_args[i].mod_vect = args->mod_vect;
		spu_event_args[i].width = image.width;
		spu_event_args[i].height = image.height;
		spu_event_args[i].started = 0;

		if (pthread_create(&threads[i], NULL, ppu_thread_cb, &spu_event_args[i])) {
			perror("Failed to create thread");
			exit(1);
		}

		events[i].events = SPE_EVENT_OUT_INTR_MBOX;
		events[i].spe = ctxs[i];
		events[i].data.ptr = (void*)&spu_event_args[i]; // recover this thread's info when event arrives
		if (spe_event_handler_register(event_handler, &events[i]) < 0) {
			perror("Failed event registration.");
			exit(1);
		}
	}

	// Common initialization:
	max_blocks = image.width * image.height / (BLOCK_SIZE * BLOCK_SIZE);
	c_image.blocks = malloc_align(max_blocks * sizeof(struct block), 16);
	image2.width = c_image.width = image.width;
	image2.height = c_image.height = image.height;
	image2.pixels = malloc_align(image2.width * image2.height * sizeof(unsigned char), 16);

	// Compress
	blocks = block_row_start = block_col_start = 0;
	active_spus = args->num_spus;

	gettimeofday(&t1, NULL);
	while (active_spus) {
		/* Wait for event. */
		if (spe_event_wait(event_handler, &event, 1, -1) < 0) {
			perror("Failed event_wait.");
			exit(1);
		}
		event_arg = event.data.ptr;

		spe_out_intr_mbox_read(event_arg->spe, &mbox_data, 1, SPE_MBOX_ANY_NONBLOCKING);

		if (++blocks > max_blocks) {
			mbox_data = 0;
			--active_spus;
			event_arg->started = 0;
		} else {
			mbox_data = (unsigned int)(&c_image.blocks[blocks - 1]);
			spe_in_mbox_write(event_arg->spe, &mbox_data, 1, SPE_MBOX_ANY_NONBLOCKING);

			mbox_data = (unsigned int)(image.pixels + block_row_start * image.width + block_col_start);
			block_col_start += BLOCK_SIZE;
			if (block_col_start >= image.width) {
				block_col_start = 0;
				block_row_start += BLOCK_SIZE;
			}
		}
		spe_in_mbox_write(event_arg->spe, &mbox_data, 1, SPE_MBOX_ANY_NONBLOCKING);
	}

	// Decompress.
	blocks = block_row_start = block_col_start = 0;
	active_spus = args->num_spus;

	while (active_spus) {
		/* Wait for event. */
		if (spe_event_wait(event_handler, &event, 1, -1) < 0) {
			perror("Failed event_wait.");
			exit(1);
		}
		event_arg = event.data.ptr;

		spe_out_intr_mbox_read(event_arg->spe, &mbox_data, 1, SPE_MBOX_ANY_NONBLOCKING);
		if (!event_arg->started) {
			event_arg->started = 1;
			mbox_data = (unsigned int)image2.pixels;
			spe_in_mbox_write(event_arg->spe, &mbox_data, 1, SPE_MBOX_ANY_NONBLOCKING);
		}

		if (blocks >= max_blocks) {
			mbox_data = 0;
			--active_spus;
			event_arg->started = 0;
		} else {
			// Write block address.
			mbox_data = (unsigned int)&(c_image.blocks[blocks]);
			spe_in_mbox_write(event_arg->spe, &mbox_data, 1, SPE_MBOX_ANY_NONBLOCKING);

			// Write k.
			mbox_data = block_row_start * image2.width + block_col_start;
			block_col_start += BLOCK_SIZE;
			if (block_col_start >= image2.width) {
				block_col_start = 0;
				block_row_start += BLOCK_SIZE;
			}
			++blocks;
		}
		spe_in_mbox_write(event_arg->spe, &mbox_data, 1, SPE_MBOX_ANY_NONBLOCKING);
	}

	/* Wait for SPU-thread to complete execution. */
	for (i = 0; i < args->num_spus; i++) {
		if (pthread_join(threads[i], NULL)) {
			perror("Failed pthread_join");
			exit(1);
		}

		/* Deregister event */
		if (spe_event_handler_deregister(event_handler, &events[i]) < 0) {
			perror("Failed event deregister.");
			exit(1);
		}

		/* Destroy context */
		if (spe_context_destroy(ctxs[i]) != 0) {
			perror("Failed destroying context");
			exit(1);
		}
	}
	gettimeofday(&t2, NULL);

	/* Destory event handler */
	if (spe_event_handler_destroy(event_handler) < 0) {
		perror("Failed destroying event handler.");
		exit(1);
	}

	write_cmp(args->cmp_file, &c_image);
	write_pgm(args->out_file, &image2);

	free_cmp(&c_image);
	free_pgm(&image);
	free_pgm(&image2);

	free(spu_event_args);
	free(ctxs);
	free(events);
	free(threads);

	*scale_time += GET_TIME_DELTA(t1, t2);
}

int main(int argc, char **argv)
{
	struct cli_args args;
	struct timeval t1, t2;
	double total_time = 0, scale_time = 0;

	if (argc != 7) {
		printf("Usage: %s mod_vect mod_dma num_spus in.pgm out.cmp out.pgm\n", argv[0]);
		return 0;
	}

	gettimeofday(&t1, NULL);

	args.mod_vect = atoi(argv[1]);
	args.mod_dma = atoi(argv[2]);
	args.num_spus = atoi(argv[3]);
	args.in_file = argv[4];
	args.cmp_file = argv[5];
	args.out_file = argv[6];

	if (args.num_spus > spe_cpu_info_get(SPE_COUNT_USABLE_SPES, -1)) {
		fprintf(stderr, "Can't create %d SPUs.\n", args.num_spus);
		return 0;
	}

	run(&args, &scale_time);

	gettimeofday(&t2, NULL);

	total_time += GET_TIME_DELTA(t1, t2);

	printf("Compress / Decompress time: %lf\n", scale_time);
	printf("Total time: %lf\n", total_time);

	return 0;
}
