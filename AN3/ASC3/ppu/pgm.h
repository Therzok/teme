#ifndef PGM_H_
#define PGM_H_

#include "../common/structs.h"
#include "utils.h"

//read_pgm
void read_pgm(char *path, struct img *in_img);
void write_pgm(char *path, struct img *out_img);
void free_pgm(struct img *out_img);

#endif
