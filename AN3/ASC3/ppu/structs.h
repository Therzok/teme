#ifndef STRUCTS_H_
#define STRUCTS_H_

//regular image
struct img {
	unsigned char *pixels;
	int width, height;
};

struct block {
	//index matrix for the pixels in the block
	unsigned char index_matrix[BLOCK_SIZE * BLOCK_SIZE];

	//min and max values for the block
	unsigned char min, max;
};

struct c_img {
	//compressed image
	struct block *blocks;
	int width, height;
};

struct nibbles {
	unsigned first_nibble : 4;
	unsigned second_nibble: 4;
};

#endif

