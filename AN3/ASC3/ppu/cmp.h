#ifndef CMP_H_
#define CMP_H_

#include "../common/structs.h"
#include "utils.h"

void write_cmp(char *path, struct c_img *out_img);
void free_cmp(struct c_img *out_img);

#endif
