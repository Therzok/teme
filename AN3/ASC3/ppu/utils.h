#ifndef UTILS_H_
#define UTILS_H_

#define BUF_SIZE 		256

//utils
void *_alloc(int size);
void *_alloc_align(int size, int alignment);
void _read_buffer(int fd, void *buf, int size);
void _write_buffer(int fd, void *buf, int size);
int _open_for_write(char *path);
int _open_for_read(char *path);
void read_line(int fd, char *path, char *buf, int buf_size);
#endif
