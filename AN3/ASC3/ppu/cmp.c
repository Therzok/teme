#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libmisc.h>

#include "cmp.h"

void write_cmp(char *path, struct c_img *out_img)
{
	int i, nr_blocks, j, fd, k, file_size;
	char *buf, small_buf[BUF_SIZE];
	struct nibbles nb;

	fd = _open_for_write(path);

	//write width and height
	sprintf(small_buf, "%d\n%d\n", out_img->width, out_img->height);
	_write_buffer(fd, small_buf, strlen(small_buf));

	nr_blocks = out_img->width * out_img->height / (BLOCK_SIZE * BLOCK_SIZE);
	file_size = nr_blocks * (2 + BLOCK_SIZE * BLOCK_SIZE / 2);
	buf = _alloc(file_size);

	k = 0;
	for (i=0; i<nr_blocks; i++){
		//write min and max
		buf[k++] = out_img->blocks[i].min;
		buf[k++] = out_img->blocks[i].max;
		//write index matrix
		j = 0;
		while (j < BLOCK_SIZE * BLOCK_SIZE){
			nb.first_nibble = out_img->blocks[i].index_matrix[j++];
			nb.second_nibble = out_img->blocks[i].index_matrix[j++];
			buf[k++] = *((char*) &nb);
		}
	}
	_write_buffer(fd, buf, file_size);

	free(buf);
	close(fd);
}

void free_cmp(struct c_img *image)
{
	free_align(image->blocks);
}
