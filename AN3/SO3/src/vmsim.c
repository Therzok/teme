#include "vmsim.h"
#include "so3.h"

w_boolean_t vmsim_init(void)
{
	return so3_init();
}

w_boolean_t vmsim_cleanup(void)
{
	return so3_cleanup();
}

w_boolean_t vm_alloc(w_size_t num_pages, w_size_t num_frames, vm_map_t *map)
{
	return TRUE;
}

w_boolean_t vm_free(w_ptr_t start)
{
	return TRUE;
}
