#ifndef SO3_H_
#define SO3_H_	1

#include "common.h"
#include "vmsim.h"

w_boolean_t so3_init(void);

w_boolean_t so3_cleanup(void);

w_boolean_t so3_map_virtual(void);

w_boolean_t so3_map_physical(void);

w_boolean_t so3_swap(void);

#endif

