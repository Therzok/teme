set -x

BUILD_DIR=../build-ethash-cuda/
SRC_DIR=../ethash-cuda3/

rm -rf $BUILD_DIR
mkdir $BUILD_DIR
cd $BUILD_DIR
cmake $SRC_DIR
make Benchmark_CUDA

