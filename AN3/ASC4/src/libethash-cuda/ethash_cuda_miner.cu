/*
  This file is part of c-ethash.

  c-ethash is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  c-ethash is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with cpp-ethereum.  If not, see <http://www.gnu.org/licenses/>.
*/
/** 
* @author Tim Hughes <tim@twistedfury.com>
* @date 2015
*
* CUDA port by Grigore Lupescu <grigore.lupescu@gmail.com>
*/

#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <cstdlib>
#include <assert.h>
#include <queue>
#include <vector>
#include <libethash/util.h>
#include <string>
#include "ethash_cuda_miner.h"

#include <vector_types.h>
#include <cuda_runtime_api.h>
#include <cuda.h>

#define CASSERT(rc) do { cudaError_t err = rc; assert(err == cudaSuccess); } while(0)

#define ETHASH_BYTES 32

#undef min
#undef max

#define DAG_SIZE 262688
#define MAX_OUTPUTS 63
#define GROUP_SIZE 64
#define ACCESSES 64

/* APPLICATION SETTINGS */
#define __ENDIAN_LITTLE__	1

#define THREADS_PER_HASH (8)
#define HASHES_PER_LOOP (GROUP_SIZE / THREADS_PER_HASH)

#define FNV_PRIME	0x01000193

__constant__ uint2 Keccak_f1600_RC[24] = {
	{0x00000001, 0x00000000},
	{0x00008082, 0x00000000},
	{0x0000808a, 0x80000000},
	{0x80008000, 0x80000000},
	{0x0000808b, 0x00000000},
	{0x80000001, 0x00000000},
	{0x80008081, 0x80000000},
	{0x00008009, 0x80000000},
	{0x0000008a, 0x00000000},
	{0x00000088, 0x00000000},
	{0x80008009, 0x00000000},
	{0x8000000a, 0x00000000},
	{0x8000808b, 0x00000000},
	{0x0000008b, 0x80000000},
	{0x00008089, 0x80000000},
	{0x00008003, 0x80000000},
	{0x00008002, 0x80000000},
	{0x00000080, 0x80000000},
	{0x0000800a, 0x00000000},
	{0x8000000a, 0x80000000},
	{0x80008081, 0x80000000},
	{0x00008080, 0x80000000},
	{0x80000001, 0x00000000},
	{0x80008008, 0x80000000},
};

/*----------------------------------------------------------------------
*	HOST/TARGET FUNCTIONS
*---------------------------------------------------------------------*/
__device__ inline uint2 r32(const uint2 &a, uint s1, uint s2)
{
	return make_uint2(a.x << s1 | a.y >> s2, a.y << s1 | a.x >> s2);
}

__device__ inline uint2 l32(const uint2 &a, uint s1, uint s2)
{
	return make_uint2(a.y << s1 | a.x >> s2, a.x << s1 | a.y >> s2);
}

__device__ inline uint2 operator&(const uint2 &a, const uint2 &b)
{
	return make_uint2(a.x & b.x, a.y & b.y);
}

__device__ inline uint2 operator^(const uint2 &a, const uint2 &b)
{
	return make_uint2(a.x ^ b.x, a.y ^ b.y);
}

__device__ inline uint2 operator|(const uint2 &a, const uint2 &b)
{
	return make_uint2(a.x | b.x, a.y | b.y);
}

__device__ inline uint2 operator~(const uint2 &a)
{
	return make_uint2(~a.x, ~a.y);
}

__device__ inline uint2& operator^=(uint2 &a, const uint2 &b)
{
	a.x ^= b.x; a.y ^= b.y; return a;
}

__device__ inline uint2 bsel(const uint2 &a, const uint2 &b, const uint2 &c)
{
	return (a & (~c)) | (b & c);
}


#define KECCAK_INIT(a, b, start) \
	b[start] = a[start] ^ a[start+5] ^ a[start+10] ^ a[start+15] ^ a[start+20]

#define KECCAK_ROUND(t, a, b, a_start, b_start, idx) \
	t = b[b_start] ^ r32(b[idx], 1, 31); \
	a[a_start] ^= t; \
	a[a_start+5] ^= t; \
	a[a_start+10] ^= t; \
	a[a_start+15] ^= t; \
	a[a_start+20] ^= t

#define KECCAK_RHO_PI_ROUND_R(a, b, aidx, bidx, s1, s2) \
	b[bidx] = r32(a[aidx], s1, s2)

#define KECCAK_RHO_PI_ROUND_L(a, b, aidx, bidx, s1, s2) \
	b[bidx] = l32(a[aidx], s1, s2)

#define KECCAK_CHI_ROUND(a, b, i1, i2, i3) \
	a[i1] = bsel(b[i1] ^ b[i2], b[i1], b[i3])

/******************************************
* FUNCTION: keccak_f1600_round
*******************************************/
__device__ void keccak_f1600_round(uint2* a, uint r, uint out_size)
{
	#if !__ENDIAN_LITTLE__
		for (uint i = 0; i != 25; ++i)
			a[i] = make_uint2(a[i].y, a[i].x);
	#endif

	/*** TODO - ASCHW4 **************************
	* Implementare Keccak/SHA3 pornind de la 
	* resurse SHA3, cod OpenCL/C/Python/Go
	*********************************************/
	uint2 t, b[25];
	KECCAK_INIT(a, b, 0);
	KECCAK_INIT(a, b, 1);
	KECCAK_INIT(a, b, 2);
	KECCAK_INIT(a, b, 3);
	KECCAK_INIT(a, b, 4);

	KECCAK_ROUND(t, a, b, 0, 4, 1);
	KECCAK_ROUND(t, a, b, 1, 0, 2);
	KECCAK_ROUND(t, a, b, 2, 1, 3);
	KECCAK_ROUND(t, a, b, 3, 2, 4);
	KECCAK_ROUND(t, a, b, 4, 3, 0);

	b[0] = a[0];
	KECCAK_RHO_PI_ROUND_R(a, b, 1, 10, 1, 31);
	KECCAK_RHO_PI_ROUND_R(a, b, 10, 7, 3, 29);
	KECCAK_RHO_PI_ROUND_R(a, b, 7, 11, 6, 26);
	KECCAK_RHO_PI_ROUND_R(a, b, 11, 17, 10, 22);
	KECCAK_RHO_PI_ROUND_R(a, b, 17, 18, 15, 17);
	KECCAK_RHO_PI_ROUND_R(a, b, 18, 3, 21, 11);
	KECCAK_RHO_PI_ROUND_R(a, b, 3, 5, 28, 4);
	KECCAK_RHO_PI_ROUND_L(a, b, 5, 16, 4, 28);
	KECCAK_RHO_PI_ROUND_L(a, b, 16, 8, 13, 19);
	KECCAK_RHO_PI_ROUND_L(a, b, 8, 21, 23, 9);
	KECCAK_RHO_PI_ROUND_R(a, b, 21, 24, 2, 30);
	KECCAK_RHO_PI_ROUND_R(a, b, 24, 4, 14, 18);
	KECCAK_RHO_PI_ROUND_R(a, b, 4, 15, 27, 5);
	KECCAK_RHO_PI_ROUND_L(a, b, 15, 23, 9, 23);
	KECCAK_RHO_PI_ROUND_L(a, b, 23, 19, 24, 8);
	KECCAK_RHO_PI_ROUND_R(a, b, 19, 13, 8, 24);
	KECCAK_RHO_PI_ROUND_R(a, b, 13, 12, 25, 7);
	KECCAK_RHO_PI_ROUND_L(a, b, 12, 2, 11, 21);
	KECCAK_RHO_PI_ROUND_L(a, b, 2, 20, 30, 2);
	KECCAK_RHO_PI_ROUND_R(a, b, 20, 14, 18, 14);
	KECCAK_RHO_PI_ROUND_L(a, b, 14, 22, 7, 25);
	KECCAK_RHO_PI_ROUND_L(a, b, 22, 9, 29, 3);
	KECCAK_RHO_PI_ROUND_R(a, b, 9, 6, 20, 12);
	KECCAK_RHO_PI_ROUND_L(a, b, 6, 1, 12, 20);

	KECCAK_CHI_ROUND(a, b, 0, 2, 1);
	KECCAK_CHI_ROUND(a, b, 1, 3, 2);
	KECCAK_CHI_ROUND(a, b, 2, 4, 3);
	KECCAK_CHI_ROUND(a, b, 3, 0, 4);
	if (out_size < 4)
		goto done;

	KECCAK_CHI_ROUND(a, b, 4, 1, 0);
	KECCAK_CHI_ROUND(a, b, 5, 7, 6);
	KECCAK_CHI_ROUND(a, b, 6, 8, 7);
	KECCAK_CHI_ROUND(a, b, 7, 9, 8);
	KECCAK_CHI_ROUND(a, b, 8, 5, 9);
	if (out_size < 8)
		goto done;

	KECCAK_CHI_ROUND(a, b, 9, 6, 5);
	KECCAK_CHI_ROUND(a, b, 10, 12, 11);
	KECCAK_CHI_ROUND(a, b, 11, 13, 12);
	KECCAK_CHI_ROUND(a, b, 12, 14, 13);
	KECCAK_CHI_ROUND(a, b, 13, 10, 14);
	KECCAK_CHI_ROUND(a, b, 14, 11, 10);
	KECCAK_CHI_ROUND(a, b, 15, 17, 16);
	KECCAK_CHI_ROUND(a, b, 16, 18, 17);
	KECCAK_CHI_ROUND(a, b, 17, 19, 18);
	KECCAK_CHI_ROUND(a, b, 18, 15, 19);
	KECCAK_CHI_ROUND(a, b, 19, 16, 15);
	KECCAK_CHI_ROUND(a, b, 20, 22, 21);
	KECCAK_CHI_ROUND(a, b, 21, 23, 22);
	KECCAK_CHI_ROUND(a, b, 22, 24, 23);
	KECCAK_CHI_ROUND(a, b, 23, 20, 24);
	KECCAK_CHI_ROUND(a, b, 24, 21, 20);

done:
	a[0] ^= Keccak_f1600_RC[r];

	#if !__ENDIAN_LITTLE__
		for (uint i = 0; i != 25; ++i)
			a[i] = make_uint2(a[i].y, a[i].x);
	#endif
}

/******************************************
* FUNCTION: keccak_f1600_no_absorb
*******************************************/
__device__ void keccak_f1600_no_absorb(ulong* a, uint in_size, uint out_size, uint isolate)
{
	
	for (uint i = in_size; i != 25; ++i)
	{
		a[i] = 0;
	}

#if __ENDIAN_LITTLE__
	a[in_size] = a[in_size] ^ 0x0000000000000001;
	a[24-out_size*2] = a[24-out_size*2] ^ 0x8000000000000000;
#else
	a[in_size] = a[in_size] ^ 0x0100000000000000;
	a[24-out_size*2] = a[24-out_size*2] ^ 0x0000000000000080;
#endif

	// Originally I unrolled the first and last rounds to interface
	// better with surrounding code, however I haven't done this
	// without causing the AMD compiler to blow up the VGPR usage.
	uint r = 0;
	do
	{
		// This dynamic branch stops the AMD compiler unrolling the loop
		// and additionally saves about 33% of the VGPRs, enough to gain another
		// wavefront. Ideally we'd get 4 in flight, but 3 is the best I can
		// massage out of the compiler. It doesn't really seem to matter how
		// much we try and help the compiler save VGPRs because it seems to throw
		// that information away, hence the implementation of keccak here
		// doesn't bother.
		if (isolate)
		{
			/*** TODO - ASCHW4: call to keccak_f1600_round */
			keccak_f1600_round((uint2*)a, r++, 25);
		}
	}
	while (r < 23);

	// final round optimised for digest size
	/*** TODO - ASCHW4: call to keccak_f1600_round */
	keccak_f1600_round((uint2*)a, r++, out_size);
}

#define copy(dst, src, count) for (uint i = 0; i != count; ++i) { (dst)[i] = (src)[i]; }

#define countof(x) (sizeof(x) / sizeof(x[0]))

__device__ uint fnv(uint x, uint y)
{
	return x * FNV_PRIME ^ y;
}

__device__ uint4 fnv4(uint4 a, uint4 b)
{
	uint4 res;

	res.x = a.x * FNV_PRIME ^ b.x;
	res.y = a.y * FNV_PRIME ^ b.y;
	res.z = a.z * FNV_PRIME ^ b.z;
	res.w = a.w * FNV_PRIME ^ b.w;

	return res;
}

__device__ uint fnv_reduce(uint4 v)
{
	return fnv(fnv(fnv(v.x, v.y), v.z), v.w);
}

typedef union
{
	ulong ulongs[32 / sizeof(ulong)];
	uint uints[32 / sizeof(uint)];
} hash32_t;

typedef union
{
	ulong ulongs[64 / sizeof(ulong)];
	uint4 uint4s[64 / sizeof(uint4)];
} hash64_t;

typedef union
{
	uint uints[128 / sizeof(uint)];
	uint4 uint4s[128 / sizeof(uint4)];
} hash128_t;

/******************************************
* FUNCTION: init_hash
*******************************************/
__device__ hash64_t init_hash(hash32_t const* header, ulong nonce, uint isolate)
{
	hash64_t init;
	uint const init_size = countof(init.ulongs);
	uint const hash_size = countof(header->ulongs);

	// sha3_512(header .. nonce)
	ulong state[25];
	copy(state, header->ulongs, hash_size);
	state[hash_size] = nonce;
	keccak_f1600_no_absorb(state, hash_size + 1, init_size, isolate);

	copy(init.ulongs, state, init_size);
	return init;
}

/******************************************
* FUNCTION: inner_loop
*******************************************/
__device__ uint inner_loop(uint4 init, uint thread_id, uint* share, hash128_t const* g_dag, uint isolate)
{
	uint4 mix = init;

	// share init0
	if (thread_id == 0)
		*share = mix.x;

	/*** TODO - ASCHW4: uncomment when function qualifiers are OK!! */
	__syncthreads();
	uint init0 = *share;

	uint a = 0;
	do
	{
		bool update_share = thread_id == (a/4) % THREADS_PER_HASH;

		#pragma unroll
		for (uint i = 0; i != 4; ++i)
		{
			if (update_share)
			{
				uint m[4] = { mix.x, mix.y, mix.z, mix.w };
				*share = fnv(init0 ^ (a+i), m[i]) % DAG_SIZE;
			}
			/*** TODO - ASCHW4: uncomment when function qualifiers are OK!! */
			__syncthreads();

			mix = fnv4(mix, g_dag[*share].uint4s[thread_id]);
		}
	}
	while ((a += 4) != (ACCESSES & isolate));

	return fnv_reduce(mix);
}

/******************************************
* FUNCTION: final_hash
*******************************************/
__device__ hash32_t final_hash(hash64_t const* init, hash32_t const* mix, uint isolate)
{
	ulong state[25];

	hash32_t hash;
	uint const hash_size = countof(hash.ulongs);
	uint const init_size = countof(init->ulongs);
	uint const mix_size = countof(mix->ulongs);

	// keccak_256(keccak_512(header..nonce) .. mix);
	copy(state, init->ulongs, init_size);
	copy(state + init_size, mix->ulongs, mix_size);
	keccak_f1600_no_absorb(state, init_size+mix_size, hash_size, isolate);

	// copy out
	copy(hash.ulongs, state, hash_size);
	return hash;
}


typedef union
{
	struct
	{
		hash64_t init;
		uint pad; // avoid lds bank conflicts
	};
	hash32_t mix;
} compute_hash_share;

/******************************************
* FUNCTION: compute_hash_simple
* INFO: no optimisations
*******************************************/
__device__ hash32_t compute_hash_simple(
	hash32_t const* g_header,
	hash128_t const* g_dag,
	ulong nonce,
	uint isolate
	)
{
	hash64_t init = init_hash(g_header, nonce, isolate);

	hash128_t mix;
	for (uint i = 0; i != countof(mix.uint4s); ++i)
	{
		mix.uint4s[i] = init.uint4s[i % countof(init.uint4s)];
	}
	
	uint mix_val = mix.uints[0];
	uint init0 = mix.uints[0];
	uint a = 0;
	do
	{
		uint pi = fnv(init0 ^ a, mix_val) % DAG_SIZE;
		uint n = (a+1) % countof(mix.uints);

		for (uint i = 0; i != countof(mix.uints); ++i)
		{
			mix.uints[i] = fnv(mix.uints[i], g_dag[pi].uints[i]);
			mix_val = i == n ? mix.uints[i] : mix_val;
		}
	}
	while (++a != (ACCESSES & isolate));

	// reduce to output
	hash32_t fnv_mix;
	for (uint i = 0; i != countof(fnv_mix.uints); ++i)
	{
		fnv_mix.uints[i] = fnv_reduce(mix.uint4s[i]);
	}
	
	return final_hash(&init, &fnv_mix, isolate);
}

__device__ hash32_t compute_hash_shared(
	hash32_t const* g_header,
	hash128_t const* g_dag,
	ulong nonce,
	uint isolate,
	compute_hash_share *share
	)
{
	uint const gid = blockIdx.x * blockDim.x + threadIdx.x;
	uint const tid = gid % THREADS_PER_HASH;
	uint const hid = (gid % GROUP_SIZE) / THREADS_PER_HASH;
	hash64_t init = init_hash(g_header, nonce, isolate);
	hash32_t mix;
	const int size = 64 / sizeof(uint4);

	uint i = 0;
	do {
		// Only one thread will do this set in a loop. No need for sync.
		if (i == tid)
			share[hid].init = init;

		// Inner loop syncs inside. No need for pre-barrier + above comment.
		share[hid].mix.uints[tid] = inner_loop(
			share[hid].init.uint4s[tid % size],
			tid,
			share[hid].mix.uints,
			g_dag,
			isolate
		);

		// We need to sync here. Otherwise, we can race on this iteration.
		__syncthreads();

		// Only one thread will do this set in a loop.
		if (i == tid)
			mix = share[hid].mix;

		// We need to sync here. Otherwise, we can race on next iteration.
		__syncthreads();
	} while (++i != (THREADS_PER_HASH & isolate));

	return final_hash(&init, &mix, isolate);
}

/******************************************
* FUNCTION: ethash_hash
*******************************************/
__global__ void ethash_hash(
	hash32_t* g_hashes,
	hash32_t const* g_header,
	hash128_t const* g_dag,
	ulong start_nonce,
	uint isolate
	)
{
	__shared__ compute_hash_share share[HASHES_PER_LOOP];

	/*** TODO - ASCHW4: compute global thread id from blockIdx, blockDim and threadIdx*/
	const uint gid = blockIdx.x * blockDim.x + threadIdx.x;
	g_hashes[gid] = compute_hash_shared(g_header, g_dag, start_nonce + gid, isolate, share);
}

/******************************************
* FUNCTION: ethash_search
*******************************************/
__global__ void ethash_search(
	uint* g_output,
	 hash32_t const* g_header,
	hash128_t const* g_dag,
	ulong start_nonce,
	ulong target,
	uint isolate
	)
{
	__shared__ compute_hash_share share[HASHES_PER_LOOP];

	/*** TODO - ASCHW4: compute global thread id from blockIdx, blockDim and threadIdx*/
	const uint gid = blockIdx.x * blockDim.x + threadIdx.x;
	hash32_t hash = compute_hash_shared(g_header, g_dag, start_nonce + gid, isolate, share);

	if (hash.ulongs[countof(hash.ulongs)-1] < target)
	{
		/*** TODO - ASCHW4: use atomicInc when function qualifiers are OK !!! */
		uint slot = min(MAX_OUTPUTS, atomicInc(&g_output[0], 1) + 1);
		//uint slot = min(MAX_OUTPUTS, (++g_output[0]) + 1);
		g_output[slot] = gid;
	}
}

/*----------------------------------------------------------------------
*	HOST ONLY FUNCTIONS
*---------------------------------------------------------------------*/
__host__ ethash_cuda_miner::ethash_cuda_miner()
{
}

__host__ void ethash_cuda_miner::finish()
{
}

/******************************************
* FUNCTION: init
*******************************************/
__host__ bool ethash_cuda_miner::init(ethash_params const& params, ethash_h256_t const *seed, unsigned workgroup_size)
{
	// store params
	m_params = params;

	// use requested workgroup size, but we require multiple of 8
	m_workgroup_size = ((workgroup_size + 7) / 8) * 8;

	// create buffer for dag
	/*** TODO - ASCHW4: Allocate using cudaMalloc for m_dag, size m_params.full_size */
	CASSERT(cudaMalloc((void**)&m_dag, m_params.full_size));

	// create buffer for header
	/*** TODO - ASCHW4: Allocate using cudaMalloc for m_header, size 32 */
	CASSERT(cudaMalloc((void**)&m_header, 32));

	// compute dag on CPU
	{
		void* cache_mem = malloc(m_params.cache_size + 63);
		ethash_cache cache;
		cache.mem = (void*)(((uintptr_t)cache_mem + 63) & ~63);
		ethash_mkcache(&cache, &m_params, seed);

		// if this throws then it's because we probably need to subdivide the dag uploads for compatibility
		char* dag_ptr = (char*) malloc(m_params.full_size);
		ethash_compute_full_data(dag_ptr, &m_params, &cache);

		/*** TODO - ASCHW4: Copy memory RAM->VRAM, SRC:dag_ptr, DST:m_dag, SIZE:m_params.full_size */
		CASSERT(cudaMemcpy(m_dag, dag_ptr, m_params.full_size, cudaMemcpyHostToDevice));
		
		delete[] dag_ptr;

		free(cache_mem);
	}

	// create mining buffers
	for (unsigned i = 0; i != c_num_buffers; ++i)
	{
		/*** TODO - ASCHW4: Allocate memory on device/VRAM
		* m_hash_buf[i], SIZE: 32*c_hash_batch_size
		* m_search_buf[i], SIZE: (c_max_search_results + 1) * sizeof(uint32_t) */
		CASSERT(cudaMalloc((void**)(&(m_hash_buf[i])), 32 * c_hash_batch_size));
		CASSERT(cudaMalloc((void**)(&(m_search_buf[i])), (c_max_search_results + 1) * sizeof(uint32_t)));
	}
	return true;
}

/******************************************
* FUNCTION: hash
*******************************************/
struct pending_batch
{
	unsigned base;
	unsigned count;
	unsigned buf;
};

__host__ void ethash_cuda_miner::hash(uint8_t* ret, uint8_t const* header, uint64_t nonce, unsigned count)
{
	std::queue<pending_batch> pending;

	/*** TODO - ASCHW4: Copy memory RAM->VRAM, SRC:header, DST:m_header, SIZE:32 */
	CASSERT(cudaMemcpy(m_header, header, 32, cudaMemcpyHostToDevice));

	unsigned buf = 0;
	dim3 threads_per_block(m_workgroup_size, 1, 1);
	for (unsigned i = 0; i < count || !pending.empty(); )
	{
		// how many this batch
		if (i < count)
		{
			unsigned const this_count = std::min(count - i, c_hash_batch_size);
			unsigned const batch_count = std::max(this_count, m_workgroup_size);

			pending_batch temp_pending_batch;
			temp_pending_batch.base = i;
			temp_pending_batch.count = this_count;
			temp_pending_batch.buf = buf;

			// execute it!
			/*** TODO - ASCHW4: call to ethash_hash */
			/* EXEC batch_count instances, 
			* ARGS to pass: m_hash_buf[buf], m_header, m_dag, nonce, ~0U */
			dim3 blocks(batch_count / m_workgroup_size, 1, 1);
			ethash_hash<<<blocks, threads_per_block>>>(
				(hash32_t *)m_hash_buf[buf],
				(const hash32_t *)m_header,
				(const hash128_t *)m_dag,
				nonce,
				~0U
			);

			pending.push(temp_pending_batch);
			i += this_count;
			buf = (buf + 1) % c_num_buffers;
		}

		// read results
		if (i == count || pending.size() == c_num_buffers)
		{
			pending_batch const& batch = pending.front();

			// could use pinned host pointer instead, but this path isn't that important.
			/*** TODO - ASCHW4: Copy memory VRAM->RAM, SRC:m_hash_buf[batch.buf], 
			* DST:ret + batch.base*ETHASH_BYTES, SIZE:batch.count*ETHASH_BYTES */
			CASSERT(cudaMemcpy(
				ret + batch.base * ETHASH_BYTES,
				m_hash_buf[batch.buf],
				batch.count * ETHASH_BYTES,
				cudaMemcpyDeviceToHost
			));

			pending.pop();
		}
	}
}

/******************************************
* FUNCTION: search
*******************************************/
struct pending_batch_search
{
	uint64_t start_nonce;
	unsigned buf;
};

__host__ void ethash_cuda_miner::search(uint8_t const* header, uint64_t target, search_hook& hook)
{
	std::queue<pending_batch_search> pending;
	static uint32_t const c_zero = 0;
	uint32_t* results = (uint32_t*)malloc((1+c_max_search_results) * sizeof(uint32_t));

	// update header constant buffer
	/*** TODO - ASCHW4: Copy memory RAM->VRAM, SRC:header, DST:m_header, SIZE:32 */
	CASSERT(cudaMemcpy(m_header, header, 32, cudaMemcpyHostToDevice));

	for (unsigned i = 0; i != c_num_buffers; ++i)
		CASSERT(cudaMemcpy(m_search_buf[i], &c_zero, 4, cudaMemcpyHostToDevice));
 
	unsigned buf = 0;

	dim3 search_blocks(c_search_batch_size / m_workgroup_size, 1, 1);
	dim3 threads_per_block(m_workgroup_size, 1, 1);
	for (uint64_t start_nonce = 0; ; start_nonce += c_search_batch_size)
	{
		/*** TODO - ASCHW4: call to ethash_search */
		/* EXEC c_search_batch_size instances, 
		* ARGS to pass: m_search_buf[buf], m_header, m_dag, start_nonce, target, ~0U */
		ethash_search<<<search_blocks, threads_per_block>>>(
			(uint *)m_search_buf[buf],
			(const hash32_t *)m_header,
			(const hash128_t *)m_dag,
			start_nonce,
			target,
			~0U
		);

		pending_batch_search temp_pending_batch;
		temp_pending_batch.start_nonce = start_nonce;
		temp_pending_batch.buf = buf;

		pending.push(temp_pending_batch);
		buf = (buf + 1) % c_num_buffers;
		
		// read results
		if (pending.size() == c_num_buffers)
		{
			pending_batch_search const& batch = pending.front();

			// could use pinned host pointer instead
			/*** TODO - ASCHW4: Copy memory VRAM->RAM, SRC:m_search_buf[batch.buf], 
			* DST:results, SIZE:(1+c_max_search_results) * sizeof(uint32_t) */
			CASSERT(cudaMemcpy(
				results,
				m_search_buf[batch.buf],
				(1 + c_max_search_results) * sizeof(uint32_t),
				cudaMemcpyDeviceToHost
			));
			
			unsigned num_found = std::min(results[0], c_max_search_results);

			uint64_t nonces[c_max_search_results];
			for (unsigned i = 0; i != num_found; ++i)
				nonces[i] = batch.start_nonce + results[i+1];

			bool exit = num_found && hook.found(nonces, num_found);
			exit |= hook.searched(batch.start_nonce, c_search_batch_size); // always report searched before exit
			if (exit)
				break;
				
			// end search prematurely due to poor performance
			if(start_nonce == 524288)
				break;

			// reset search buffer if we're still going
			if (num_found)
				CASSERT(cudaMemcpy(m_search_buf[batch.buf], &c_zero, 4, cudaMemcpyHostToDevice));

			pending.pop();
		}
	}
	
	delete[] results;
}

