/* Ungureanu Marius-Stefan 333CC */
/* Normal include */
#include <stdio.h>

/* Inline indented include */
#	include <unistd.h>

/* Indented include */
	#ifdef FAKE_DEF
		void fail_if_show(int b) {}
	#endif

/* Multiline if */
#if defined(MULTILINE1) && \
		defined(MULTILINE2) && \
		defined(MULTILINE3)
		void fail_if_show2() {}
#endif

/* Nested ifdef */
#if DEF2 && DEF1
	void fail_if_show3() {}
#endif

#if DEF1
	void fail_if_show4() {}
#elif DEF2
	void fail_if_show5() {}
#else
	void fail_if_show6() {}
#endif

struct test {
	int x;
	int z;
} my_test;
#pragma test /* asdf \ */

void print(int x) {
	int a[][3] = {
		{ 1, 2, 3 },
		{ 2, 3, 4 },
	};
	int b, c;
	b = c = a[0][2];
	printf("%d", x);
}

void
multiline_func(int a,
			   int b,
			   int c)
{
	unsigned
		long
		int x;
	print (x);
}

/* global var */
int b;

/***
 multistar start end, no star on line
 **/

/* **
 * multiline comment
 */

/* Simple define */
#define max(a, b) ((a > b) ? a : b);

#define INSERT_FUNCTION \
	void suma(int a, int b) { int c; return a + b; }
/* Multi line define */
#define DONOTHING(a) \
		(void)a; \
		(void)a; \
		(void)a; \
		(void)a;

#ifdef DONT_REMOVE
#endif
void fail_if_here();

void fail_if_not_here()
{
	int b;
}
#ifdef DONT_REMOVE
#endif

/* Define in ifdef */
#ifdef DEF1
#define IM_DEFINED
#endif

int suma(int v[], int n)
{	int sum = 0, i;
	for (i = 0; i < n; i++) /* actualizeaza suma*/ sum += v[i];
	return sum;
}

unsigned char test()
{
	int t[] = { 1, 2 };
	char b;
	double x;
	return 0;
}

	#pragma lellel
	/* #false directive hey */
int main()
{
	/**/
	/* empty comment/**//* */
	/*** a* comment ***/ char b[] = "lel */";
	int unsigned long long c;
	int a[] = {1, 2, 3}, i = 0, s;
	int d =
#ifdef TEST
	5
#else
	3
#endif
	;
	// int e;
	while (i < 3)
	{
	s = suma(a, 3);
	printf("%d \n", s);
	i++;
	}
	// asdf
	return 0;
}

void other()
{
	unsigned char c;
}
