/* Ungureanu Marius-Stefan 333CC */
#include <stdio.h>
#include <stdlib.h>
#ifdef TEMA
	#include <math.h>
#endif

typedef struct {
	int x;
	int z;
} str_t;

void functia1(int c, unsigned long b)
{
	unsigned char d;
	int x;

	while (1) {
		if (x != c)
			break;
	}

	return;
}

double mypow(double base, double exp)
{
	return pow(base, exp);
}

#if REMOVE
double removed(unsigned int a)
{
	int a[] = { 2,
		3, 4 };
	int c;
	char v[] = { 'a', 'b', 'c' };
	return 0.0;
}
#endif

int fix_me()
{
	int i;
	for (i = 0; i < 100; ++i)
		return 42;
}

int others()
{
	unsigned long int x;
	do {
		x = 2 > 0 ? 4 : 1;
	} while (x != 1);
	return x;
}

int main()
{
	int a[] = {1, 2, 3}, i = 0, s;
	while (i < 3)
	{
		printf("%d\n", i);
		i++;
	}
	return 0;
}
