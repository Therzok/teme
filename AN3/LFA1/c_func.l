/* Ungureanu Marius-Stefan 333CC */
%{
	/* Variable definition node. */
	typedef struct v_node {
		struct v_node *next;
		char *name;
	} v_node_t;

	/* Type definition node. */
	typedef struct t_node {
		struct t_node *next;
		struct v_node *names;
		char *type;
		int count;
	} t_node_t;

	/* For variable definitions. */
	t_node_t first;
	/* For function definitions. */
	char *p, *f_type, *f_name, buf[256];
	int brace_nest_count, brace_array_count, from_state;

	/* Prints output. */
	void flush_output()
	{
		t_node_t *t;
		v_node_t *v;

		printf("\tvariabile locale: ");
		if (!first.next)
			printf("0");
		else {
			for (t = first.next; t; t = t->next) {
				printf("\n\t\t%d (%s ", t->count, t->type);
				for (v = t->names; v; v = v->next)
					printf("%s%s", v->name, v->next ? ", " : ")");
			}
		}
		printf("\n\n");
	}

	void insert_var(char *type, char *name)
	{
		t_node_t *t;
		v_node_t *v;
		for (t = &first; t->next; t = t->next) {
			/* Not a matched type. */
			if (strcmp(t->next->type, type))
				continue;

			/* Matched a type. */
			++(t->next->count);

			/* Skip all nodes until last. */
			for (v = t->next->names; v->next; v = v->next)
				;

			/* Create a new variable. */
			v->next = (v_node_t *)calloc(1, sizeof(v_node_t));
			v->next->name = strdup(name);
			return;
		}

		/* Create a new variable. */
		v = (v_node_t *)calloc(1, sizeof(v_node_t));
		v->name = strdup(name);

		/* Create a new type node. */
		t->next = (t_node_t *)calloc(1, sizeof(t_node_t));
		t->next->type = strdup(type);
		t->next->count = 1;
		t->next->names = v;
	}

	void init_func()
	{
		t_node_t *t, *tnext;
		v_node_t *v, *vnext;
		for (t = first.next; t; t = tnext) {
			tnext = t->next;
			free(t->type);
			for (v = t->names; v; v = vnext) {
				vnext = v->next;
				free(v->name);
				free(v);
			}
			free(t);
		}
		first.next = NULL;
	}
%}

%option noyywrap
%option stack

	/* Define states. */
	/* Used to check whether in a comment block. */
%x COMMENT
	/* Used to check whether preprocessor directive block. */
%x DIRECTIVE
	/* Used to check whether in a function block. */
%x FUNCTION
	/* Used to check whether in an if directive. */
%x IFDEF
	/* Used to check whether in a declaration block. */
%x DECLARE
	/* Used to check whether in an assignment. */
%x ASSIGN

	/* Valid C program, so don't bother about number of keywords. */
	/* Identifier regex. At least one letter or underscore, possibly brackets. */
IDENT	[a-zA-Z_][a-zA-Z_0-9\[\]]*
	/* KEYWORD is also the possible return type. */
KEYWORD	"char"|"short"|"int"|"long"|"signed"|"unsigned"|"float"|"double"|"void"

%%
	/* Comments state machine. */
<*>\/\/.*			/* Drop comment line. */
					/* Start a comment block and keep state. Can't open comment in comment. */
<INITIAL,FUNCTION,IFDEF,DECLARE,ASSIGN>"/*" yy_push_state(COMMENT);
<COMMENT>"*/"		yy_pop_state();

	/* #ifdef or #if defined state machine. */
<INITIAL,FUNCTION,DECLARE,ASSIGN>"#if"	yy_push_state(IFDEF);
<IFDEF>#endif			yy_pop_state();

	/* General preprocessor state machine. */
					/* Start a preprocessor block and keep state. */
<INITIAL,FUNCTION,DECLARE,ASSIGN>"#"	yy_push_state(DIRECTIVE);
<DIRECTIVE>[^\\].*\n	yy_pop_state();

	/* Function matching part. */
					/* Skip function declarations. */
{KEYWORD}[^);]+\)[ \t\n]*;
{KEYWORD}[^);]+\)	{
					/* Print function header. */
					ECHO;
					init_func();
					f_type = strtok(yytext, "(");

					/* Match at least one whitespace character. */
					p = strrchr(f_type, ' ');
					if (!p)
						p = strrchr(f_type, '\t');
					if (!p)
						p = strrchr(f_type, '\n');

					f_name = malloc(strlen(p));
					strcpy(f_name, p + 1);
					f_type[p - f_type] = 0;

					printf("\n\ttip intors: %s\n", f_type);
					printf("\tnume: %s\n", f_name);
					free(f_name);
					yy_push_state(FUNCTION);
				}

				/* Look for keywords. Any combination */
<FUNCTION>({KEYWORD}[ \t\n]+)+ {
					/* Sanitize name for matching types later. */
					f_type = strtok(yytext, " \t\n");
					strcpy(buf, f_type);
					while ((f_type = strtok(NULL, " \t\n"))) {
						strcat(buf, " ");
						strcat(buf, f_type);
					}
					f_type = buf;
					yy_push_state(DECLARE);
				}
				/* Look for identifiers. */
<DECLARE>{IDENT}	insert_var(f_type, strtok(yytext, " \t\n"));
				/* Semi-colon ends declaration block. */
<DECLARE,ASSIGN>";"	yy_pop_state();
				/* Entering assignment we need to skip it. */
<DECLARE>"="		BEGIN(ASSIGN);
				/* Array nest depth checking. */
<ASSIGN>"{"			++brace_array_count;
<ASSIGN>"}"			--brace_array_count;
				/* Assignment ended on comma only if it's not in array assignment. */
<ASSIGN>","		{
					/* Exited array. */
					if (!brace_array_count)
						BEGIN(DECLARE);
				}
				/* Function nest depth checking. */
<FUNCTION>"{"		++brace_nest_count;
<FUNCTION>"}"	{
					/* Function ended, start matching again. */
					if (!(--brace_nest_count)) {
						flush_output();
						yy_pop_state();
					}
				}
<*>.|\n			/* Anything uninteresting. */
%%

int main(int argc, char *argv[])
{
	if (argc < 2) {
		printf("Usage: ./a.out fisier.c");
		return 1;
	}

	yyin = fopen(argv[1], "r");
	if (!yyin) {
		printf("File \"%s\" does not exist.", argv[1]);
		return 1;
	}

	yylex();
	fclose(yyin);
	return 0;
}

