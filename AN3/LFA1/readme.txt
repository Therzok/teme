Ungureanu Marius-Stefan 333CC
Tema 4 LFA
==========

Timp de rezolvare: 8 ore.

Tema este testata pe clang si gcc, flex 2.5.39.

Tema se compileaza folosind 'make' si se ruleaza folosind './main <fisier_input>'
sau folosing 'make run ARG=<fisier_input'.

Am folosit o lista inlantuita in care tin minte tipul variabilelor declarate,
si in interiorul fiecarui nod o alta lista inlantuita cu numele variabilelor
pentru afisare.

`v_node_t` contine numele unei variabile si legatura catre urmatorul nod, iar
`t_node_t` contine numarul de variabile de tipul respectiv, numele tipului,
lista de variabile si legatura catre urmatorul tip.

`first` este un nod de tip santinela catre lista de declaratii, `f_type`,
`f_name`, `p`, `buf` fiind pointeri ajutatori pentru prelucrarea sirurilor de
keywords complexe.

Cu ajutorul lui `brace_nest_count` si `brace_array_count` tin minte nivelul
de imbricare al blocurilor in functie sau a atribuirilor in vectori
(uni/multidimensionali).

Functii:
	`flush_output` afiseaza la ecran informatiile culese din functia parsata
	anterior.

	`insert_var` introduce un nod in lista inlantuita cautand un nod cu acelasi
	tip in lista de tipuri, altfel creeand unul nou. In ambele cazuri, se va
	insera un nod nou in lista de variabile.

	`init_func` elibereaza memoria pentru elementele din lista si seteaza primul
	element non-santinela ca NULL.

Sunt activate optiunile: noyywrap si stack (pentru stiva de stari).

Sunt definite 6 stari:
	COMMENT: Pentru blockurile de comentarii /* */.
	DIRECTIVE: Pentru orice directiva de preprocesor.
	FUNCTION: Pentru block de functie.
	IFDEF: Pentru block #ifdef/#if defined
	DECLARE: Pentru block de declaratii.
	ASSIGN: Pentru block de atribuire.

Sunt definite 2 aliasuri:
	IDENT: identificator C, cel putin o litera/underscore urmat de oricate litere,
	underscoruri, numere sau paranteze patrate.
	KEYWORD: keyword predefinit C pentru tipuri.

Tot ce nu este interesant de parsat este fallback la sfarsit, .|\n este aruncat.

Am folosit stiva de stari pentru a nu avea probleme in cazul unor cazuri mai
complexe cum ar fi functie cu ifdef care contin comentarii.

Mai intai sunt verificate comentariile, comentariile single line fiind matchuite
de orice dupa un '//'. Comentariile multi-line sunt realizate cu un automat,
pentru a nu avea probleme cu modul de matchuire al FLEX-ului.

De exemplu, /* */ int x; printf("*/"); ar fi matchuit prost in cazul nefolosirii
unui automat.

Directiva #if este realizata cu automat, pentru a nu avea probleme cu modul de
matchuire al FLEX-ului.

De exemplu,
#if 0
#endif
void fail_if_not_here()
{
	int x;
}
#if 0
#endif

Este cod valid, iar in cazul nefolosirii automatului, fail_if_not_here nu ar fi
analizata in cadrului automatului de functie.

Orice alta directiva este tratata in starea DIRECTIVE, unde se ignora liniile
pana cand nu se mai termina linia cu '\'.

Prototipurile functiilor sunt ignorate. Altfel, orice block de functie gasit
este parsat si sanitizat pentru o afisare ulterioara. Se foloseste lista
inlantuita definita mai sus pentru a tine minte declaratiile.

In cadrul definitiilor de functie, orice combinatie de keyworduri incepe
un block de tip declaratie. In cadrul declaratiilor, putem gasi un identificator
urmat poate de un block de atribuire. La finalul atribuirii, virgula separa
identificatorii.

In cadrul atribuirii, tinem minte nivelul de imbricare al acoladelor pentru
cazul initializarii vectorului. Virgula este folosita ca separator in cadrul
acestui block, nu pentru trecerea la urmatorul identificator.

In cadrul functiei main, fac redirectarea fisierului dat ca primul argument sa
inlocuiasca bufferul de input al flex-ului si aloc memorie pentru nodul santinela.

Am ales ignorarea functiilor in cadrul directivelor de preprocesor si al
comentariilor. Deasemenea ignor functiile fara corp.

Exista doua fisiere de test, fis.c si fis2.c care trateaza toate cazurile la
care m-am gandit ca ar putea avea probleme.
