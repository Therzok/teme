from threading import Thread

class NodeThread(Thread):
    def __init__(self, task, in_slices, out_slices, nodes, semaphore):
        """
        Constructor.

        @type task: Task
        @param task: The task to be run.

        @type in_slices: List of (Integer, Integer, Integer)
        @param in_slices: a list of the slices of data that need to be
            gathered for the task; each tuple specifies the id of a node
            together with the starting and ending indexes of the slice; the
            ending index is exclusive

        @type out_slices: List of (Integer, Integer, Integer)
        @param out_slices: a list of slices where the data produced by the
            task needs to be scattered; each tuple specifies the id of a node
            together with the starting and ending indexes of the slice; the
            ending index is exclusive

        @type nodes: List of Node
        @param nodes: the list of nodes to gather data from.

        @type semaphore: Semaphore
        @param semaphore: The semaphore to use to check whether there is an
            available core to run on.
        """

        super(NodeThread, self).__init__()

        self.task = task
        self.in_slices = in_slices
        self.out_slices = out_slices
        self.nodes = nodes
        self.semaphore = semaphore

    def run(self):
        """
        Start the execution of the thread.
        """

        """ Announce thread start """
        self.semaphore.acquire()

        """ Gather data """
        in_data = []

        for x in self.in_slices:
            node = self.nodes[x[0]]
            in_slice = node.get_data()
            in_data.extend(in_slice[x[1] : x[2]])

        """ Run task """
        out_data = self.task.run(in_data)

        """ Scatter data """
        start = 0
        for x in self.out_slices:
            node = self.nodes[x[0]]
            count = x[2] - x[1]
            out_slice = out_data[start : start + count]

            node.set_data_slice(out_slice, x[1])
            start += count

        """ Announce free thread slot """
        self.semaphore.release()

