"""
This module represents a cluster's computational node.

Computer Systems Architecture Course
Assignment 1 - Cluster Activity Simulation
March 2015
"""

from threading import Lock, Semaphore
from barrier import ThreadBarrier
from node_thread import NodeThread

class Node:
    """
    Class that represents a cluster node with computation and storage
    functionalities.
    """

    def __init__(self, node_id, data):
        """
        Constructor.

        @type node_id: Integer
        @param node_id: the unique id of this node; between 0 and N-1

        @type data: List of Integer
        @param data: a list containing this node's data
        """
        self.node_id = node_id
        self.data = data
        self.nodes = None

        """
        Clone of data to perform the addition on.
        """
        self.clone = list(data)

        """
        List of threads that have been queued.
        """
        self.threads = []

        """
        Used for data updating locking.
        """
        self.add_lock = Lock()

        """
        Maximum of 16 threads at a time
        """
        self.semaphore = Semaphore(16)

    def __str__(self):
        """
        Pretty prints this node.

        @rtype: String
        @return: a string containing this node's id
        """
        return "Node %d" % self.node_id

    def set_cluster_info(self, nodes):
        """
        Informs the current node about the other nodes in the cluster.
        Guaranteed to be called before the first call to 'schedule_task'.

        @type nodes: List of Node
        @param nodes: a list containing all the nodes in the cluster
        """
        self.nodes = nodes

        """
        Create a common barrier. Framework guarantees that node 0 is
        initialized first.
        """
        if self.node_id == 0:
            self.barrier = ThreadBarrier(len(nodes))
        else:
            self.barrier = nodes[0].barrier

    def schedule_task(self, task, in_slices, out_slices):
        """
        Schedule task to execute on the node.

        @type task: Task
        @param task: the task object to execute

        @type in_slices: List of (Integer, Integer, Integer)
        @param in_slices: a list of the slices of data that need to be
            gathered for the task; each tuple specifies the id of a node
            together with the starting and ending indexes of the slice; the
            ending index is exclusive

        @type out_slices: List of (Integer, Integer, Integer)
        @param out_slices: a list of slices where the data produced by the
            task needs to be scattered; each tuple specifies the id of a node
            together with the starting and ending indexes of the slice; the
            ending index is exclusive
        """
        thread = NodeThread(task, in_slices, out_slices, self.nodes, self.semaphore)
        self.threads.append(thread)
        thread.start()

    def sync_results(self):
        """
        Wait for scheduled tasks to finish and write results.
        """
        [ t.join() for t in self.threads ]
        self.barrier.wait()
        self.threads = []

        """
        Copy modified clone back into data.
        """
        self.data = list(self.clone)
        self.barrier.wait()

    def set_data_slice(self, data, start):
        """
        Sets the data into a cloned copy.

        @type data: List of Integer
        @param data: A list of elements to be added to (start, count).

        @type start: Integer
        @param start: The start index to add to.
        """

        """
        Acquire addition lock then do addition on items.
        """
        self.add_lock.acquire()

        for x in range(0, len(data)):
            self.clone[start + x] += data[x]

        """
        Release the addition lock.
        """
        self.add_lock.release()

    def get_data(self):
        """
        Return a copy of this node's data.
        """
        return list(self.data)

    def shutdown(self):
        """
        Instructs the node to shutdown (terminate all threads). This method
        is invoked by the tester. This method must block until all the threads
        started by this node terminate.
        """
        [ t.join() for t in self.threads ]

