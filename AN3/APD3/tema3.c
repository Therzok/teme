#include <complex.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include "mpi.h"

int rank, size;

#define NUM_COLORS 256

/* used to check algorithm to apply */
typedef enum {
	AT_MANDELBROT = 0,
	AT_JULIA,
	AT_MAX,
} algorithm_type;

typedef struct {
	algorithm_type type;
	int num_steps;
	double x_min, x_max, y_min, y_max;
	double step;
	double complex julia;
} input_data;

typedef struct {
	int height, width;
	int start_line, end_line;
	int *colors;
} calc_data;

/* number of bytes to send */
int calc_size(int end, int start, int width)
{
	return (end - start) * width;
}

/* mandelbrot specific setting */
void mandelbrot(double complex *z, double complex *c,
				input_data *data, int x, int y)
{
	*z = 0 + 0 * I;
	*c = (data->x_min + data->step * x) + (data->y_min + data->step * y) * I;
}

/* julia specific setting */
void julia(double complex *z, double complex *c,
		   input_data *data, int x, int y)
{
	*z = (data->x_min + data->step * x) + (data->y_min + data->step * y) * I;
	*c = data->julia;
}

/* algorithm specific settings */
void (*algorithms[AT_MAX])(double complex *z, double complex *c,
						   input_data *data, int x, int y) = {
	mandelbrot,	/* AT_MANDELBROT */
	julia,		/* AT_JULIA */
};

/* general algorithm */
void algorithm(input_data *data, calc_data *calc)
{
	int x, y; /* number of steps from resolution */
	int step; /* current steps */
	double complex z, c; /* formula z, c */

	for (y = calc->start_line; y < calc->end_line; ++y) {
		for (x = 0; x < calc->width; ++x) {
			algorithms[data->type](&z, &c, data, x, y);
			step = 0;

			while (cabs(z) < 2 && step < data->num_steps) {
				z = z * z + c;
				++step;
			}
			calc->colors[(y - calc->start_line) * calc->width + x] = step % NUM_COLORS;
		}
	}
}

/* reads data with specific input format */
input_data *input(const char *fin)
{
	input_data *data = (input_data *)malloc(sizeof(input_data));
	double real = 0, imag = 0;

	FILE *f = fopen(fin, "r");
	fscanf(f, "%d", &data->type);
	fscanf(f, "%lf%lf%lf%lf", &data->x_min, &data->x_max, &data->y_min, &data->y_max);
	fscanf(f, "%lf", &data->step);
	fscanf(f, "%d", &data->num_steps);
	if (data->type == AT_JULIA)
		fscanf(f, "%lf%lf", &real, &imag);
	data->julia = real + imag * I;
	fclose(f);

	return data;
}

/* writes data with pgm format */
void output(const char *fout, calc_data *calc)
{
	FILE *f = fopen(fout, "w");
	int i, j;

	fprintf(f, "P2\n%d %d\n%d\n", calc->width, calc->height, NUM_COLORS - 1);
	for (i = calc->height - 1; i >= 0; --i) {
		for (j = 0; j < calc->width; ++j)
			fprintf(f, "%d ", calc->colors[i * calc->width + j]);
		fprintf(f, "\n");
	}

	fclose(f);
}

/* slave specific work */
void slave(calc_data *calc)
{
	/* place start_line, end_line on the first 2 ints */
	int count = calc_size(calc->end_line, calc->start_line, calc->width);
	int *buf = (int *)malloc((count + 2) * sizeof(int));

	buf[0] = calc->start_line;
	buf[1] = calc->end_line;
	memcpy(buf+2, calc->colors, count * sizeof(int));

	/* send data to root */
	MPI_Send(buf, count + 2, MPI_INT, 0, 1, MPI_COMM_WORLD);
	free(buf);
}

/* common work */
void common(input_data *data, calc_data *calc)
{
	/* Comm size is 0 when only 1 process */
	/* broadcast data to all processes */
	MPI_Bcast(data, sizeof(input_data), MPI_CHAR, 0, MPI_COMM_WORLD);

	/* calculate range to process */
	calc->width = abs((data->x_max - data->x_min) / data->step);
	calc->height = abs((data->y_max - data->y_min) / data->step);
	calc->start_line = (calc->height / size) * rank;
	calc->end_line = MIN(calc->start_line + (calc->height / size), calc->height);

	/* Allocate matrix */
	calc->colors = (int *)malloc(
		calc_size(calc->end_line, calc->start_line, calc->width) * sizeof(int));

	/* process data */
	algorithm(data, calc);

	if (rank != 0)
		slave(calc);
}

void master(const char *fin, const char *fout)
{
	MPI_Request *reqs;
	MPI_Status stat;
	int **buf;
	int i, j;
	int start_line, end_line, count;

	input_data *data = input(fin);
	calc_data *calc = (calc_data *)malloc(sizeof(calc_data));

	/* calculate work and process set */
	common(data, calc);
	free(data);

	/* get root's buffer size */
	count = calc_size(calc->end_line, calc->start_line, calc->width) + 2;

	/* buf is guaranteed to be at most count */
	buf = (int **)malloc((size - 1) * sizeof(int *));
	for (i = 0; i < size - 1; ++i)
		buf[i] = (int *)malloc(count * sizeof(int));

	/* initialize output calculation */
	calc_data *out = malloc(sizeof(calc_data));
	out->width = calc->width;
	out->height = calc->height;
	out->colors = (int *)malloc((out->height * out->width * sizeof(int)));

	/* Add root's data */
	memcpy(out->colors, calc->colors,
		   calc_size(calc->end_line, calc->start_line, calc->width) * sizeof(int));

	free(calc);

	reqs = (MPI_Request *)calloc(size - 1, sizeof(MPI_Request));

	/* wait for data from all slaves */
	for (i = 0; i < size - 1; ++i)
		MPI_Irecv(buf[i], count, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &reqs[i]);

	for (i = 0; i < size - 1; ++i) {
		MPI_Waitany(size - 1, reqs, &j, &stat);

		start_line = buf[j][0];
		end_line = buf[j][1];
		memcpy(out->colors + start_line * out->width, buf[j]+2,
			   calc_size(end_line, start_line, out->width) * sizeof(int));
	}
	output(fout, out);

	for (i = 0; i < size - 1; ++i)
		free(buf[i]);
	free(buf);

	free(out);
}

int main(int argc, char **argv)
{
	input_data *data;
	calc_data *calc;

	if (argc < 3)
		return 1;

	/* init */
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	if (rank == 0)
		master(argv[1], argv[2]);
	else {
		data = (input_data *)malloc(sizeof(input_data));
		calc = (calc_data *)malloc(sizeof(calc_data));
		common(data, calc);
		free(data);
		free(calc);
	}

	MPI_Finalize();
	return 0;
}

