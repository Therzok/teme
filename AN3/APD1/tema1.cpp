#include <fstream>
#include <limits>
#include <omp.h>

using namespace std;

// Flags used to define cell properties.
enum CellFlag {
	CF_DEAD		= 0x0, // CF_DEAD means no CF_ALIVE flag.
	CF_ALIVE	= 0x1, // CF_ALIVE means cell is occupied.
	CF_NEXT		= 0x2, // CF_NEXT means it'll be CF_ALIVE in the next gen.
};

class map
{
public:
	map(ifstream &fin, char mtype, int w, int h, int sw, int sh)
		: type(mtype), width(w), height(h), sim_width(sw), sim_height(sh)
	{
		int i, j;

		// Create bordered matrix.
		sh += 2;
		sw += 2;
		matrix = new int*[sh];

		// Allocate memory initialized with 0.
		#pragma omp parallel for private(i)
		for (i = 0; i < sh; ++i)
			matrix[i] = new int[sw]();

		// Only take needed height/width from file.
		height = min(height, sim_height);
		width = min(width, sim_width);

		// Read from file and discard unneeded elements.
		for (i = 1; i <= height; ++i) {
			for (j = 1; j <= width; ++j)
				fin >> matrix[i][j];
			fin.ignore(numeric_limits<streamsize>::max(), '\n');
		}
	}

	~map()
	{
		#pragma omp parallel for
		for (int i = 0; i < sim_height + 2; ++i)
			delete[] matrix[i];
		delete[] matrix;
	}

	void run(int steps)
	{
		for (int i = 0; i < steps; ++i)
			run_generation();
	}

	void finish(char *file)
	{
		normalize_output();

		ofstream fout(file);
		int i, j;

		// Output header: TIP W_harta H_harta W H
		fout << type << ' ' << width << ' ' << height << ' '
			<< sim_width << ' ' << sim_height << '\n';

		// Output matrix.
		for (i = 1; i <= height; ++i) {
			for (j = 1; j <= width; ++j)
				fout << matrix[i][j] << ' ';
			fout << '\n';
		}
		fout.close();
	}

private:
	void normalize_output()
	{
		int i, j;

		// Calculate output file border.
		height = width = 0;
#pragma omp parallel
{
		#pragma omp for private(i, j)
		for (i = 1; i <= sim_height; ++i)
			for (j = 1; j <= sim_width; ++j) {
				if (matrix[i][j] != CF_ALIVE)
					continue;

				// Atomic access only if first check passes.
				if (i > height)
					#pragma omp critical
					{ if (i > height) height = i; }

				// Atomic access only if first check passes.
				if (j > width)
					#pragma omp critical
					{ if (j > width) width = j; }
			}
}
	}

	void run_generation()
	{
		int i, j, count;
		int i1, j1, i2, j2;
#pragma omp parallel
{
		// Mark cells with new spawn flags.
		#pragma omp for private(i, j, count, i1, j1, i2, j2)
		for (i = 1; i <= sim_height; ++i) {
			i1 = i == 1 && type == 'T' ? sim_height : i-1;
			i2 = i == sim_height && type == 'T' ? 1 : i+1;

			for (j = 1; j <= sim_width; ++j) {
				j1 = j == 1 && type == 'T' ? sim_width : j-1;
				j2 = j == sim_width && type == 'T' ? 1 : j+1;

				count =
					(matrix[i1][j1] & CF_ALIVE) +
					(matrix[i1][j] & CF_ALIVE) +
					(matrix[i1][j2] & CF_ALIVE) +
					(matrix[i][j1] & CF_ALIVE) +
					(matrix[i][j2] & CF_ALIVE) +
					(matrix[i2][j1] & CF_ALIVE) +
					(matrix[i2][j] & CF_ALIVE) +
					(matrix[i2][j2] & CF_ALIVE);

				if (!(matrix[i][j] & CF_ALIVE)) {
					if (count == 3)
						matrix[i][j] |= CF_NEXT;
				} else if (count == 2 || count == 3)
					matrix[i][j] |= CF_NEXT;
			}
		}

		// Apply flags to spawn new generation.
		#pragma omp for private(i, j)
		for (i = 1; i <= sim_height; ++i)
			for (j = 1; j <= sim_width; ++j)
				matrix[i][j] = (matrix[i][j] & CF_NEXT) ? CF_ALIVE : CF_DEAD;
}
	}

	char type;
	int width, height, sim_width, sim_height, **matrix;
};

int main(int argc, char *argv[])
{
	char type;
	int w, h, sw, sh;

	// Check argument usage.
	if (argc < 5)
		return 1;

	// Set thread count.
	omp_set_num_threads(atoi(argv[1]));

	// Read file.
	ifstream fin(argv[3]);
	fin >> type >> w >> h >> sw >> sh;

	// Construct initial map.
	map m(fin, type, w, h, sw, sh);
	fin.close();

	// Run through all steps.
	m.run(atoi(argv[2]));

	// Write to file.
	m.finish(argv[4]);
	return 0;
}

