Elemente de grafica pe calculator - Tema 1

	Simplified Geometry Dash
	Ungureanu Marius 333CC

1. Cerinta
Implementarea unui joc propriu de tip Geometry Dash in care actorul (un patrat)
se plimba pe harta evitand obstacole si colectand puncte. Singura actiune
a actorului este saritura si se foloseste de platforme pentru a supravietui.

2. Utilizare
Programul se compileaza folosind Visual Studio 2013 (probabil ultimul update
de la 2012 aduce compatibilitate). Apoi se ruleaza executabilul ori din
interfata Visual Studio, ori din folderul Debug, aplicatia EGC1.exe.

2.1 Fisiere
Nu are parametri de intrare, nici fisiere de intrare.

2.2 Consola
Nu este implementata interfatare cu consola.

2.3 Input Tastatura
[Space] - Se foloseste pentru a (re)incepe un joc/sari in timpul jocului.
[Left][Right] - Se folosesc inainte de inceperea jocului pentru a selecta actorul
[Up][Down] - Schimba dificultatea intre hard si easy in modul menu.
[ESC] - Iese din joc.
[Tab] - Genereaza o noua harta in modul menu.
[A] - Cheat pentru a scadea din vieti.
[S] - Cheat pentru a adauga punctaj.

2.4 Interfata Grafica
[Left Mouse] face acelasi lucru ca si tasta [Space]

3. Implementare
Tema a fost realizata pe Windows 8.1, Visual Studio 2013, cl.exe versiunea 18.00.30723.

Nu au fost folosite alte biblioteci/framework-uri in afara de WINAPI si freeglut.

Am folosit un algoritm de generare a hartii prin stabilirea platformelor si apoi
folosind proprietatile lor sa generez obstacolele si jumpere-le ajutatoare.

Schema generala:
[Deschidere aplicatie]
[Generare elemente de interfata grafica initiale]
[Asteptare input pentru joc] (1)
[Repeta jocul pana ramane jucatorul fara vieti sau castiga]
[(1)]

Clasele sunt dupa tiparul Framework-ului de laborator, de forma:

Color - Un triplet RGB

Point2 - Tuplu X,Y

Text - Wrapper pentru a face display al unui text

Object2 - Obiect de baza pentru celelalte, container-ul de puncte initialize si transformate
|-------Triangle2 - Object2 cu 3 puncte pentru a desena un triunghi
|-------Circle2 - Object2 cu 18 puncte pentru a desena un cerc
|-------Rectangle2 - Object2 cu 4 puncte pentru a desena un dreptunghi
|-------Steag - Object2 cu 4 puncte pentru a desena un steag

Visual2 defineste un container de obiecte Object2 si Text2 care vor fi folosite pentru desenare.

transform contine un namespace cu functii dedicate pentru a modifica matricea de modelare.
 * Similar lui Transform2D din Framework.

drawing contine un namespace cu functii dedicate managementului datelor ce vor fi desenate.
 * Similar lui DrawingWindow din Framework.

game contine un namespace dedicat engine-ului de joc propriu zis.
 * Contine callback-urile din DrawingWindow din Framework.

generators contine un namespace dedicat constantelor de joc si al generarii de nivele.

main contine functiile de glue intre engine si freeglut.

Framework-ul construit de mine contine doar strictul necesar realizarii temei si o
idiomizare mai buna a logicii. Nu am considerat necesara o clasa pentru engine-ul in sine.

4. Testare
Tema a fost testata pe Windows 8.1 Pro, Visual Studio 2013. Am testat jocul pentru diferite
seed-uri ale rand()-ului si majoritatea nivelelor erau terminabile.

5. Probleme aparute
Prima problema a fost folosirea unui Framework mult prea mare si nevoia de a-l rescrie pentru
a face doar strictul necesar pentru joc.

A doua problema a fost gasirea unui model eficient de coliziuni pentru cazurile de rotire
incompleta (adica multiplu de PI/4), ajungand sa accept o eroare a pozitiei Y a actorului
fata de platforma.

O alta problema a fost gasirea unui model eficient de a genera nivele aleatorii.
Pentru a avea posibilitatea de a face un nivel perfect e nevoie de adaugarea de prea multe
constrangeri asupra sistemului de generare. Astfel, sistemul poate da gres si sa genereze
un nivel ce nu poate fi terminat.

6. Continutul Arhivei
EGC1
| Debug - freeglut.dll - Target-ul de copy nu merge.
|
| EGC1
|  |
|  |---- dependente
|  |     |
|  |     |---- Fisierele freeglut din Framework
|  |     |---- 11 fisiere .wav
|  |
|  |---- Framework
|  |     |
|  |     |---- Circle2.h
|  |     |---- Color.h
|  |     |---- Object2.h
|  |     |---- Point2.h
|  |     |---- Rectangle2.h
|  |     |---- Steag.h
|  |     |---- Text.h
|  |     |---- Triangle2.h
|  |     |---- Visual2.h
|  | 
|  |---- .gitignore
|  |---- drawing.cpp
|  |---- drawing.h
|  |---- EGC1.vcxproj
|  |---- EGC1.vcxproj.filters
|  |---- game.cpp
|  |---- game.h
|  |---- generators.cpp
|  |---- generators.h
|  |---- main.cpp
|  |---- transform.cpp
|  |---- transform.h
|
| EGC1.sln
| README.TXT

7. Functionalitati

Toate cerintele standard.
- Translatarea scenei.
- Saritura jucatorului cu rostogolire
  * cu rotunjire la multiplu de PI/4 pentru rostogoliri incomplete.
- Sistemul de vieti.
- Sistemul de checkpoint + scor (steagurile).
- Compozitia din mai multe parti a actorului.
- Implementarea obstacolelor, platformelor de sarit si cercurilor de propulsare.
- Sistemul de aterizare pe platforme si sarit in continuare.
- Sistemul de text.
- Generarea unui nivel complet.

Functionalitati bonus:
- Mai multe nivele de dificultate (Easy/Hard - schimba viteza jocului)
- Generare automata/random a scenelor
- Sunete pentru saritura, propulsie pe jumper, pierderea unei vieti,
 atingerea unui checkpoint, castigarea/pierderea unui joc.
- Obstacole care se misca pe verticala.

8.
Codul este comentat si se poate citi cu usurinta, incercand sa creez un API
inteligibil.