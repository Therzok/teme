#include "Framework/Object2.h"
#include "transform.h"
#include <math.h>

namespace transform
{
	//initial matricea de transformare e matricea identitate
	// 1 0 0
	// 0 1 0
	// 0 0 1

	float TransformMatrix[3][3] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };
	void loadIdentityMatrix()
	{
		//matricea de transformare se intoarce la matricea identitate
		// 1 0 0
		// 0 1 0
		// 0 0 1
		TransformMatrix[0][0] = 1;	TransformMatrix[0][1] = 0;	TransformMatrix[0][2] = 0;
		TransformMatrix[1][0] = 0;	TransformMatrix[1][1] = 1;	TransformMatrix[1][2] = 0;
		TransformMatrix[2][0] = 0;	TransformMatrix[2][1] = 0;	TransformMatrix[2][2] = 1;
	}

	void multiplyMatrix(float matrix[3][3])
	{//								m00   m01  m02     tm00  tm01  tm02
		// matrix * TransformMatrix =  m10   m11  m12  *  tm10  tm11  tm12
		//								m20   m21  m22     tm20  tm21  tm22
		int i, j, k;
		float aux_matrix[3][3];
		for (i = 0; i < 3; i++)
			for (j = 0; j < 3; j++)
			{
				aux_matrix[i][j] = 0;
				for (k = 0; k < 3; k++)
					aux_matrix[i][j] = aux_matrix[i][j] + matrix[i][k] * TransformMatrix[k][j];
			}

		for (i = 0; i < 3; i++)
			for (j = 0; j < 3; j++)
				TransformMatrix[i][j] = aux_matrix[i][j];
	}

	void translateMatrix(float tx, float ty)
	{
		float TranslateMatrix[3][3] = {};

		//se construieste matricea de translatie
		//1  0  tx 
		//0  1  ty
		//0  0  1

		TranslateMatrix[0][0] = TranslateMatrix[1][1] = TranslateMatrix[2][2] = 1;
		TranslateMatrix[0][2] = tx;
		TranslateMatrix[1][2] = ty;

		//se inmulteste matricea de translatie cu matricea curenta de transformari
		//folosim scrierea vectori coloana
		multiplyMatrix(TranslateMatrix);
	}

	void rotateMatrix(float u)
	{
		float RotateMatrix[3][3] = {};
		//se construieste matricea de rotatie
		//cos(u)  -sin(u)  0 
		//sin(u)  cos(u)   0
		//0        0       1

		RotateMatrix[0][0] = RotateMatrix[1][1] = cos(u);
		RotateMatrix[0][1] = -sin(u);
		RotateMatrix[1][0] = sin(u);
		RotateMatrix[2][2] = 1;

		//se inmulteste matricea de rotatie cu matricea curenta de transformari
		//folosim scrierea vectori coloana
		multiplyMatrix(RotateMatrix);
	}

	void applyTransform(Point2 *p, Point2 *transf_p)
	{
		//se inmulteste matricea curenta de transformari cu pozitia punctului
		// tm00  tm01  tm02    p.x
		// tm10  tm11  tm12  * p.y
		// tm20  tm21  tm22     1

		float aux_x = p->x;
		transf_p->x = TransformMatrix[0][0] * p->x + TransformMatrix[0][1] * p->y + TransformMatrix[0][2];
		transf_p->y = TransformMatrix[1][0] * aux_x + TransformMatrix[1][1] * p->y + TransformMatrix[1][2];
	}

	void applyTransform(Object2 *o)
	{
		//se aplica transformarea pe toate punctele obiectului
		for (unsigned int i = 0; i < o->points.size(); i++)
			applyTransform(o->points[i], o->transf_points[i]);
	}
}