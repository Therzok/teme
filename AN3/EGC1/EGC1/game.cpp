#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <ctime>
#ifdef _MSC_VER
#include <windows.h>
#endif

#include "Framework/Circle2.h"
#include "Framework/Triangle2.h"
#include "Framework/Text.h"

#include "drawing.h"
#include "game.h"
#include "generators.h"
#include "transform.h"

using namespace constants;
using namespace generators;
using namespace drawing;

namespace game
{
	// Game variables.

	bool playing, gameOver;
	bool rotating, jumping, freefall, willfall, collide, landed, super_jump;
	bool keyJump;

	int lives = MAX_LIVES;
	int score;
	Difficulty diff = EASY;

	// UI objects.

	Visual2 *v2bg, *v2actor, *v2course;
	vector<Rectangle2 *> actor;
	Text *lifeText, *scoreText, *gameOverText, *optsText;

	// Game engine model.

	static float checkpoint_x, checkpoint_y;
	static float rotation, translation, scene, landing, jumped_from;

	// Forward declaration.

	void restart();
	void actorMove();
	void sceneMove();

	// Text + sunet si setare game over.
	void gameFinish(bool won)
	{
		char snd[50];
		gameOverText->text = std::string("You ") + (won ? "won" : "lost") + "!";
		if (won)
		{
			sprintf(snd, "snd_win%d.wav", rand() % 3);
				PlaySound(TEXT(snd), NULL, SND_ASYNC | SND_FILENAME);
			gameOverText->color = Color(0, 1, 0);
		}
		else
		{
			sprintf(snd, "snd_loss%d.wav", rand() % 3);
			gameOverText->color = Color(1, 0, 0);
		}
		PlaySound(TEXT(snd), NULL, SND_ASYNC | SND_FILENAME);

		// Set width based on text.
		gameOverText->pos.x = MIDDLE(WIDTH, glutBitmapLength(FONT, (const unsigned char*)gameOverText->text.c_str()));
		playing = false;
		gameOver = true;
	}

	// Pierdem o viata, si reincepem de la checkpoint.
	void loseLife(bool fromFall)
	{
		lifeText->text[lifeText->text.length() - 1] = --lives + '0';
		lifeText->color = Color(0.3f * (MAX_LIVES - lives), 0.3f * lives, 0);
		if (!lives)
			gameFinish(false);
		else
		{
			restart();
			if (lives != MAX_LIVES)
			{
				if (fromFall)
					PlaySound(TEXT("snd_bump.wav"), NULL, SND_ASYNC | SND_FILENAME);
				else
					PlaySound(TEXT("snd_fall.wav"), NULL, SND_ASYNC | SND_FILENAME);
			}
		}
	}

	// Am trecut de un checkpoint.
	void acquirePoint()
	{
		scoreText->text[scoreText->text.length() - 1] = ++score + '0';
		if (score != 0)
			PlaySound(TEXT("snd_check.wav"), NULL, SND_ASYNC | SND_FILENAME);

		if (score == 5)
			gameFinish(true);
	}

	// Marim dificultatea.
	void changeDifficulty(bool modify = true)
	{
		if (modify)
			diff = (diff == EASY) ? HARD : EASY;
		optsText->text = std::string("Difficulty: ") + DIFFICULTY_TEXT[diff];
	}

	// Reparam eroare de rotire.
	void getNearestPiMultiple(float &rotation)
	{
		const float pi = -PI;
		const float pi2 = 2 * pi;
		const float rots[] = {
			3.f * pi / 2,
			pi,
			pi / 2,
			0
		};

		while (rotation < pi2)
			rotation -= pi2;

		float min = abs(rots[0] - rotation);
		int k = 0;
		for (int i = 1; i < 4; ++i){
			float dist = abs(rots[i] - rotation);
			if (dist > min)
				continue;

			k = i;
			min = dist;
		}

		rotation = rots[k];
	}

	// Daca nu trebuie sa cadem.
	bool isOnPlatform(Point2 *p, Rectangle2 *r)
	{
		return p->x >= r->transf_points[0]->x && p->x <= r->transf_points[2]->x &&
			(p->y + translation - r->transf_points[2]->y) < TRANSLATION_SPEED[diff];
	}

	// Aplicam pe punctul nostru si cate o pereche din combarile de colturi ale triunghiului.
	bool sign(Point2 *p1, Point2 *p2, Point2 *p3)
	{
		return (p1->x - p3->x) * (p2->y - p3->y) - (p2->x - p3->x) * (p1->y - p3->y) < 0;
	}

	// Verificam daca un punct este in triunghi.
	void checkPointInTriangle(Point2 *p, Triangle2 *t)
	{
		bool b1 = sign(p, t->transf_points[0], t->transf_points[1]);
		bool b2 = sign(p, t->transf_points[1], t->transf_points[2]);
		bool b3 = sign(p, t->transf_points[2], t->transf_points[0]);

		// Punctul este inauntru daca toate b-urile au acelasi semn.
		collide |= b1 == b2 && b2 == b3;
	}

	// Verificam daca un punct apartine dreptunghiului, admitand o eroare de strapungere datorita rotirii.
	void checkPointInRect(Point2 *p, Rectangle2 *r)
	{
		collide |= p->x >= r->transf_points[0]->x && p->x <= r->transf_points[2]->x &&
			(p->y < r->transf_points[2]->y - 5 * TRANSLATION_SPEED[diff]) && (p->y > r->transf_points[0]->y + 5 * TRANSLATION_SPEED[diff]);
	}

	// Verificam daca un punct apartine unui cerc. Verificarea de fapt se face pentru o forma de patrat in jurul lui.
	void checkPointInCircle(Point2 *p, Circle2 *c)
	{
		if (c->used)
			return;

		Point2 *center = c->getCenter();
		if (p->x >= center->x - CIRCLE_RADIUS && p->x <= center->x + CIRCLE_RADIUS &&
			p->y >= center->y - CIRCLE_RADIUS && p->y <= center->y + CIRCLE_RADIUS)
		{
			super_jump |= true;
			c->used = true;
		}
		delete center;
	}

	// Verificam conditiile speciale: cadere libera, jumper platform si checkpoint.
	void checkFallAndSuperJump(Object2 *obstacle)
	{
		Rectangle2 *rect_a = (Rectangle2 *)actor[0];
		if (obstacle->type == OT_RECTANGLE)
		{
			Rectangle2 *rect_o = (Rectangle2 *)obstacle;
			if (!jumping) {
				Point2 *ll = rect_a->points[0];
				Point2 *lr = rect_a->points[1];

				if (isOnPlatform(ll, rect_o) || isOnPlatform(lr, rect_o))
				{
					willfall = false;
					landing = rect_o->transf_points[2]->y;
				}
			}
		}
		else if (obstacle->type == OT_CIRCLE)
		{
			Circle2 *circle_o = (Circle2 *)obstacle;
			FOREACH_VECTOR_PARAMS(checkPointInCircle, Point2 *, rect_a->transf_points, circle_o);
		}
		else if (obstacle->type == OT_STEAG)
		{
			Point2 *lr = rect_a->lowRight();
			if (lr->x >= obstacle->transf_points[0]->x)
			{
				// Setam noul checkpoint.
				float new_checkpoint = obstacle->points[0]->x - GAME_BORDER - ACTOR_SIZE/2;
				if (checkpoint_x < new_checkpoint)
				{
					checkpoint_x = new_checkpoint;
					checkpoint_y = obstacle->points[0]->y - GAME_BORDER;
					acquirePoint();
				}
			}
		}
	}

	// Verifica coliziunea care este fatala.
	void checkCollision(Object2 *obstacle)
	{
		Rectangle2 *rect_a = (Rectangle2 *)actor[0];

		// Crash in obstacol.
		if (obstacle->type == OT_TRIANGLE)
		{
			Triangle2 *tri_o = (Triangle2 *)obstacle;

			// Check corners only. Chances are you'll collide anyway.
			FOREACH_VECTOR_PARAMS(checkPointInTriangle, Point2 *, rect_a->transf_points, tri_o);
		}
		// Crash in platforma.
		else if (obstacle->type == OT_RECTANGLE)
		{
			Rectangle2 *rect_o = (Rectangle2 *)obstacle;
			FOREACH_VECTOR_PARAMS(checkPointInRect, Point2 *, rect_a->transf_points, rect_o);
		}
	}

	// Display-ul initial al ferestrei.
	void onInit()
	{
		srand(time(NULL));
		main_visual = new Visual2();

		// Actor.
		v2actor = new Visual2(0.0f, 0.0f, (float)WIDTH, (float)HEIGHT, 0, 0, WIDTH, HEIGHT);
		shuffleActors();
		addVisual2D(v2actor);

		// Game scene.
		v2course = new Visual2(0.f, 0.f, (float)GAME_WIDTH, (float)HEIGHT, 0, 0, GAME_WIDTH, HEIGHT);
		generateLevel();
		addVisual2D(v2course);

		// Background.
		v2bg = new Visual2(0.0f, 0.0f, (float)WIDTH, (float)HEIGHT, 0, 0, WIDTH, HEIGHT);

		// Margin 14px. Top left 'score', top right 'lives'.
		const char *tScore = "Score: 0";
		const char *tLife = "Lives: 3";
		const char *tSelect = "Select char with left and right keys. Press space to begin or jump.";
		const char *tOpts = "Difficulty: Easy";
		static int lScore = glutBitmapLength(FONT, (const unsigned char*)tScore);
		static int lLife = glutBitmapLength(FONT, (const unsigned char*)tLife);
		static int lSelect = glutBitmapLength(FONT, (const unsigned char*)tSelect);
		static int lOpts = glutBitmapLength(FONT, (const unsigned char*)lOpts);

		scoreText = new Text(tScore, Point2(MIDDLE(WIDTH, lScore), HEIGHT - MARGIN - CHAR_HEIGHT), Color(0, 1, 0), FONT);
		lifeText = new Text(tLife, Point2(WIDTH - lLife - MARGIN - MARGIN, HEIGHT - MARGIN - CHAR_HEIGHT), Color(0, 1, 0), FONT);
		gameOverText = new Text(tSelect, Point2(MIDDLE(WIDTH / 4, 0), MIDDLE(HEIGHT, CHAR_HEIGHT)), Color(1, 0, 0), FONT);
		optsText = new Text(tOpts, Point2(MIDDLE(WIDTH / 4, 0), MIDDLE(HEIGHT, CHAR_HEIGHT) - CHAR_HEIGHT), Color(1, 0, 0), FONT);

		// Adaugam elemente in background.
		addObject2D(new Rectangle2(Point2(0, 0), WIDTH, HEIGHT, Color(0.25f, 0.41f, 0.88f), true), v2bg);
		addText(lifeText, v2bg);
		addText(scoreText, v2bg);
		addText(gameOverText, v2bg);
		addText(optsText, v2bg);
		addVisual2D(v2bg);
	}

	// Resetam starea de folosire a cercurilor la restart.
	void resetJumpers(Object2 *object)
	{
		if (object->type == OT_CIRCLE)
			((Circle2*)object)->used = false;
	}

	// Resetam toate variabilele de engine.
	void restart()
	{
		rotation = jumped_from = 0.f;

		translation = checkpoint_y;
		scene = checkpoint_x;
		jumping = rotating = freefall = collide = landed = super_jump = false;
		playing = false;

		FOREACH_VECTOR_PARAMS(resetJumpers, Object2 *, v2course->objects);
	}

	// Reinitializam valorile pentru a incepe un joc nou.
	void reinit()
	{
		// Reset game status.
		gameOver = false;
		lives = MAX_LIVES + 1;
		score = -1;
		checkpoint_x = checkpoint_y = 0.f;

		// Update texts.
		loseLife(false);
		acquirePoint();
		changeDifficulty(false);
		restart();
		actorMove();
		sceneMove();
	}

	// Deseneaza obiectele din contextele secundare si apoi obiectele din contextul principal.
	void onDisplay()
	{
		for (vector<Visual2 *>::iterator i = visuals.begin(); i != visuals.end(); ++i)
		{
			(*i)->decupare(HEIGHT);
			// deseneaza obiectele din contextul vizual curent
			drawObjects(*i);
		}
		//deseneaza obiectele din containerul principal
		drawObjects(main_visual);
	}

	// Aplica miscarea de saritura si rostogolire a actorului.
	void actorMove()
	{
		Point2 *center = actor[0]->getCenter(false);

		transform::loadIdentityMatrix();
		transform::translateMatrix(-center->x, -center->y);
		transform::rotateMatrix(rotation);
		transform::translateMatrix(center->x, center->y + translation);

		FOREACH_ACTOR_PARAMS(transform::applyTransform);

		delete center;
	}

	// Aplica miscarea la stanga a scenei, si miscarea verticala a obiectelor miscatoare.
	void sceneMove()
	{
		static float dist = 0.f;
		static float step = TRANSLATION_SPEED[diff] / 2;

		for (vector<Object2 *>::iterator i = v2course->objects.begin(); i != v2course->objects.end(); ++i)
		{
			transform::loadIdentityMatrix();
			transform::translateMatrix(-scene, (*i)->moving ? dist : 0);
			transform::applyTransform(*i);
		}

		dist += step;
		if (dist >= 48.f || dist <= 0.f)
			step = -step;
	}

	// Logica principala.
	void onIdle()
	{
		if (!playing)
			return;

		// Presupunem ca nu e pe o platforma si verificam.
		willfall = !jumping;
		landed = collide = super_jump = false;
		landing = 0.f;
		FOREACH_VECTOR_PARAMS(checkFallAndSuperJump, Object2 *, v2course->objects);

		if (freefall && !willfall)
			landed = true; // Pe platforma.

		freefall = willfall;

		// Verificam daca user-ul vrea sa sara.
		if (keyJump) {
			// Daca nu sarim, incepem sa sarim.
			if (!jumping && !freefall)
			{
				jumping = true;
				jumped_from = translation;
				PlaySound(TEXT("snd_jump.wav"), NULL, SND_ASYNC | SND_FILENAME);
			}
		}

		// Verificam daca am atins un jumper.
		if (super_jump) {
			freefall = false;
			jumping = true;
			jumped_from = translation;
			PlaySound(TEXT("snd_jumper.wav"), NULL, SND_ASYNC | SND_FILENAME);
		}

		// Modificam translatia si unghiul de rotire al obiectului pentru saritura.
		if (jumping) {
			if (translation >= JUMP_DISTANCE + jumped_from)
				jumping = false;
			else
			{
				translation += TRANSLATION_SPEED[diff];
				rotation -= ROTATION_SPEED[diff];
			}
		}

		// Modificam translatia si unghiul de rotire al obiectului pentru cadere libera.
		if (freefall) {
			translation -= TRANSLATION_SPEED[diff];
			rotation -= ROTATION_SPEED[diff];
		}

		// Daca am aterizat, unghiul va fi rotunjit la un multiplu de PI/4.
		if (landed) {
			getNearestPiMultiple(rotation);
			translation = landing - actor[0]->points[0]->y;
		}

		// Miscam scena obstacolelor.
		scene += MOVEMENT_SPEED[diff];

		// Aplicam transformarile pentru cele 2 scene.
		actorMove();
		sceneMove();

		// Verificam daca moare de la cadere libera.
		bool fromfall = false;
		if (actor[0]->points[1]->y + translation < 0)
		{
			collide = true;
			fromfall = true;
		}
		// Verificam daca moare de la coliziune.
		if (!landed)
			FOREACH_VECTOR_PARAMS(checkCollision, Object2 *, v2course->objects);

		// Eliminam o viata.
		if (collide)
			loseLife(fromfall);
	}

	// Callback pentru apasare de taste.
	void onKey(unsigned char key)
	{
		bool reverse = false;
		switch (key)
		{
		case 27: exit(0);	// Exit
		case ' ':			// Jump ON + Game start
			if (!playing)
			{
				if (gameOver)
					reinit();
				else
					playing = true;

				gameOverText->text = "";
				optsText->text = "";
			}
			else
				keyJump = true;
			break;
		case '\t':			// New random map.
			if (!playing)
				generateLevel();
			break;

			// Cheats.
		case 'a':			// Lose life cheat.
			loseLife(false);
			break;
		case 's':			// Acquire point cheat.
			acquirePoint();
			break;

		case GLUT_KEY_LEFT:	// Reverse select actor.
			reverse = true;
		case GLUT_KEY_RIGHT:// Select actor.
			if (!playing)
				shuffleActors(reverse);
			break;

		case GLUT_KEY_UP:	// Switch difficulty
		case GLUT_KEY_DOWN:
			if (!playing)
				changeDifficulty();
			break;
		}
	}

	// Callback pentru ridicare de taste.
	void onKeyUp(unsigned char key)
	{
		switch (key)
		{
		case ' ':			// Jump OFF
			keyJump = false;
			break;
		}
	}

	// Callback pentru mouse.
	void onMouse(int button, int state, int x, int y)
	{
		if (button != GLUT_LEFT_BUTTON)
			return;
		keyJump = state == GLUT_DOWN; // else GLUT_UP
	}
}
