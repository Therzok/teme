#include <algorithm>
#include "drawing.h"
#include "game.h"
#include "generators.h"
#include "Framework/Triangle2.h"
#include "Framework/Circle2.h"
#include "Framework/Steag.h"
#include "Framework/Rectangle2.h"

using namespace constants;
using namespace drawing;
using namespace game;

namespace constants
{
	const char *DIFFICULTY_TEXT[] =
	{
		"Easy",
		"Hard",
	};
}

namespace generators
{
	enum Actors
	{
		MinActor = -1,
		Normal = 0,
		Cyclop,
		Funky,
		Derpy,
		MaxActor
	};

	int currentActor = MinActor;
	void shuffleActors(bool rev)
	{
		currentActor = rev ? currentActor - 1 : currentActor + 1;
		if (currentActor == -1)
			currentActor = MaxActor - 1;
		else if (currentActor == MaxActor)
			currentActor = MinActor + 1;

		FOREACH_ACTOR_PARAMS(removeObject2D, v2actor);
		switch (currentActor)
		{
		case Normal: // Frame, eye, eye, mouth.
			actor.push_back(new Rectangle2(Point2(GAME_BORDER, GAME_BORDER), ACTOR_SIZE, ACTOR_SIZE, Color(0, 0, 1), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + 11, ACTOR_EYE_Y), 10, 10, Color(0, 1, 1), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + ACTOR_SIZE / 2 + 11, ACTOR_EYE_Y), 10, 10, Color(0, 1, 1), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + 17, ACTOR_MOUTH_Y), 30, 10, Color(1, 0, 0), true));
			break;
		case Cyclop: // Frame, eye, mouth.
			actor.push_back(new Rectangle2(Point2(GAME_BORDER, GAME_BORDER), ACTOR_SIZE, ACTOR_SIZE, Color(0, 0.8, 0), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + 22, ACTOR_EYE_Y), 20, 10, Color(0, 1, 1), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + 17, ACTOR_MOUTH_Y), 30, 10, Color(1, 1, 0), true));
			break;
		case Funky: // Frame, eye, eye, mouth.
			actor.push_back(new Rectangle2(Point2(GAME_BORDER, GAME_BORDER), ACTOR_SIZE, ACTOR_SIZE, Color(1, 0.5, 0.75), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + 11, ACTOR_EYE_Y), 10, 10, Color(0, 0.5, 0.5), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + ACTOR_SIZE / 2 + 11, ACTOR_EYE_Y), 10, 10, Color(0, 0.5, 0.5), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + 17, ACTOR_MOUTH_Y + 10), 30, 10, Color(0, 0, 1), true));
			break;
		case Derpy: // Frame, eye, eye, mouth.
			actor.push_back(new Rectangle2(Point2(GAME_BORDER, GAME_BORDER), ACTOR_SIZE, ACTOR_SIZE, Color(1, 0.5, 0), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + 20, ACTOR_EYE_Y), 10, 10, Color(0.5, 0.5, 0.5), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + ACTOR_SIZE / 2 + 20, ACTOR_EYE_Y), 10, 10, Color(0.5, 0.5, 0.5), true));
			actor.push_back(new Rectangle2(Point2(GAME_BORDER + 8, ACTOR_MOUTH_Y + 10), 30, 10, Color(0, 0, 1), true));
			break;
		}
		FOREACH_ACTOR_PARAMS(addObject2D, v2actor);
	}

	// Distanta parcursa intr-o saritura.
	const int distancePerJump = MOVEMENT_SPEED[0] * (JUMP_DISTANCE / TRANSLATION_SPEED[0]);

	// Clasa pentru generare de platforme si proprietati ale lor.
	class Layout
	{
	public:
		Layout() {}
		Layout(int _start, int _end, float _height, bool _requireJumper, int _numSpikes, int _numBlocks) :
			start(_start), end(_end), height(_height), numSpikes(_numSpikes), numBlocks(_numBlocks) {}

		// Platform data.
		int start, end, height;

		// Properties.
		int numSpikes, numBlocks;
	};

	// Dispersie medie a checkpoint-urilor peste 32 de platforme.
	bool isFlagLayout(int i)
	{
		return i == 7 || i == 13 || i == 19 || i == 25 || i == 31;
	}

	// Generam un nivel nou.
	void generateLevel()
	{
		vector<Object2 *> copy(v2course->objects);
		FOREACH_VECTOR_PARAMS(removeObject2D, Object2 *, copy, v2course);
		Color platform_color(0, 0, 0.5);
		Color triangle_color(0.4, 0.01, 0.01);
		Color jumper_color(0.3, 0.8, 0.5);
		
		// Generate platform layout.
		const int numLayouts = 27 + 5;
		vector<Layout> layout;
		vector<Point2> required_jumpers;

		layout.push_back(Layout(0, GAME_BORDER + 400, GAME_BORDER, false, 0, 0));
		for (int i = 1; i < numLayouts; ++i)
		{
			// Pad the rectangle so we don't fail on the collision check.
			int start = layout[i - 1].end - 1;
			float height = layout[i - 1].height;
			int numSpikes = 0;
			int numBlocks = 0;
			int x;

			if (isFlagLayout(i))
			{
				layout.push_back(Layout(start, start + 150, max(height, 50.f), false, numSpikes, numBlocks));
				continue;
			}

			int length = ((rand() % 3) + 2) * distancePerJump;
			if (isFlagLayout(i - 1))
				x = 0;
			else if (height >= HEIGHT / 2) // Too high, go down or continue.
				x = rand() & 1;
			else if (height <= 0) // Too low.
			{
				if (rand() & 1) // Go up.
					x = 2;
				else // Continue on low.
				{
					x = 0;
					required_jumpers.push_back(Point2(start, height + 20));
				}
			}
			else // Go anywhere.
				x = rand() % 3;

			// Height change constraint. 100 requires jumper.
			if (x == 2) // GO UP
			{
				if (rand() & 1)
					height += 50;
				else
				{
					required_jumpers.push_back(Point2(start - 100, height + 40));
					height += 120;
				}
			}
			else if (x == 1) // GO DOWN
				height -= 50;

			if (height == 0)
				length = min(distancePerJump, length);
			else
			{
				if (length >= 200)
					numSpikes += rand() % 2;
				if (length >= 300)
				{
					numSpikes += rand() % 2;
					numBlocks += rand() % 2;
				}
				if (length >= 400)
				{
					numSpikes += rand() % 3;
					numBlocks += rand() % 2;
				}

				if (numSpikes >= 2 && numBlocks == 0)
					required_jumpers.push_back(Point2(start + length - 100, height + 40));
			}

			layout.push_back(Layout(start, start + length, height, false, numSpikes, numBlocks));
		}

		addObject2D(new Rectangle2(Point2(layout[0].start, 0), layout[0].end - layout[0].start, layout[0].height, platform_color, true), v2course);
		for (int i = 1; i < numLayouts; ++i)
		{
			int start = layout[i].start;
			int height = layout[i].height;
			int length = layout[i].end - layout[i].start - 1;
			int center = layout[i].start + length / 2;

			if (height != 0)
			{
				// Add platform.
				addObject2D(new Rectangle2(Point2(start, 0), length, height, platform_color, true), v2course);

				// Add flag if needed.
				if (isFlagLayout(i))
				{
					addObject2D(new Steag(Point2(center, height)), v2course);
					continue;
				}
			}

			bool packSpikes = layout[i].numSpikes > 2;
			if (packSpikes)
			{
				bool positive = rand() & 1;
				float offset = positive ? layout[i].start : layout[i].end;

				for (int j = 1; j <= layout[i].numSpikes; ++j)
				{
					if (positive)
						offset += (FLAG_HEIGHT + j * 25) * (positive ? 1 : -1);
					addObject2D(new Triangle2(Point2(offset, height), FLAG_HEIGHT, triangle_color, true), v2course);
				}
			}
			else
			{
				for (int j = 1; j <= layout[i].numSpikes; ++j)
				{
					float offset;
					if ((layout[i-1].height - height <= 100) && rand() & 1)
						offset = layout[i].start + (FLAG_HEIGHT + j * 35);
					else
						offset = layout[i].end - (FLAG_HEIGHT + j * 35);
					addObject2D(new Triangle2(Point2(offset, height), FLAG_HEIGHT, triangle_color, true), v2course);
				}
			}

			for (int j = 1; j <= layout[i].numBlocks; ++j)
			{
				int offset;
				if (rand() & 1)
					offset = layout[i].start + j * 75;
				else
					offset = layout[i].end - j * 75;

				Rectangle2 *rect = new Rectangle2(Point2(offset, height + 50), 60, 30, jumper_color, true);
				rect->moving = rand() & 1;
				addObject2D(rect, v2course);
			}
		}

		for (vector<Point2>::iterator i = required_jumpers.begin(); i != required_jumpers.end(); ++i)
		{
			if (rand() & 1)
				addObject2D(new Circle2(Point2(i->x, i->y + CIRCLE_RADIUS), CIRCLE_RADIUS, jumper_color, true), v2course);
			else
				addObject2D(new Rectangle2(Point2(i->x, i->y), 70, 30, jumper_color, true), v2course);
		}
	}
}