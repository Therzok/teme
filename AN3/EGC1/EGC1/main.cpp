#include <ctime>
#include "drawing.h"
#include "game.h"

void displayFunction()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	game::onDisplay();

	//swap buffers
	glutSwapBuffers();
}

void reshapeFunction(int width, int height)
{
	glutReshapeWindow(drawing::WIDTH, drawing::HEIGHT);

	//glViewport stabileste transformarea in poarta de vizualizare
	glViewport(0, 0, drawing::WIDTH, drawing::HEIGHT);

	//se stabileste transformarea de proiectie
	glMatrixMode(GL_PROJECTION);
	//se porneste de la matricea identitate
	glLoadIdentity();
	//glOrtho este o proiectie ortografica - ea stabileste volumul de vizualizare
	//in cazul nostru, stabileste fereastra de vizualizare
	glOrtho(0, 200, 0, 200, 1, 300);
	//ne intoarcem la matricea de modelare vizualizare

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//observatorul este pozitionat pe axa pozitiva a lui z si priveste spre planul XOY
	gluLookAt(0.0, 0.0, 20.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

// Idle function for UNIX.
void idleFunction()
{
	game::onIdle();
	glutPostRedisplay();
}

// Idle function for Windows.
void timerFunction(int flag)
{
	clock_t drawStartTime = clock();
	game::onIdle();
	glutPostRedisplay();
	clock_t drawEndTime = clock();

	const int MAX_FPS = 60;
	unsigned int delayToNextFrame = (CLOCKS_PER_SEC / MAX_FPS) - (drawEndTime - drawStartTime);
	delayToNextFrame = (unsigned int)(delayToNextFrame + 0.5f);
	delayToNextFrame < 0 ? delayToNextFrame = 0 : NULL;
	glutTimerFunc(delayToNextFrame, timerFunction, 0);
}

void keyboardFunction(unsigned char key, int x, int y)
{
	game::onKey(key);
}

void specialFunction(int key, int x, int y)
{
	game::onKey(key);
}

void keyboardUpFunction(unsigned char key, int x, int y)
{
	game::onKeyUp(key);
}

void specialUpFunction(int key, int x, int y)
{
	game::onKeyUp(key);
}

void mouseFunction(int button, int state, int x, int y)
{
	game::onMouse(button, state, x, y);
}

int main(int argc, char **argv)
{
	const char *GAME_NAME = "Geometry Dash - Ungureanu Marius";

	// Initializare fereastra.
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(drawing::WIDTH, drawing::HEIGHT);

	// Centrare fereastra.
	int startY = MIDDLE(glutGet(GLUT_SCREEN_HEIGHT), drawing::HEIGHT);
	int startX = MIDDLE(glutGet(GLUT_SCREEN_WIDTH), drawing::WIDTH);
	glutInitWindowPosition(startX, startY);
	glutCreateWindow(GAME_NAME);

	// Inregistrare callback-uri.
	glutDisplayFunc(displayFunction); //functia apelata pentru afisarea continutului ferestrei aplicatiei
	glutReshapeFunc(reshapeFunction); //functia apelata pentru refacerea continutului ferestrei dupa o redimensionare sau dupa ce fereastra a fost (partial) acoperita
#ifndef _MSC_VER
	glutIdleFunc(idleFunction); //functia apelata atunci cand sistemul nu are de tratat alte evenimente
#else
	glutTimerFunc(0, timerFunction, 0);
#endif
	glutKeyboardFunc(keyboardFunction); //functia apelata la apasarea unei taste
	glutSpecialFunc(specialFunction); //functia apelata la apasarea unei taste speciale
	glutKeyboardUpFunc(keyboardUpFunction); //functia apelata la apasarea unei taste
	glutSpecialUpFunc(specialUpFunction);
	glutMouseFunc(mouseFunction); //functia apelata la click de mouse

	glClearColor(1, 1, 1, 1);  //afiseaza fondul ferestrei aplicatiei in alb

	// activeaza testul de adancime pentru eliminarea partilor nevizibile in imagini
	glEnable(GL_DEPTH_TEST);

	// Initializare game engine.
	game::onInit();

	// Bucla principala.
	glutMainLoop();
	return 0;
}
