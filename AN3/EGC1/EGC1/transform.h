#pragma once

class Object;

// Din Framework.
namespace transform
{
	// Reseteaza matricea de transformari.
	void loadIdentityMatrix();
	// Translateaza matricea cu tx si ty.
	void translateMatrix(float tx, float ty);
	// Roteste matricea cu u grade.
	void rotateMatrix(float u);
	// Aplica transformarea pe obiect.
	void applyTransform(Object2 *o);
}