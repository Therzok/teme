#pragma once
#include "Object2.h"

class Triangle2 : public Object2
{
public:
	Triangle2(Point2 center, int height) : Object2(OT_TRIANGLE, Color(), false)
	{
		addPointInternal(Point2(center.x - height / 2, center.y));
		addPointInternal(Point2(center.x + height / 2, center.y));
		addPointInternal(Point2(center.x, center.y + height));
	}

	Triangle2(Point2 center, int height, Color _color, bool _fill) : Object2(OT_TRIANGLE, _color, _fill)
	{
		addPointInternal(Point2(center.x - height / 2, center.y));
		addPointInternal(Point2(center.x + height / 2, center.y));
		addPointInternal(Point2(center.x, center.y + height));
	}
};