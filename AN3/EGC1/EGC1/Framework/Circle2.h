#pragma once
#include "Object2.h"
#include <cmath>

// Clasa pentru cerc.
class Circle2 : public Object2
{
public:
	Circle2(Point2 center, float radius) :
		Object2(OT_CIRCLE, Color(), false), used(false)
	{
		constructPoints(center, radius);
	}

	Circle2(Point2 center, float radius, Color _color, bool _fill) :
		Object2(OT_CIRCLE, _color, _fill), used(false)
	{
		constructPoints(center, radius);
	}

	~Circle2() {}

	// Se foloseste pentru a ignora coliziuni pe acelasi jumper.
	// Se reseteaza la restart de nivel.
	bool used;

private:
	void constructPoints(Point2 center, float radius)
	{
		const float du = 20;
		for (float u = 0; u <= 360; u += du)
		{
			float u_rad = u * 3.14159 / 180;
			float x = center.x + radius * cos(u_rad);
			float y = center.y + radius * sin(u_rad);
			addPointInternal(Point2(x, y));
		}
	}
};