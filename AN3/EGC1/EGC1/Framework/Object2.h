#pragma once
#include "Color.h"
#include "Point2.h"
#include <vector>

using namespace std;

// Enum pentru tipul de obiect desenat. Pentru rendering, se foloseste doar OT_LINE.
// Restul se folosesc pentru verificari in joc.
enum ObjectType
{
	OT_RECTANGLE	= 0,
	OT_CIRCLE		= 1,
	OT_POLYGON		= 2,
	OT_TRIANGLE		= 3,
	OT_STEAG		= 4,
};

// Clasa de baza pentru obiecte 2D.
class Object2
{
public:
	// Punctele netransformate.
	vector<Point2 *> points;
	// Punctele dupa transformari.
	vector<Point2 *> transf_points;
	// Tipul obiectului.
	ObjectType type;
	// Culoarea obiectului.
	Color color;
	// Umple cu culoare.
	bool fill;
	// E obstacol miscator.
	bool moving;

	Object2() {}
	Object2(vector<Point2 *> _points, ObjectType _type, Color _color, bool _fill) :
		type(_type), color(_color), fill(_fill), moving(false)
	{
		for (vector<Point2 *>::iterator i = _points.begin(); i != _points.end(); ++i)
			addPointInternal(*(*i));
	}

	virtual ~Object2()
	{
		while (!points.empty())
		{
			delete points.back();
			points.pop_back();
			delete transf_points.back();
			transf_points.pop_back();
		}
	}

	// Calculeaza centrul unui obiect.
	// In functie de transf, ne intoarce centrul punctelor transformate sau netransformate.
	Point2 *getCenter(bool transf = true)
	{
		Point2 *p = new Point2();
		vector<Point2 *> v = transf ? transf_points : points;

		for (vector<Point2 *>::iterator i = v.begin(); i != v.end(); ++i)
		{
			p->x += (*i)->x;
			p->y += (*i)->y;
		}

		p->x /= v.size();
		p->y /= v.size();

		return p;
	}

protected:
	// Protected constructor for subclasses.
	Object2(ObjectType _type, Color _color, bool _fill) : type(_type), color(_color), fill(_fill), moving(false) {}

	// Protected point adding.
	void addPointInternal(Point2 p)
	{
		points.push_back(new Point2(p.x, p.y));
		transf_points.push_back(new Point2(p.x, p.y));
	}
};
