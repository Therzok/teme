#pragma once

// Clasa pentru un Punct in 2D.
class Point2
{
public:
	// Componente 2D.
	float x, y;

	Point2() :
		x(0.f), y(0.f) {}
	Point2(float _x, float _y) :
		x(_x), y(_y) {}
	~Point2() {}
};