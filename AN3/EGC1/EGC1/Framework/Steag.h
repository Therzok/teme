#pragma once
#include "Object2.h"

// Clasa de baza pentru checkpoint.
class Steag : public Object2
{
public:
	Steag(Point2 base) : Object2(OT_STEAG, Color(1, 1, 0), false)
	{
		addPointInternal(base);
		addPointInternal(Point2(base.x, base.y + 64));
		addPointInternal(Point2(base.x - 32, base.y + 48));
		addPointInternal(Point2(base.x, base.y + 32));
	}

	~Steag() {}
};