#pragma once
#include "Object2.h"

// Clasa de baza pentru Dreptunghi.
class Rectangle2 : public Object2
{
public:
	Rectangle2(Point2 p, float _width, float _height) : Object2(OT_RECTANGLE, Color(), false)
	{
		constructPoints(p, _width, _height);
	}

	Rectangle2(Point2 p, float _width, float _height, Color _color, bool _fill) : Object2(OT_RECTANGLE, _color, _fill)
	{
		constructPoints(p, _width, _height);
	}

	~Rectangle2() {}

	// Intoarce coordonatele dreapta-jos ale dreptunghiului.
	// Apelantul trebuie sa elibereze memoria.
	Point2 *lowRight()
	{
		Point2 lr = Point2(0, 1000000);
		for (int i = 0; i < 4; ++i)
			if (lr.x <= transf_points[i]->x && lr.y >= transf_points[i]->y) {
				lr.x = transf_points[i]->x;
				lr.y = transf_points[i]->y;
			}
		return new Point2(lr.x, lr.y);
	}

private:
	void constructPoints(Point2 p, float w, float h)
	{
		addPointInternal(p);
		addPointInternal(Point2(p.x + w, p.y));
		addPointInternal(Point2(p.x + w, p.y + h));
		addPointInternal(Point2(p.x, p.y + h));
	}
};