#pragma once
#include "Framework/Rectangle2.h"
#include "Framework/Visual2.h"

#ifdef _MSC_VER
#include "..\dependente\freeglut.h"
#else
#include "GL/freeglut.h"
#endif

// Calculeaza centrul pe interfata a unui element.
#define MIDDLE(max, min) \
	((max - min) / 2)

// Aplica functia pe fiecare element al actorului.
#define FOREACH_ACTOR_PARAMS(FUNC, ...) \
	FOREACH_VECTOR_PARAMS(FUNC, Rectangle2 *, actor , ##__VA_ARGS__)

namespace game
{
	// Elementele ce constituie actorul.
	extern vector<Rectangle2 *> actor;
	// Contextul actorului.
	extern Visual2 *v2actor;
	// Contextul scenei.
	extern Visual2 *v2course;

	// Callbacks

	void onInit();
	void onIdle();
	void onDisplay();
	void onKey(unsigned char key);
	void onKeyUp(unsigned char key);
	void onMouse(int button, int state, int x, int y);
}
