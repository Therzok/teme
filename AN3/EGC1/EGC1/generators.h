#include <vector>

// Apeleaza functia pe fiecare element din vector cu argumentele date.
#define FOREACH_VECTOR_PARAMS(FUNC, TYPE, VECTOR, ...) \
	for (vector<TYPE>::reverse_iterator i = VECTOR.rbegin(); i != VECTOR.rend(); ++i) \
		FUNC(*i , ##__VA_ARGS__)

namespace constants
{
#define FONT GLUT_BITMAP_TIMES_ROMAN_24

#define PI 3.14159265358979323846f

	// UI constants.

	const int MARGIN = 14;
	const int CHAR_HEIGHT = 24;
	const int ACTOR_SIZE = 64;
	const float GAME_BORDER = 100.f;
	const float GAME_START = 300.f;
	const float ACTOR_EYE_Y = GAME_BORDER + 40.f;
	const float ACTOR_MOUTH_Y = GAME_BORDER + 10.f;
	const float EPS = 0.01f;
	const float FLAG_HEIGHT = 60;
	const float CIRCLE_RADIUS = 30.f;

	// Game constants.

	const int MAX_LIVES = 3;
	const int MAX_SCORE = 5;

	enum Difficulty
	{
		EASY = 0,
		HARD = 1,
	};

	// Textul asociat dificultatii. "Easy"/"Hard"
	extern const char *DIFFICULTY_TEXT[];

	// Inaltimea sariturii.
	const float JUMP_DISTANCE = 120.f;
	// Viteza de ascensiune/descensiune.
	const float TRANSLATION_SPEED[] =
	{
		3.f,
		6.f,
	};
	// Viteza de miscare a actorului.
	const float MOVEMENT_SPEED[] =
	{
		3.f,
		6.f
	};
	// Viteza de rotire.
	const float ROTATION_SPEED[] = {
		PI / 2 / (JUMP_DISTANCE / TRANSLATION_SPEED[0]),
		PI / 2 / (JUMP_DISTANCE / TRANSLATION_SPEED[1]),
	};
}

namespace generators
{
	// Inlocuieste actorul principal cu unul dintr-o lista predefinita.
	// rev va merge inapoi in lista.
	void shuffleActors(bool rev = false);
	// Genereaza un nivel nou aleator.
	void generateLevel();
}