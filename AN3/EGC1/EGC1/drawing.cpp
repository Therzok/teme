#include <algorithm>
#include <iostream>
#include <cassert>
#include "drawing.h"

using namespace std;

namespace drawing
{
	Visual2 *main_visual;
	vector<Visual2 *> visuals;

	// From Framework.
	void drawObjects(Visual2 *v)
	{
		// Desenare text.
		for (vector<Text *>::iterator i = v->texts.begin(); i != v->texts.end(); ++i)
		{
			glColor3f((*i)->color.r, (*i)->color.g, (*i)->color.b);
			glRasterPos2f((*i)->pos.x, (*i)->pos.y);
			glutBitmapString((*i)->font, (const unsigned char*)(*i)->text.c_str());
		}

		// Desenare obiecte
		for (vector<Object2 *>::iterator i = v->objects.begin(); i != v->objects.end(); ++i)
		{
			glColor3f((*i)->color.r, (*i)->color.g, (*i)->color.b);
			// Trebuie sa avem minim 3 puncte.
			assert((*i)->points.size() >= 3); 
 
			if ((*i)->fill)
				glBegin(GL_TRIANGLE_FAN); // TRIANGLE_FAN cere doar un punct, fata de GL_TRIANGLES
			else
				glBegin(GL_LINE_LOOP); // GL_LINE_LOOP cere doar un punct, fata de GL_LINES

			for (std::vector<Point2 *>::iterator j = (*i)->transf_points.begin(); j != (*i)->transf_points.end(); ++j)
				glVertex3f((*j)->x, (*j)->y, 0);

			glEnd();
		}
	}

	void addVisual2D(Visual2 *v)
	{
		visuals.push_back(v);
	}

	void removeVisual2D(Visual2 *v)
	{
		visuals.erase(remove(visuals.begin(), visuals.end(), v));
	}

	void addText(Text *text, Visual2 *v)
	{
		v->texts.push_back(text);
	}

	void removeText(Text *text, Visual2 *v)
	{
		v->texts.erase(remove(v->texts.begin(), v->texts.end(), text));
	}

	void addObject2D(Object2 *o, Visual2 *v)
	{
		v->objects.push_back(o);
	}

	void removeObject2D(Object2 *o, Visual2 *v)
	{
		v->objects.erase(remove(v->objects.begin(), v->objects.end(), o));
	}
}