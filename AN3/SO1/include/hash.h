
#ifndef HASH_H_
#define HASH_H_

/**
 * Hashes a string for the size.
 *
 * @param str The string to be hashed.
 * @param hash_length The size of the hashtable.
 * @return The hash of the string.
 */
unsigned int hash(const char *str, unsigned int hash_length);

#endif

