/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef HASHTABLE_H_
#define HASHTABLE_H_

/**
 * Hashtable entry bucket structure
 *
 * Iterate it with `so1_hashtable_bucket_iter`.
 */
struct hashtable_entry;

/**
 * Hashtable structure
 *
 * Manipulate it with `so1_hashtable_` functions and iterate with
 * `so1_hashtable_iter`.
 */
struct hashtable;

/**
 * Creates and initializes a new hashtable.
 *
 * @param size The size of the hashtable.
 * @return The hashtable. NULL if failure.
 */
struct hashtable *so1_hashtable_new(unsigned int size);

/**
 * Clears the hashtable entries.
 *
 * @param ht The hashtable.
 */
void so1_hashtable_clear(struct hashtable *ht);

/**
 * Frees the whole hashtable.
 *
 * @param ht The hashtable.
 */
void so1_hashtable_free(struct hashtable *ht);

/**
 * Gets a hashtable entry for a given key.
 *
 * @param ht The hashtable.
 * @param key The key to be hashed.
 * @return The hashtable entry. NULL if non-existent.
 */
struct hashtable_entry *so1_hashtable_get(struct hashtable *ht,
					  char *key);

/**
 * Gets a bucket of the hashtable.
 *
 * @param ht The hashtable.
 * @param bucket The bucket.
 * @return The hashtable bucket. NULL if empty.
 */
struct hashtable_entry *so1_hashtable_bucket(struct hashtable *ht,
					     unsigned int bucket);

/**
 * Adds a key to the hashtable.
 *
 * @param ht The hashtable.
 * @param key The key to be hashed.
 * @return The entry that was allocated. NULL on failure.
 */
struct hashtable_entry *so1_hashtable_add(struct hashtable *ht, char *key);

/**
 * Removes a key from the hashtable.
 *
 * @param ht The hashtable.
 * @param key The key.
 */
void so1_hashtable_remove(struct hashtable *ht, char *key);

/**
 * Doubles a hashtable's size.
 *
 * @param ht The hashtable.
 * @return ht. NULL on failure.
 */
struct hashtable *so1_hashtable_double(struct hashtable *ht);

/**
 * Halves a hashtable's size.
 *
 * @param ht The hashtable.
 * @return ht. NULL on failure.
 */
struct hashtable *so1_hashtable_halve(struct hashtable *ht);

/**
 * Initiates an iteration of a hashtable.
 *
 * @param ht The hashtable.
 * @param cb The callback to be used on each entry list.
 * return 0 to continue, non-zero to stop iterating.
 * @param userdata The payload to be used across iterations.
 */
void so1_hashtable_iter(struct hashtable *ht,
			int (*iterate_table_cb)
			(struct hashtable_entry *entry,
			 void *userdata),
			void *userdata);


/**
 * Initiates an iteration of a hashtable bucket.
 *
 * @param ht The hashtable.
 * @param cb The callback to be used on each bucket.
 * return 0 to continue, non-zero to stop iterating.
 * @param userdata The payload to be used across iterations.
 */
void so1_hashtable_bucket_iter(struct hashtable_entry *entry,
			       int (*iterate_entry_cb)
			       (char *key,
				void *userdata),
			       void *userdata);

#endif

