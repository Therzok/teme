/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>

#include "parse.h"
#include "hashtable.h"
#include "utils.h"

/**
 * The hashtable that gets allocated.
 */
static struct hashtable *ht;

/**
 * The current file that is being parsed.
 */
static FILE *input;

/**
 * The number of operations the cli supports.
 */
#define OP_COUNT 7

/**
 * Reference command values for operations.
 */
static const char *op_names[OP_COUNT] = {
	/* Adds a key to the hashtable - cmd word */
	"add",
	/* Removes a key from the hashtable - cmd word */
	"remove",
	/* Tries to find a key in the hashtable - cmd word [file] */
	"find",
	/* Clears a hashtable - cmd */
	"clear",
	/* Prints the contents of a bucket - cmd idx [file] */
	"print_bucket",
	/* Prints the contents of the hashtable - cmd [file] */
	"print",
	/* Resizes the hashtable - cmd double/halve */
	"resize",
};

/**
 * The separator used for splitting tokens.
 */
static const char *sep = "\n ";

/**
 * Ends parsing for the last retained file.
 * Cleans up memory.
 */
static void parse_end(void)
{
	int err;

	/* There is no file open, exit early. */
	if (!input)
		return;

	/* Guaranteed to be called only on non-stdin. */
	err = fclose(input);
	if (err)
		DIE_CB("fclose failure", so1_parse_free);

	input = NULL;
}

/**
 * Closes the file and frees the hashtable on any failure.
 */
static void parse_failure(void)
{
	parse_end();
	so1_parse_free();
}

/**
 * Gets the next word in the command stream.
 * Dies if the word is requested and doesn't exist.
 *
 * @param err_msg The message to be used in case of failure.
 * @return The word.
 */
static char *get_next_word(const char *err_msg)
{
	char *word;

	word = strtok(NULL, sep);
	if (!word)
		DIE_CB(err_msg, parse_failure);

	return word;
}

/**
 * Opens the file and keeps track of it.
 * If the file doesn't exist, fallback to stdin.
 *
 * @return The FILE * of the file.
 */
static FILE *get_output_file()
{
	FILE *file;
	char *filename;

	file = NULL;
	filename = strtok(NULL, sep);
	if (filename)
		file = fopen(filename, "a");

	if (!file)
		file = stdout;

	return file;
}

/**
 * Closes any non-stdout file.
 *
 * @param file The file to be closed.
 */
static void close_output_file(FILE *file)
{
	if (file != stdout)
		fclose(file);
}

/**
 * Adds the next word in the command to the hashtable.
 */
static void cmd_add(void)
{
	if (!so1_hashtable_add(ht, get_next_word("add: missing word")))
		DIE_CB("hashtable add failure.", parse_failure);
}

/**
 * Removes the next word in the command from the hashtable.
 */
static void cmd_remove(void)
{
	so1_hashtable_remove(ht, get_next_word("remove: missing word"));
}

/**
 * Tries to find the next word in the command in the hashtable.
 * Prints "True\n" or "False\n" to the given file or stdout.
 */
static void cmd_find(void)
{
	const char *result;
	char *word;
	FILE *file;

	word = get_next_word("find: missing word");
	file = get_output_file();

	/* Get the file name, if NULL, use stdout. */
	result = so1_hashtable_get(ht, word) ? "True" : "False";
	fprintf(file, "%s\n", result);

	close_output_file(file);
}

/**
 * Clears the hashtable entries.
 */
static void cmd_clear(void)
{
	so1_hashtable_clear(ht);
}

/**
 * Callback which prints the key in the FILE in the userdata.
 *
 * @param key The hashtable entry's key.
 * @param userdata The FILE * passed.
 * @return 0. Always continues iteration.
 */
static int cmd_print_entry_cb(char *key,
			       void *userdata)
{
	FILE *file = userdata;

	fprintf(file, "%s ", key);
	return 0;
}

/**
 * Callback which iteratively prints keys in the table buckets.
 *
 * @param entry The hashtable entry.
 * @param userdata The FILE * passed.
 * @return 0. Always continues iteration.
 */
static int cmd_print_table_cb(struct hashtable_entry *entry,
			       void *userdata)
{
	FILE *file = userdata;

	so1_hashtable_bucket_iter(entry, cmd_print_entry_cb, file);
	fprintf(file, "%s", "\n");
	return 0;
}

/**
 * Prints the contents of a given bucket index in the given FILE or stdout.
 */
static void cmd_print_bucket(void)
{
	struct hashtable_entry *entry;
	char *word;
	FILE *file;
	unsigned int bucket;

	word = get_next_word("print bucket: missing bucket");
	if (sscanf(word, "%u", &bucket) < 1)
		DIE_CB("sscanf: not an unsigned number", parse_failure);

	file = get_output_file();

	entry = so1_hashtable_bucket(ht, bucket);
	so1_hashtable_bucket_iter(entry, cmd_print_entry_cb, file);
	fprintf(file, "\n");

	close_output_file(file);
}

/**
 * Prints the whole hashtable in the given file or stdout.
 */
static void cmd_print(void)
{
	FILE *file;

	file = get_output_file();

	so1_hashtable_iter(ht, cmd_print_table_cb, file);
	fprintf(file, "\n");

	close_output_file(file);
}

/**
 * Resizes the hashtable based on the next command word.
 * Can 'double' or 'halve' the hashtable..
 */
static void cmd_resize(void)
{
	char *word;

	word = get_next_word("resize: missing param");

	if (!strcmp("double", word))
		ht = so1_hashtable_double(ht);
	else if (!strcmp("halve", word))
		ht = so1_hashtable_halve(ht);

	if (!ht)
		DIE_CB("allocation failure: resize", parse_failure);
}

/**
 * Array of callbacks to command handlers.
 */
static void (*command_cb[OP_COUNT])(void) = {
	cmd_add,
	cmd_remove,
	cmd_find,
	cmd_clear,
	cmd_print_bucket,
	cmd_print,
	cmd_resize,
};

void so1_parse_new(unsigned int bucket_size)
{
	ht = so1_hashtable_new(bucket_size);
	DIE(!ht, "malloc failure: hashtable");
}

void so1_parse_free(void)
{
	so1_hashtable_free(ht);
}

/**
 * Scans a line, grabbing the command and dispatching the handler.
 * Initiates a strtok which can be used by handlers to get the next tokens.
 */
static void parse_line(char *line)
{
	char *command;
	int i;

	/* Parse the command and return if empty line. */
	command = strtok(line, sep);
	if (!command)
		return;

	/* Try matching a known command string. */
	for (i = 0; i < OP_COUNT; ++i)
		if (!strcmp(command, op_names[i]))
			command_cb[i]();
}

/* 20k block buffer - as per SO docs */
#define BUFSIZE 20000

/**
 * Processes a file by reading each line and feeding it to parse_line.
 * Maximum line size is 20k.
 */
static void read_file(FILE *f)
{
	char line[BUFSIZE];

	/* Process operation line by line. */
	while (!feof(f) && fgets(line, BUFSIZE, f))
		parse_line(line);
}

#undef BUFSIZE

void so1_parse_stdin(void)
{
	read_file(stdin);
}

void so1_parse_file(char *filename)
{
	input = fopen(filename, "r");

	/* We don't care about non-existent files. */
	if (!input)
		return;

	read_file(input);
	parse_end();
}

