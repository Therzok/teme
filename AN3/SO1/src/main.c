/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>

#include "parse.h"
#include "utils.h"

/**
 * Processes each given file in a hashtable of given size.
 *
 * @param bucket_size The initial hashtable size.
 * @param files The files to be processed.
 * @param file_count The number of files to be processed. 0 if stdin is used
 */
static void read_files(unsigned int bucket_size,
		       char **files,
		       unsigned int file_count)
{
	unsigned int i;

	so1_parse_new(bucket_size);

	if (file_count == 0) {
		so1_parse_stdin();
		goto cleanup;
	}

	for (i = 0; i < file_count; ++i)
		so1_parse_file(files[i]);

cleanup:
	so1_parse_free();
}

int main(int argc, char *argv[])
{
	unsigned int initial_length;
	int ret;

	/* No perror is set, but test doesn't pass without DIE. */
	DIE(argc < 2, "Usage: tema1 hash_size [input_files...]");

	/* Get the bucket size. */
	ret = sscanf(argv[1], "%u", &initial_length);
	DIE(ret < 1, "unsigned length expected");

	/* Here be dragons! Pass only file parameters. */
	read_files(initial_length, argc > 2 ? argv + 2 : NULL, argc - 2);

	return 0;
}
