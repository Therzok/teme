/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>

#include "hash.h"
#include "hashtable.h"
#include "utils.h"

/**
 * An entry contains a key and a pointer to the next entry.
 */
struct hashtable_entry {
	char *key;
	struct hashtable_entry *next;
};

/**
 * A hashtable contains the size of the bucket list and the buckets.
 */
struct hashtable {
	unsigned int size;
	struct hashtable_entry **buckets;
};

/**
 * Returns a list of hashtable entries for the given size.
 * @param size The size of the list.
 * @return The list of entries NULL-ed out. NULL for failure.
 */
static struct hashtable_entry **hashtable_alloc_buckets(unsigned int size)
{
	struct hashtable_entry **buckets;
	unsigned int i;

	/* Don't rely on calloc.
	 * Not all implementations use zero bit null pointers.
	 * http://c-faq.com/null/machexamp.html
	 */
	buckets = malloc(size * sizeof(struct hashtable_entry *));
	if (!buckets)
		goto ret;

	for (i = 0; i < size; ++i)
		buckets[i] = NULL;

ret:
	return buckets;
}

struct hashtable *so1_hashtable_new(unsigned int size)
{
	struct hashtable *ht;

	ht = malloc(sizeof(struct hashtable));
	if (!ht)
		goto ret;

	ht->buckets = hashtable_alloc_buckets(size);
	ht->size = size;

ret:
	return ht;
}

/**
 * Frees the contents of a given entry list.
 * @param buckets The list of entries.
 * @param size The size of the list.
 */
static void hashtable_clear_buckets(struct hashtable_entry **buckets,
				    unsigned int size)
{
	struct hashtable_entry *entry, *next;
	unsigned int i;

	for (i = 0; i < size; ++i)
		for (entry = buckets[i]; entry; entry = next) {
			next = entry->next;
			free(entry->key);
			free(entry);
			buckets[i] = NULL;
		}
}

void so1_hashtable_clear(struct hashtable *ht)
{
	hashtable_clear_buckets(ht->buckets, ht->size);
}

void so1_hashtable_free(struct hashtable *ht)
{
	so1_hashtable_clear(ht);
	free(ht->buckets);
	free(ht);
}

struct hashtable_entry *so1_hashtable_get(struct hashtable *ht, char *key)
{
	unsigned int hash_value;
	struct hashtable_entry *entry;

	hash_value = hash(key, ht->size);

	for (entry = ht->buckets[hash_value]; entry; entry = entry->next)
		if (!strcmp(key, entry->key))
			break;

	return entry;
}

struct hashtable_entry *so1_hashtable_bucket(struct hashtable *ht,
					     unsigned int bucket)
{
	return ht->buckets[bucket];
}

struct hashtable_entry *so1_hashtable_add(struct hashtable *ht, char *key)
{
	unsigned int hash_value;
	struct hashtable_entry *entry, *old;

	hash_value = hash(key, ht->size);

	for (old = NULL, entry = ht->buckets[hash_value];
	     entry;
	     old = entry, entry = entry->next)
		if (!strcmp(key, entry->key))
			goto ret;

	entry = malloc(sizeof(struct hashtable_entry));
	if (!entry)
		goto ret;

	entry->key = malloc((strlen(key) + 1) * sizeof(char));
	if (!entry->key) {
		free(entry);
		goto ret;
	}

	strcpy(entry->key, key);
	entry->next = NULL;

	if (old)
		old->next = entry;
	else
		ht->buckets[hash_value] = entry;

ret:
	return entry;
}

void so1_hashtable_remove(struct hashtable *ht, char *key)
{
	unsigned int hash_value;
	struct hashtable_entry *entry, *old;

	hash_value = hash(key, ht->size);

	for (old = NULL, entry = ht->buckets[hash_value];
	     entry;
	     old = entry, entry = entry->next) {
		if (strcmp(entry->key, key))
			continue;

		if (old)
			old->next = entry->next;
		else
			ht->buckets[hash_value] = entry->next;

		free(entry->key);
		free(entry);
		entry = NULL;
		break;
	}
}

/**
 * Resizes a hashtable to the new size.
 *
 * @param ht The hashtable to be resized.
 * @param new_size The new size to be resized to.
 */
static struct hashtable *hashtable_resize(struct hashtable *ht,
					  unsigned int new_size)
{
	struct hashtable_entry **buckets, *entry;
	unsigned int size, i;

	/* Keep old values and create new storage. */
	buckets = ht->buckets;
	size = ht->size;

	ht->buckets = hashtable_alloc_buckets(new_size);
	ht->size = new_size;

	for (i = 0; i < size; ++i)
		for (entry = buckets[i]; entry; entry = entry->next) {
			if (so1_hashtable_add(ht, entry->key))
				continue;

			/* Clear the hashtable to free memory on error. */
			so1_hashtable_free(ht);
			ht = NULL;
			break;
		}

	hashtable_clear_buckets(buckets, size);
	free(buckets);
	return ht;
}

struct hashtable *so1_hashtable_double(struct hashtable *ht)
{
	return hashtable_resize(ht, ht->size * 2);
}

struct hashtable *so1_hashtable_halve(struct hashtable *ht)
{
	return hashtable_resize(ht, ht->size / 2);
}

void so1_hashtable_iter(struct hashtable *ht,
			int (*iterate_table_cb)
			(struct hashtable_entry *entry,
			 void *userdata),
			void *userdata)
{
	unsigned int i;

	for (i = 0; i < ht->size; ++i) {
		if (!ht->buckets[i])
			continue;

		if (iterate_table_cb(ht->buckets[i], userdata))
			break;
	}
}

void so1_hashtable_bucket_iter(struct hashtable_entry *entry,
			       int (*iterate_entry_cb)
			       (char *key,
				void *userdata),
			       void *userdata)
{
	for (; entry; entry = entry->next)
		if (iterate_entry_cb(entry->key, userdata))
			break;
}

