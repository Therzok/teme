/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>

#include "cmd.h"
#include "io.h"
#include "parser.h"
#include "utils.h"

/* Used by parser, do NOT remove. */
void parse_error(const char *str, const int where)
{
	fprintf(stderr, ERRFMT_PARSE, where, str);
}

static void start_shell(void)
{
	char *line;
	command_t *root;
	int ret;

	ret = 0;

	for (;;) {
		printf(PROMPT);
		fflush(stdout);

		root = NULL;
		line = so2_io_read_line();
		if (!line)
			break;

		parse_line(line, &root);
		if (root)
			ret = so2_cmd_parse(root);

		free_parse_memory();
		free(line);

		if (ret == SHELL_EXIT)
			break;
	}
}

int main(void)
{
	start_shell();
	return EXIT_SUCCESS;
}

