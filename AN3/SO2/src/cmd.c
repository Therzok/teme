/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>

#include "cmd.h"
#include "env.h"
#include "file.h"
#include "pipe.h"
#include "utils.h"

/* Custom environment variables that the shell supports. */
#define OLDPWD	"OLDPWD"
#define PWD	"PWD"
#define HOME	"HOME"

/**
 * Internal update PWD/OLDPWD commands.
 *
 * @param old The value to set OLDPWD to.
 * @param pwd The value to set PWD to.
 */
static void cmd_update_pwd(char *old)
{
	char *pwd;

	/* Ignore failure. Not critical. */
	if (old)
		so2_env_set(OLDPWD, old);

	pwd = so2_env_get_workdir();
	if (pwd)
		so2_env_set(PWD, pwd);

	free(pwd);
}

/**
 * Internal change-directory command.
 *
 * @param dir The word that contains the directory.
 */
static void cmd_shell_cd(word_t *dir)
{
	char *old, *dirname;
	int alloc;

	old = so2_env_get_workdir();
	alloc = 0;

	dirname = so2_util_get_word(dir);
	if (!dirname) {
		dirname = so2_env_get(HOME);
		DIE(!dirname, ERR_DIR);
	} else
		alloc = 1;

	if (so2_file_test_directory(dirname) < 0)
		goto free;

	DIE(so2_env_set_workdir(dirname) == -1, ERR_SET_WD);

	cmd_update_pwd(old);

free:
	free(old);

	if (alloc)
		free(dirname);
}

static void cmd_assign_env(simple_command_t *s)
{
	char *command, *envname, *envval;
	word_t *word;
	int rc;

	command = so2_util_get_word(s->verb);

	/* Take lhs. */
	envname = strtok(command, "=");

	/* Skip lh operand and = sign. */
	word = s->verb->next_part->next_part;
	if (word)
		envval = so2_util_get_word(word);
	else
		envval = "";

	rc = so2_env_set(envname, envval);
	DIE(rc, ERR_ENV);

	if (word)
		free(envval);
	free(command);
}

static int cmd_parse_simple(simple_command_t *s)
{
	char *command;

	command = so2_util_get_word(s->verb);

	/* Try executing built in commands first. */
	if (!strcmp(command, "exit") || !strcmp(command, "quit")) {
		free(command);
		return SHELL_EXIT;
	}

	if (!strcmp(command, "cd")) {
		free(command);

		/* Touch redirect files. */
		so2_file_close(so2_file_open(s, FILE_OUT));
		so2_file_close(so2_file_open(s, FILE_ERR));

		cmd_shell_cd(s->params);
		return 0;
	}
	free(command);

	/* Environment assignment. We have multiple parts. */
	if (s->verb->next_part) {
		cmd_assign_env(s);
		return 0;
	}

	return so2_cmd_parse_simple(s);
}

/**
 * Parse and execute a command.
 */
int so2_cmd_parse(command_t *c)
{
	int ret, negate, piped;

	negate = piped = 0;

	switch (c->op) {
	case OP_NONE:
		return cmd_parse_simple(c->scmd);

	case OP_SEQUENTIAL:
		so2_cmd_parse(c->cmd1);
		return so2_cmd_parse(c->cmd2);

	case OP_PIPE:
		piped = 1;
	case OP_PARALLEL:
		return so2_cmd_parse_parallel(c->cmd1, c->cmd2, piped);

	case OP_CONDITIONAL_ZERO:
		negate = 1;
	case OP_CONDITIONAL_NZERO:
		ret = (!so2_cmd_parse(c->cmd1)) ^ negate;
		if (ret)
			return ret;

		return so2_cmd_parse(c->cmd2);

	default:
		return SHELL_EXIT;
	}
}

