/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <windows.h>
#include <string.h>

#include "cmd.h"
#include "env.h"
#include "utils.h"

static LPTSTR util_get_argv(simple_command_t *command)
{
	LPTSTR argv, substring;
	word_t *param;
	DWORD string_length, substring_length;

	substring = NULL;
	string_length = substring_length = 0;

	argv = so2_util_get_word(command->verb);
	if (!argv)
		return NULL;

	string_length = strlen(argv);

	param = command->params;
	while (param != NULL) {
		substring = so2_util_get_word(param);
		substring_length = strlen(substring);

		argv = realloc(argv, string_length + substring_length + 4);
		DIE(!argv, ERR_ALLOC);

		strcat(argv, " ");

		/* Surround parameters with ' ' */
		strcat(argv, "'");
		strcat(argv, substring);
		strcat(argv, "'");

		string_length += substring_length + 3;
		param = param->next_word;

		free(substring);
	}

	return argv;
}

INT so2_cmd_parse_simple(simple_command_t *s)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	LPTSTR argv;
	DWORD ret;

	ZeroMemory(&si, sizeof(STARTUPINFO));
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	si.cb = sizeof(STARTUPINFO);

	so2_file_redirect_std(s, &si);

	ret = EXIT_SUCCESS;
	argv = util_get_argv(s);

	ret = CreateProcess(NULL,
			    argv,
			    NULL,
			    NULL,
			    TRUE,
			    0,
			    NULL,
			    NULL,
			    &si,
			    &pi);

	if (!ret) {
		if (GetLastError() == ERROR_FILE_NOT_FOUND) {
			fprintf(stderr, ERRFMT_EXEC, argv);
			fflush(stderr);
			return -1;
		}
	}

	DIE(WaitForSingleObject(pi.hProcess, INFINITE) == WAIT_FAILED,
	    ERR_PROC);
	DIE(!GetExitCodeProcess(pi.hProcess, &ret), ERR_CLOSE);

	DIE(!CloseHandle(pi.hThread), ERR_CLOSE);
	DIE(!CloseHandle(pi.hProcess), ERR_CLOSE);

	if (si.hStdInput != GetStdHandle(STD_INPUT_HANDLE))
		CloseHandle(si.hStdInput);
	if (si.hStdOutput != GetStdHandle(STD_OUTPUT_HANDLE))
		CloseHandle(si.hStdOutput);
	if (si.hStdError != GetStdHandle(STD_ERROR_HANDLE))
		CloseHandle(si.hStdError);

	free(argv);
	return ret;
}

DWORD WINAPI cmd_thread_function(LPVOID param)
{
	return so2_cmd_parse(param);
}

INT so2_cmd_parse_parallel(command_t *c1, command_t *c2, int piped)
{
	HANDLE thread;
	DWORD thread_id;
	SECURITY_ATTRIBUTES sa;
	INT ret;

	thread = CreateThread(
	    &sa,
	    0,
	    cmd_thread_function,
	    c1,
	    0,
	    &thread_id);
	DIE(!thread, ERR_PROC);

	ret = so2_cmd_parse(c2);
	DIE(WaitForSingleObject(thread, INFINITE) == WAIT_FAILED, ERR_PROC);
	DIE(!CloseHandle(thread), ERR_CLOSE);

	return ret;
}
