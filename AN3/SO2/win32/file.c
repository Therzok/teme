/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <windows.h>

#include "utils.h"
#include "file.h"

struct file {
	HANDLE h;
};

static LPTSTR file_name(simple_command_t *s, int open_type)
{
	switch (open_type) {
	case FILE_IN:	return so2_util_get_word(s->in);
	case FILE_OUT:	return so2_util_get_word(s->out);
	case FILE_ERR:	return so2_util_get_word(s->err);
	default:	return NULL;
	};
}

struct file *so2_file_standard_open(int open_type)
{
	struct file *file;

	file = so2_file_alloc();
	if (!file)
		goto done;

	switch (open_type) {
	case FILE_IN:
		file->h = GetStdHandle(STD_INPUT_HANDLE);
		break;

	case FILE_OUT:
		file->h = GetStdHandle(STD_OUTPUT_HANDLE);
		break;

	case FILE_ERR:
		file->h = GetStdHandle(STD_ERROR_HANDLE);
		break;
	}

done:
	return file;
}

struct file *so2_file_open(simple_command_t *s, int open_type)
{
	SECURITY_ATTRIBUTES sa;
	struct file *file;
	LPTSTR filename;
	DWORD access, share_mode, disposition;
	HANDLE h;

	if (open_type >= FILE_COUNT)
		return NULL;

	file = NULL;
	ZeroMemory(&sa, sizeof(SECURITY_ATTRIBUTES));
	sa.bInheritHandle = TRUE;

	if (open_type == FILE_IN) {
		access = GENERIC_READ;
		share_mode = FILE_SHARE_READ;
		disposition = OPEN_EXISTING;
	} else {
		access = GENERIC_WRITE;
		share_mode = FILE_SHARE_WRITE;
		disposition = CREATE_ALWAYS;
		if ((open_type == FILE_OUT && (s->io_flags & IO_OUT_APPEND)) ||
		    (open_type == FILE_ERR && (s->io_flags & IO_ERR_APPEND)))
			disposition = OPEN_ALWAYS;
	}

	filename = file_name(s, open_type);
	if (!filename)
		return NULL;

	h = CreateFile(filename,
		       access,
		       share_mode,
		       &sa,
		       disposition,
		       FILE_ATTRIBUTE_NORMAL,
		       NULL);
	if (h == INVALID_HANDLE_VALUE)
		goto clean_filename;

	if (disposition == OPEN_ALWAYS &&
	    SetFilePointer(h,
			   0,
			   0,
			   FILE_END) == INVALID_SET_FILE_POINTER) {
		CloseHandle(h);
		goto clean_filename;
	}

	file = so2_file_alloc();
	if (!file) {
		CloseHandle(h);
		goto clean_filename;
	}

	file->h = h;

clean_filename:
	free(filename);
	return file;
}

struct file *so2_file_alloc()
{
	return malloc(sizeof(struct file));
}

void so2_file_free(struct file *file)
{
	free(file);
}

int so2_file_close(struct file *file)
{
	HANDLE h;

	if (!file)
		return 0;

	h = file->h;
	so2_file_free(file);
	return !CloseHandle(h);
}

int so2_file_redirect(struct file *from, struct file *to)
{
	return 0;
}

int so2_file_redirect_std(simple_command_t *s, void *userdata)
{
	STARTUPINFO *si;
	LPTSTR fileout, fileerr;
	struct file *f;

	si = userdata;

	si->dwFlags |= STARTF_USESTDHANDLES;
	si->hStdInput = GetStdHandle(STD_INPUT_HANDLE);
	si->hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	si->hStdError = GetStdHandle(STD_ERROR_HANDLE);

	f = so2_file_open(s, FILE_IN);
	if (f) {
		si->hStdInput = f->h;
		so2_file_free(f);
	}

	f = so2_file_open(s, FILE_OUT);
	if (f) {
		si->hStdOutput = f->h;
		so2_file_free(f);
	}

	fileout = so2_util_get_word(s->out);
	fileerr = so2_util_get_word(s->err);
	if (fileout && fileerr && !strcmp(fileout, fileerr))
		si->hStdError = si->hStdOutput;
	else {
		f = so2_file_open(s, FILE_ERR);
		if (f) {
			si->hStdError = f->h;
			so2_file_free(f);
		}
	}
	free(fileout);
	free(fileerr);

	return 0;
}

int so2_file_test_directory(char *file)
{
	return 0;
}
