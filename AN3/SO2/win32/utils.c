/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <windows.h>

#include "env.h"
#include "parser.h"

/**
 * Concatenate parts of the word to obtain the command
 */
LPTSTR so2_util_get_word(word_t *s)
{
	PVOID tmp;
	DWORD string_length, substring_length;
	LPTSTR string, substring, env;

	string = NULL;
	string_length = substring_length = 0;

	while (s != NULL) {
		if (s->expand) {
			env = so2_env_get(s->string);
			if (env) {
				substring = malloc(strlen(env) + 1);
				if (!substring) {
					free(string);
					free(env);
					return NULL;
				}

				strcpy(substring, env);
				free(env);
			} else
				substring = NULL;
		} else {
			substring = malloc(strlen(s->string) + 1);
			if (!substring) {
				free(string);
				return NULL;
			}

			strcpy(substring, s->string);
		}

		substring_length = substring ? strlen(substring) : 0;

		tmp = realloc(string, string_length + substring_length + 1);
		if (!tmp) {
			free(substring);
			free(string);
			return NULL;
		}
		string = tmp;
		memset(string + string_length, 0, substring_length + 1);

		if (substring)
			strcat(string, substring);
		string_length += substring_length;

		s = s->next_part;

		free(substring);
		substring = NULL;
	}

	return string;
}

VOID PrintLastError(const PCHAR message)
{
	CHAR errBuff[1024];

	FormatMessage(
	    FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_MAX_WIDTH_MASK,
	    NULL,
	    GetLastError(),
	    0,
	    errBuff,
	    sizeof(errBuff) - 1,
	    NULL);
	fprintf(stderr, "%s: %s\n", message, errBuff);
}

