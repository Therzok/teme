/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <windows.h>

#define MAX_SIZE_ENVIRONMENT_VARIABLE	100

LPTSTR so2_env_get(LPTSTR name)
{
	LPSTR substring;
	DWORD dwret;

	substring = malloc(MAX_SIZE_ENVIRONMENT_VARIABLE * sizeof(char));
	dwret = GetEnvironmentVariable(name,
				       substring,
				       MAX_SIZE_ENVIRONMENT_VARIABLE);

	if (!dwret) {
		free(substring);
		return NULL;
	}

	return substring;
}

INT so2_env_set(LPTSTR name, LPTSTR value)
{
	return !SetEnvironmentVariable(name, value);
}

LPTSTR so2_env_get_workdir(void)
{
	LPSTR substring;
	DWORD dwret;

	substring = malloc(MAX_SIZE_ENVIRONMENT_VARIABLE * sizeof(char));
	dwret = GetCurrentDirectory(MAX_SIZE_ENVIRONMENT_VARIABLE, substring);

	if (!dwret) {
		free(substring);
		return NULL;
	}

	return substring;
}

INT so2_env_set_workdir(LPCTSTR workdir)
{
	return !SetCurrentDirectory(workdir);
}
