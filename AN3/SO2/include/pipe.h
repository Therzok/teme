/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef PIPE_H_
#define PIPE_H_

#include "io.h"

#define PIPE_IN		0
#define PIPE_OUT	1
#define PIPE_COUNT	2

/**
 * Struct to define a pipe connection.
 */
struct pipe;

/**
 * Create a pipe for the connection.
 *
 * @return The pipe to be used. Release with so2_pipe_free.
 */
struct pipe *so2_pipe_open(void);

/**
 * Releases a pipe structure.
 *
 * @param pipe The pipe to be released.
 */
void so2_pipe_free(struct pipe *pipe);

/**
 * Connect the pipe end to the standard device.
 *
 * @param pipe The pipe to be used.
 * @param file_open_type FILE_IN or FILE_OUT or FILE_ERR.
 * @param pipe_open_type PIPE_IN or PIPE_OUT.
 */
int so2_pipe_connect(struct pipe *pipe,
		     int file_open_type,
		     int pipe_open_type);

#endif

