/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef CMD_H_
#define CMD_H_

#include "parser.h"
#include "pipe.h"

/* Prompt string to show on message. */
#define PROMPT		"> "

/**
 * Parse and execute a command.
 *
 * @param c The command to parse.
 * @return The return code of the process that ran the command.
 */
int so2_cmd_parse(command_t *c);

/**
 * Parse a simple command (internal, env var assign, external).
 *
 * @param s The simple command with the commandline data.
 * @return int The return code of the process that ran the command.
 */
int so2_cmd_parse_simple(simple_command_t *s);

/**
 * Parse two commands in parallel. If piped, it will be sequential by
 * pipe nature.
 *
 * @param c1 The first command to execute. If piped, the process pipes out.
 * @param c2 The second command to execute. If piped, the process pipes in.
 * @param piped Whether to execute the commands piped.
 *
 * @return The return code of the block.
 */
int so2_cmd_parse_parallel(command_t *c1, command_t *c2, int piped);

#endif

