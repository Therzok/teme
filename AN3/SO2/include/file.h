/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef FILE_H_
#define FILE_H_

#include "cmd.h"

#define FILE_IN		0
#define FILE_OUT	1
#define FILE_ERR	2
#define FILE_COUNT	3

/**
 * Struct to define a file handle.
 */
struct file;

/**
 * Opens the standard terminal information.
 *
 * @param open_type FILE_IN or FILE_OUT or FILE_ERR.
 * @return A file structure. Release with so2_file_free.
 */
struct file *so2_file_standard_open(int open_type);

/**
 * Opens a file for processing.
 *
 * @param s The simple command to use data from for opening the file.
 * @param open_type FILE_IN or FILE_OUT or FILE_ERR.
 * @return A file structure. Release with so_file_close.
 */
struct file *so2_file_open(simple_command_t *s, int open_type);

/**
 * Allocate a file structure. Don't use if you don't know what you're
 * doing. This exists for pipe communication.
 *
 * @return An empty file structure. No guarantee on what will happen if
 * you use it in any other function.
 */
struct file *so2_file_alloc();

/**
 * Frees an open file.
 *
 * @param file The standard terminal file structure to free. Does not
 * close the file.
 */
void so2_file_free(struct file *file);

/**
 * Closes an open file.
 *
 * @param file The file structure to close or NULL.
 * @return 0 on success, non-zero on error.
 */
int so2_file_close(struct file *file);

/**
 * Redirects the `from` stream to `to`.
 *
 * @param from The file to redirect from.
 * @param to The file to redirect to.
 *
 * @return 0 on success, non-zero on failure.
 */
int so2_file_redirect(struct file *from, struct file *to);

/**
 * Redirects the standard devices to the command given files.
 *
 * @param s The command to get redirect data from.
 * @param userdata Payload.
 * @return 0 on success, non-zero on failure.
 */
int so2_file_redirect_std(simple_command_t *s, void *userdata);

/**
 * Tests whether the given file is a directory. Prints output in case of
 * error.
 *
 * @param file The file to test.
 * @return int 0 on success, non-zero on failure.
 */
int so2_file_test_directory(char *file);

#endif

