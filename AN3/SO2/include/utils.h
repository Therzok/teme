/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef UTILS_H_
#define UTILS_H_
#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include "parser.h"

/**
 * Concatenate parts of the word to get the command.
 *
 * Return surfaced as char * due to no UNICODE.
 */
char *so2_util_get_word(word_t *s);

/* Buffer sizes. */
#define BUF_SIZE		256
#define CHUNK_SIZE		100

/* Die messages. */
#define ERR_ALLOC		"alloc failure"
#define ERR_ARGV		"argv not parsed"
#define ERR_CLOSE		"cannot close handle"
#define ERR_DIR			"dir null or no home"
#define ERR_ENV			"cannot set env var"
#define ERR_SET_WD		"cannot change work dir"
#define ERR_PIPE		"cannot open pipe"
#define ERR_PIPE_CONN	"cannot connect pipe"
#define ERR_PROC		"cannot create process"
#define ERR_THREAD		"cannot create thread"
#define ERR_THREAD_JOIN		"cannot join thread"
#define ERR_WORD		"word not parsed"

/* Error formats for stderr printing. */
#define ERRFMT_DIR_ENOTDIR	"cd: not a directory: %s\n"
#define ERRFMT_DIR_ENOENT	"cd: no such file or directory: %s\n"
#define ERRFMT_EXEC		"Execution failed for '%s'\n"
#define ERRFMT_PARSE		"parse error near %d: %s\n"

#define SHELL_EXIT		-100

#define UNUSED(x)		(void)(x)

/* Define DIE for per platform usage. */
#ifdef _WIN32
VOID PrintLastError(const PCHAR message);
#define ERR_FUNC		PrintLastError
#else
#define ERR_FUNC		perror
#endif

#define DIE(assertion, call_description)			\
	do {							\
		if (assertion) {				\
			fprintf(stderr, "(%s, %d): ",		\
					__FILE__, __LINE__);	\
			ERR_FUNC(call_description);		\
			exit(EXIT_FAILURE);			\
		}						\
	} while (0)

#endif
