/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "cmd.h"
#include "env.h"
#include "file.h"
#include "pipe.h"
#include "utils.h"

/**
 * Concatenate arguments in a NULL terminated list in order to
 * pass them to execvp.
 *
 * @param command The command that gets executed.
 * @param size The size that will be set to argc.
 *
 * @return The argument list of strings.
 */
static char **util_get_argv(simple_command_t *command, int *size)
{
	void *temp;
	char **argv;
	word_t *param;
	int argc, i;

	argc = 0;
	argv = calloc(argc + 1, sizeof(char *));
	if (!argv)
		return NULL;

	argv[argc] = so2_util_get_word(command->verb);
	if (!argv[argc]) {
		free(argv);
		return NULL;
	}

	++argc;
	for (param = command->params; param; param = param->next_word) {
		temp = realloc(argv, (argc + 1) * sizeof(char *));
		if (!temp) {
			free(argv);
			return NULL;
		}
		argv = temp;

		argv[argc] = so2_util_get_word(param);
		if (!argv[argc]) {
			for (i = 0; i < argc; ++i)
				free(argv[argc]);
			free(argv);

			return NULL;
		}

		++argc;
	}

	temp = realloc(argv, (argc + 1) * sizeof(char *));
	if (!temp) {
		for (i = 0; i < argc; ++i)
			free(argv[argc]);
		free(argv);

		return NULL;
	}

	argv = temp;
	argv[argc] = NULL;
	*size = argc;

	return argv;
}

int so2_cmd_parse_simple(simple_command_t *s)
{
	char *command, **argv;
	pid_t pid;
	int status, count, ret;

	ret = EXIT_SUCCESS;
	command = so2_util_get_word(s->verb);

	pid = fork();
	if (!pid) {
		ret = so2_file_redirect_std(s, NULL);
		if (!ret) {
			argv = util_get_argv(s, &count);
			DIE(!argv, ERR_ARGV);
			execvp(command, argv);
		}
		exit(errno);
	}

	DIE(pid < 0, ERR_PROC);

	waitpid(pid, &status, 0);
	DIE(!WIFEXITED(status), ERR_PROC);

	ret = WEXITSTATUS(status);
	if (ret == ENOENT)
		fprintf(stderr, ERRFMT_EXEC, command);

	free(command);
	return ret;
}

int so2_cmd_parse_parallel(command_t *c1, command_t *c2, int piped)
{
	struct pipe *pip;
	pid_t pid;
	int ret, status;

	pid = fork();

	/* Child */
	if (!pid) {
		if (piped) {
			pip = so2_pipe_open();
			DIE(!pip, ERR_PIPE);
		}
		pid = fork();

		/* Grandchild */
		if (!pid) {
			if (piped) {
				ret = so2_pipe_connect(pip, FILE_OUT, PIPE_OUT);
				DIE(ret, ERR_PIPE_CONN);
				so2_pipe_free(pip);
			}
			exit(so2_cmd_parse(c1));
		}

		DIE(pid < 0, ERR_PROC);

		if (piped) {
			ret = so2_pipe_connect(pip, FILE_IN, PIPE_IN);
			DIE(ret, ERR_PIPE_CONN);
			so2_pipe_free(pip);
		}
		ret = so2_cmd_parse(c2);

		waitpid(pid, &status, 0);
		DIE(!WIFEXITED(status), ERR_PROC);

		exit(ret);
	}

	/* Parent */
	DIE(pid < 0, ERR_PROC);

	waitpid(pid, &status, 0);
	DIE(!WIFEXITED(status), ERR_PROC);

	ret = WEXITSTATUS(status);
	return ret;
}

