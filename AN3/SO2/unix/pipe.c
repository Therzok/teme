/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "pipe.h"
#include "utils.h"

struct pipe {
	struct file *fd_in;
	struct file *fd_out;
};

struct pipe *so2_pipe_open(void)
{
	struct pipe *pip;
	int fds[PIPE_COUNT], ret;

	pip = malloc(sizeof(struct pipe));
	if (!pip)
		goto done;

	pip->fd_in = so2_file_alloc();
	if (!pip->fd_in)
		goto clear_pipe;

	pip->fd_out = so2_file_alloc();
	if (!pip->fd_out)
		goto clear_pipe_in;

	ret = pipe(fds);
	if (ret < 0)
		goto clear_pipe_both;
	else {
		/*
		 * Here be dragons. We cannot expose structure of `struct file`,
		 * so do all the magic in underlying memory accessing.
		 */
		*(int *)pip->fd_in = fds[PIPE_IN];
		*(int *)pip->fd_out = fds[PIPE_OUT];
		goto done;
	}

clear_pipe_both:
	free(pip->fd_out);
clear_pipe_in:
	free(pip->fd_in);
clear_pipe:
	free(pip);
	pip = NULL;

done:
	return pip;
}

void so2_pipe_free(struct pipe *pipe)
{
	free(pipe);
}

/**
 * Closes the given pipe type.
 *
 * @param pipe The pipe
 * @param pipe_open_type PIPE_IN or PIPE_OUT
 * @return 0 on success, non-zero on failure.
 */
static int pipe_close(struct pipe *pipe, int pipe_open_type)
{
	return so2_file_close(pipe_open_type == PIPE_IN ?
			      pipe->fd_in :
			      pipe->fd_out);
}

int so2_pipe_connect(struct pipe *pipe, int file_open_type, int pipe_open_type)
{
	struct file *std;
	int ret;

	/* Close the other pipe. */
	ret = pipe_close(pipe, PIPE_OUT - pipe_open_type);
	if (ret)
		goto done;

	std = so2_file_standard_open(file_open_type);
	if (!std)
		goto done;

	ret = so2_file_redirect(std, pipe_open_type == PIPE_IN ?
				pipe->fd_in :
				pipe->fd_out);
	if (ret)
		goto done;

	so2_file_free(std);

	ret = pipe_close(pipe, pipe_open_type);
	if (ret)
		goto done;

done:
	return ret;
}

