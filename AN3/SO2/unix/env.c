/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include "env.h"
#include "utils.h"

char *so2_env_get(const char *name)
{
	return getenv(name);
}

int so2_env_set(char *name, char *value)
{
	return setenv(name, value, 1);
}

char *so2_env_get_workdir(void)
{
	char *buf, *tmp;
	int size;

	size =  1;/*BUF_SIZE; */
	buf = malloc(size * sizeof(char));

	for (;;) {
		tmp = getcwd(buf, size);
		if (tmp) {
			buf = tmp;
			goto done;
		}

		if (errno == ERANGE) {
			size *= 2;
			tmp = realloc(buf, size * sizeof(char));
			if (!tmp)
				break;

			buf = tmp;
			continue;
		}
		break;
	}

	free(buf);
	buf = NULL;

done:
	return buf;
}

int so2_env_set_workdir(const char *workdir)
{
	return chdir(workdir);
}

