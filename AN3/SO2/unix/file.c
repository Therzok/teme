/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Marius Ungureanu <therzok@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "file.h"
#include "utils.h"

struct file {
	int fd;
};

static int file_flags(simple_command_t *s, int open_type)
{
	int flags;

	switch (open_type) {
	case FILE_IN:
		flags = O_RDONLY;
		break;

	case FILE_OUT:
		flags = O_WRONLY | O_CREAT;
		flags |= (s->io_flags & IO_OUT_APPEND) ? O_APPEND : O_TRUNC;
		break;

	case FILE_ERR:
		flags = O_WRONLY | O_CREAT;
		flags |= (s->io_flags & IO_ERR_APPEND) ? O_APPEND : O_TRUNC;
		break;

	default:
		return 0;
	}

	return flags;
}

static char *file_name(simple_command_t *s, int open_type)
{
	switch (open_type) {
	case FILE_IN:	return so2_util_get_word(s->in);
	case FILE_OUT:	return so2_util_get_word(s->out);
	case FILE_ERR:	return so2_util_get_word(s->err);
	default:	return NULL;
	};
}

struct file *so2_file_standard_open(int open_type)
{
	struct file *file;
	int fd;

	file = so2_file_alloc();
	if (!file)
		goto done;

	switch (open_type) {
	case FILE_IN:
		fd = STDIN_FILENO;
		break;

	case FILE_OUT:
		fd = STDOUT_FILENO;
		break;

	case FILE_ERR:
		fd = STDERR_FILENO;
		break;
	}

	file->fd = fd;

done:
	return file;
}

struct file *so2_file_open(simple_command_t *s, int open_type)
{
	struct file *file;
	char *filename;
	int flags, fd;

	file = NULL;

	if (open_type >= FILE_COUNT)
		return NULL;

	flags = file_flags(s, open_type);
	filename = file_name(s, open_type);
	if (!filename)
		return NULL;

	fd = open(filename, flags, 0644);
	if (fd < 0)
		goto clean_filename;

	file = so2_file_alloc();
	if (!file)
		goto clean_filename;

	file->fd = fd;

clean_filename:
	free(filename);
	return file;
}

struct file *so2_file_alloc()
{
	return malloc(sizeof(struct file));
}

void so2_file_free(struct file *file)
{
	free(file);
}

int so2_file_close(struct file *file)
{
	int fd;

	if (!file)
		return 0;

	fd = file->fd;
	so2_file_free(file);

	return close(fd);
}

int so2_file_redirect(struct file *from, struct file *to)
{
	return dup2(to->fd, from->fd) < 0;
}

static int redirect_fd(struct file *redir, int stdfd)
{
	struct file *std;
	int ret;

	if (redir) {
		std = so2_file_standard_open(stdfd);
		ret = so2_file_redirect(std, redir);
		if (ret)
			goto done;

		so2_file_free(std);
	}

	ret = 0;

done:
	return ret;
}

int so2_file_redirect_std(simple_command_t *s, void *userdata)
{
	struct file *redir;
	char *fileout, *fileerr;
	int ret, same;

	UNUSED(userdata);

	redir = so2_file_open(s, FILE_IN);
	ret = redirect_fd(redir, STDIN_FILENO);
	if (ret)
		goto done;

	ret = so2_file_close(redir);
	if (ret)
		goto done;

	fileout = so2_util_get_word(s->out);
	fileerr = so2_util_get_word(s->err);
	same = fileout && fileerr && !strcmp(fileout, fileerr);
	free(fileout);
	free(fileerr);

	redir = so2_file_open(s, FILE_OUT);
	ret = redirect_fd(redir, FILE_OUT);
	if (ret)
		goto done;

	if (!same) {
		ret = so2_file_close(redir);
		if (ret)
			goto done;

		redir = so2_file_open(s, FILE_ERR);
	}

	ret = redirect_fd(redir, FILE_ERR);
	if (ret)
		goto done;

	ret = so2_file_close(redir);

done:
	return ret;
}

int so2_file_test_directory(char *file)
{
	DIR *dir;

	dir = opendir(file);
	if (!dir) {
		if (errno == ENOTDIR)
			fprintf(stderr, ERRFMT_DIR_ENOTDIR, file);
		else if (errno == ENOENT)
			fprintf(stderr, ERRFMT_DIR_ENOENT, file);

		return -1;
	}

	free(dir);
	return 0;
}

