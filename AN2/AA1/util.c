//
//  util.c
//  TemaAA
//
//  Created by Marius Ungureanu on 17/11/13.
//  Copyright (c) 2013 Marius Ungureanu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "util.h"

const char *util_counter_name[OP_TOTAL] =
{
    "aritmetice",
    "de comparatie",
    "de atribuire",
    "de indexare",
    "de selectie a elementului",
    "de dereferentiere",
    "de apelare",
    "malloc",
    "sizeof"
};

const char *util_algo_name[ALG_TOTAL] =
{
    "bubble sort",
    "shell sort",
    "quick sort",
    "insertion sort",
/*
    "smooth sort",
 */
    "strand sort",
    "cocktail sort"
};

const char *util_gen_name[GEN_TOTAL] =
{
    "generat crescator",
    "generat descrescator",
    "generat aleator",
    "sortat"
};

/*
 * Tabelul de operatii elementare.
 */
int util_op_table[ALG_TOTAL][OP_TOTAL];

int util_inc_op(int alg, int op)
{
    ++util_op_table[alg][op];
    return 1;
}

void util_flush_op_table()
{
    int i;
    int j;

    for (i = 0; i < ALG_TOTAL; ++i)
        for (j = 0; j < OP_TOTAL; ++j)
            util_op_table[i][j] = 0;
}

/*
 * Comparator crescator pentru qsort
 */
int sort_inc(const void *a, const void *b)
{
    return (*(int *)a - *(int *)b);
}

/*
 * Comparator descrescator pentru qsort
 */
int sort_dec(const void *a, const void *b)
{
    return (*(int *)b - *(int *)a);
}

/*
 * Aloca memoria pentru un vector sample de teste.
 * sz: dimensiunea vectorului
 */
int *util_alloc_sample(int sz)
{
    int *vec;

    vec = malloc(sz * sizeof(int));
    return vec;
}

int *util_generate_sample(int sz, int lim, int gen_type)
{
    int i;
    int *vec;

    vec = util_alloc_sample(sz);
    for (i = 0; i < sz; ++i)
    {
        vec[i] = rand();
        if (lim)
            vec[i] = vec[i] % 100 + 1;
        else
            if (i % 2)
                vec[i] = -vec[i];
    }

    // Folosim qsort-ul din C pentru a sorta vectorii randomizati.
    if (gen_type == GEN_INCREASING)
        qsort(vec, sz, sizeof(int), sort_inc);
    else if (gen_type == GEN_DECREASING)
        qsort(vec, sz, sizeof(int), sort_dec);

    return vec;
}

int *util_copy_from_sample(int sz, int *sample)
{
    int i;
    int *vec;

    vec = util_alloc_sample(sz);
    for (i = 0; i < sz; ++i)
        vec[i] = sample[i];

    return vec;
}

/*
 * Printeaza numarul de pasi efectuati.
 */
void util_print_result(int alg)
{
    int i;
    int total;

    if (alg == ALG_TOTAL)
    {
        printf("\n");
        return;
    }

    total = 0;

    printf("S-au efectuat:\n");
    for (i = 0; i < OP_TOTAL; ++i)
    {
        if (util_op_table[alg][i] == 0)
            continue;

        total += util_op_table[alg][i];
        
        printf(" * %d operatii %s\n",
               util_op_table[alg][i], util_counter_name[i]);
    }
    printf(" * %d operatii in total\n", total);
    printf("\n\n");
}

void util_print_sample(int sz, int *vec, int alg, int gen_type, int lim)
{
    int i;

    if (alg == ALG_TOTAL)
        printf("Vectorul %s cu %d elemente:\n", util_gen_name[gen_type], sz);
    else
        printf("Vectorul %s in urma algoritmului %s:\n",
               util_gen_name[GEN_SORT_DONE], util_algo_name[alg]);

    if (lim)
    {
        printf("[");
        for (i = 0; i < sz-1; ++i)
            printf("%d ", vec[i]);
        printf("%d]\n", vec[i]);
    }
    util_print_result(alg);
}
