//
//  test.c
//  TemaAA
//
//  Created by Marius Ungureanu on 17/11/13.
//  Copyright (c) 2013 Marius Ungureanu. All rights reserved.
//

#include <stdlib.h>
#include <time.h>
#include "test.h"
#include "util.h"

/*
 * Initializeaza environmentul de test.
 */
void test_init()
{
    srand((unsigned)time(NULL));
}

/*
 * Ruleaza o testare pentru un algoritm.
 * alg: tipul de algoritm - util_algo_type
 * sample: vectorul de test
 * sz: dimensiunea de test
 */
void test_do(int alg, int sz, int *sample)
{
    (*util_algo_func[alg])(sz, sample);
}

void test_run(int sample_size, int lim, int gen_type)
{
    int i;
    int *copy_samples[sample_size];
    int *sample;

    test_init();
    util_flush_op_table();

    sample = util_generate_sample(sample_size, lim, gen_type);
    util_print_sample(sample_size, sample, ALG_TOTAL, gen_type, lim);

    /* Copiere si rulare test */
    for (i = 0; i < ALG_TOTAL; ++i)
    {
        copy_samples[i] = util_copy_from_sample(sample_size, sample);
        test_do(i, sample_size, copy_samples[i]);
    }

    /* Printare simultana */
    for (i = 0; i < ALG_TOTAL; ++i)
        util_print_sample(sample_size, copy_samples[i], i, GEN_SORT_DONE, lim);

    /* Deinitializare */
    for (i = 0; i < ALG_TOTAL; ++i)
        free(copy_samples[i]);
    free(sample);
}
