//
//  main.c
//  Tema 1 AA
//
//  Created by Marius Ungureanu on 16/11/13.
//  Copyright (c) 2013 Marius Ungureanu. All rights reserved.
//

#include <stdio.h>
#include "test.h"
#include "util.h"

int main(int argc, const char *argv[])
{
    // Verificam daca avem parametri in linie de comanda.s
    char test = argc == 2 ? argv[1][0] : '0';

    while (test != '1' && test != '2')
    {
        printf("Alegeti modulul:\n");
        printf(" 1 - Modul de testare\n");
        printf(" 2 - Modul de analiza\n");
        test = getchar();
        getchar(); // flush \n din buffer
    }

    if (test == '1')
        test_run(10, 1, GEN_RANDOM);
    else
    {
        test_run(10, 0, GEN_INCREASING);
        test_run(300, 0,GEN_INCREASING);
        test_run(10000, 0, GEN_INCREASING);

        test_run(10, 0, GEN_RANDOM);
        test_run(300, 0, GEN_RANDOM);
        test_run(10000, 0, GEN_RANDOM);

        test_run(10, 0, GEN_DECREASING);
        test_run(300, 0, GEN_DECREASING);
        test_run(10000, 0, GEN_DECREASING);
    }
    return 0;
}

