//
//  util.h
//  TemaAA
//
//  Created by Marius Ungureanu on 17/11/13.
//  Copyright (c) 2013 Marius Ungureanu. All rights reserved.
//

#ifndef TemaAA_util_h
#define TemaAA_util_h

/*
 * Lista cu tipurile de operatii elementare
 */
enum util_operation
{
    OP_ARITHMETIC = 0,
    OP_COMPARISON,
    OP_ASSIGNMENT,
    OP_INDEX,
    OP_SELECT,
    OP_POINT,
    OP_FUNC_CALL,
    OP_MEMORY,
    OP_SIZEOF,
    OP_TOTAL
};

/*
 * Lista cu algoritmii.
 */
enum util_algo_type
{
    ALG_BUBBLE = 0,
    ALG_SHELL,
    ALG_QUICK,
    ALG_INSERTION,
/*
    ALG_SMOOTH,
 */
    ALG_STRAND,
    ALG_COCKTAIL,
    ALG_TOTAL
};

/*
 * Lista cu tipul de generare
 */
enum util_gen_type
{
    GEN_INCREASING,
    GEN_DECREASING,
    GEN_RANDOM,
    GEN_SORT_DONE,
    GEN_TOTAL
};

/*
 * Textul pentru fiecare operatie.
 * Definit ca extern ca sa nu avem probleme cu linkerul.
 */
extern const char *util_counter_name[OP_TOTAL];

/*
 * Textul pentru fiecare algoritm.
 * Definit ca extern ca sa nu avem probleme cu linkerul.
 */
extern const char *util_algo_name[ALG_TOTAL];

/*
 * Functiile pentru fiecare algoritm
 */
extern void (*util_algo_func[ALG_TOTAL])(int sz, int *vec);

/*
 * Textul pentru fiecare generare.
 * Definit ca extern ca sa nu avem probleme cu linkerul.
 */
extern const char *util_gen_name[GEN_TOTAL];

/*
 * Incrementeaza numarul de operatii.
 * alg: tipul de algoritm - util_algo_type
 * op: tipul de operatie - util_operation
 * Return: int - Code hack pentru a putea integra incrementarea intr-o conditie,
 *               fara a mai scrie cod extra pentru bucle.
 */
int util_inc_op(int alg, int op);

/*
 * Reseteaza contoarele de operatii
 */
void util_flush_op_table();

/*
 * Genereaza un vector sample pentru teste.
 * Return: vectorul cu elemente randomizate.
 * sz: dimensiunea vectorului
 * lim: daca e limitat: numerele apartin [1,100]
 * gen_type: tipul generat - util_gen_type
 */
int *util_generate_sample(int sz, int lim, int gen_type);

/*
 * Se face o copie a vectorului sample in alt vector.
 * sz: dimensiunea vectorului
 * sample: vectorul sursa
 */
int *util_copy_from_sample(int sz, int *sample);

/*
 * Printeaza vectorul sample.
 * sz: dimensiunea vectorului
 * vec: vectorul sample
 * alg: algoritmul folosit. Pentru ALG_TOTAL, e generat.
 * gen_type: tipul generat - util_gen_type
 * lim: daca e limitat: nu printeaza vectorii
 */
void util_print_sample(int sz, int *vec, int alg, int gen_type, int lim);


/*
 * Nod simplu de lista inlantuita
 */
struct ll_node
{
    struct ll_node *next;
    int val;
};

#endif
