//
//  alg.h
//  TemaAA
//
//  Created by Marius Ungureanu on 17/11/13.
//  Copyright (c) 2013 Marius Ungureanu. All rights reserved.
//

#ifndef TemaAA_alg_h
#define TemaAA_alg_h

void alg_bubble_sort(int sz, int *vec);
void alg_shell_sort(int sz, int *vec);
void alg_quick_sort(int sz, int *vec);
void alg_insertion_sort(int sz, int *vec);
void alg_smooth_sort(int sz, int *vec);
void alg_strand_sort(int sz, int *vec);
void alg_cocktail_sort(int sz, int *vec);

#endif
