//
//  alg.c
//  TemaAA
//
//  Created by Marius Ungureanu on 17/11/13.
//  Copyright (c) 2013 Marius Ungureanu. All rights reserved.
//

#include <stdlib.h>
#include "util.h"
#include "alg.h"

void (*util_algo_func[ALG_TOTAL])(int sz, int *vec) =
{
    alg_bubble_sort,
    alg_shell_sort,
    alg_quick_sort,
    alg_insertion_sort,
/*
    alg_smooth_sort,
 */
    alg_strand_sort,
    alg_cocktail_sort
};

void alg_bubble_sort(int sz, int *vec)
{
    int n_sz;
    int i;
    int swap;

    util_inc_op(ALG_BUBBLE, OP_ASSIGNMENT);
    n_sz = 1;

    while (util_inc_op(ALG_BUBBLE, OP_COMPARISON) && n_sz != 0)
    {
        util_inc_op(ALG_BUBBLE, OP_ASSIGNMENT);
        n_sz = 0;

              // i = 1
        for (util_inc_op(ALG_BUBBLE, OP_ASSIGNMENT), i = 1;
             util_inc_op(ALG_BUBBLE, OP_COMPARISON) && i < sz;
             util_inc_op(ALG_BUBBLE, OP_ARITHMETIC), // i + 1
             util_inc_op(ALG_BUBBLE, OP_ASSIGNMENT), // i =
             ++i)
        {
            util_inc_op(ALG_BUBBLE, OP_ARITHMETIC);  // i - 1
            util_inc_op(ALG_BUBBLE, OP_INDEX);       // vec[i-1]
            util_inc_op(ALG_BUBBLE, OP_INDEX);       // vec[i]
            util_inc_op(ALG_BUBBLE, OP_COMPARISON);  // vec[i-1] > vec[i]
            if (vec[i-1] > vec[i])
            {
                util_inc_op(ALG_BUBBLE, OP_ARITHMETIC);  // i - 1
                util_inc_op(ALG_BUBBLE, OP_INDEX);       // vec[i-1]
                util_inc_op(ALG_BUBBLE, OP_ASSIGNMENT);  // swap =
                swap = vec[i-1];

                util_inc_op(ALG_BUBBLE, OP_ARITHMETIC);  // i - 1
                util_inc_op(ALG_BUBBLE, OP_INDEX);       // vec[i-1]
                util_inc_op(ALG_BUBBLE, OP_INDEX);       // vec[i]
                util_inc_op(ALG_BUBBLE, OP_ASSIGNMENT);  // vec[i-1] =
                vec[i-1] = vec[i];

                util_inc_op(ALG_BUBBLE, OP_INDEX);       // vec[i]
                util_inc_op(ALG_BUBBLE, OP_ASSIGNMENT);  // vec[i] =
                vec[i] = swap;

                util_inc_op(ALG_BUBBLE, OP_ASSIGNMENT);  // n_sz =
                n_sz = i;
            }
        }
    }
}

void alg_shell_sort(int sz, int *vec)
{
    int i;
    int j;
    int swap;
    int gap;

    // TODO: Use 2^k + 1
    for (util_inc_op(ALG_BUBBLE, OP_ASSIGNMENT), gap = sz;
         util_inc_op(ALG_SHELL, OP_COMPARISON) && gap > 0;
         util_inc_op(ALG_SHELL, OP_ARITHMETIC), // g / 2
         util_inc_op(ALG_SHELL, OP_ASSIGNMENT), // g =
         gap /= 2)
    {
        ;  // i = gap
        for (util_inc_op(ALG_SHELL, OP_ASSIGNMENT), i = gap;
             util_inc_op(ALG_SHELL, OP_COMPARISON) && i < sz;
             util_inc_op(ALG_SHELL, OP_ARITHMETIC), // i + 1
             util_inc_op(ALG_SHELL, OP_ASSIGNMENT), // i =
             ++i)
        {
            util_inc_op(ALG_SHELL, OP_INDEX);        // vec[i]
            util_inc_op(ALG_SHELL, OP_ASSIGNMENT);   // swap =
            swap = vec[i];

            ;   // j = i
            for (util_inc_op(ALG_SHELL, OP_ASSIGNMENT), j = i;
                 j >= gap && vec[j-gap] > swap;
                 util_inc_op(ALG_SHELL, OP_ARITHMETIC), // j - gap
                 util_inc_op(ALG_SHELL, OP_ASSIGNMENT),      // j =
                 j -= gap)
            {
                util_inc_op(ALG_SHELL, OP_COMPARISON);   // j >= gap
                util_inc_op(ALG_SHELL, OP_ARITHMETIC);   // j - gap
                util_inc_op(ALG_SHELL, OP_INDEX);        // vec[j - gap]
                util_inc_op(ALG_SHELL, OP_COMPARISON);   // vec[j - gap] > swap

                util_inc_op(ALG_SHELL, OP_ARITHMETIC);   // j - gap
                util_inc_op(ALG_SHELL, OP_INDEX);        // vec[j - gap]
                util_inc_op(ALG_SHELL, OP_INDEX);        // vec[j]
                util_inc_op(ALG_SHELL, OP_ASSIGNMENT);   // vec[j] =
                vec[j] = vec[j-gap];
            }

            // Inca o verificare de conditie pentru j >= gap && vec[j - gap] > swap
            util_inc_op(ALG_SHELL, OP_COMPARISON);   // j >= gap

            // Daca a evaluat prima expresie cu adevarat,
            // iar cealalta a esuat.
            if (j >= gap)
            {
                util_inc_op(ALG_SHELL, OP_ARITHMETIC);   // j - gap
                util_inc_op(ALG_SHELL, OP_INDEX);        // vec[j - gap]
                util_inc_op(ALG_SHELL, OP_COMPARISON);   // vec[j - gap] > swap
            }

            util_inc_op(ALG_SHELL, OP_INDEX);        // vec[j]
            util_inc_op(ALG_SHELL, OP_ASSIGNMENT);   // vec[j] =
            vec[j] = swap;
        }
    }
}

void do_quick_sort(int l, int r, int *vec)
{
    int i;
    int j;
    int pivot;
    int swap;

    util_inc_op(ALG_QUICK, OP_COMPARISON);
    if (l < r)
    {
        // TODO: Find better choosing?
        // Alegem indexul pivotului.
        util_inc_op(ALG_QUICK, OP_ARITHMETIC);  // l+r
        util_inc_op(ALG_QUICK, OP_ARITHMETIC);  // (l+r)/2
        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // pivot =
        pivot = (l+r)/2;

        util_inc_op(ALG_QUICK, OP_INDEX);       // vec[l]
        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // swap =
        swap = vec[l];

        util_inc_op(ALG_QUICK, OP_INDEX);       // vec[l]
        util_inc_op(ALG_QUICK, OP_INDEX);       // vec[pivot]
        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // vec[l] =
        vec[l] = vec[pivot];

        util_inc_op(ALG_QUICK, OP_INDEX);       // vec[pivot]
        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // vec[pivot] =
        vec[pivot] = swap;

        // Alegem valoarea pivotului si partitionam.
        util_inc_op(ALG_QUICK, OP_INDEX);       // vec[l]
        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // pivot =
        pivot = vec[l];

        util_inc_op(ALG_QUICK, OP_ARITHMETIC);  // l + 1
        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // i =
        i = l + 1;

        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // j =
        j = r;

        while (util_inc_op(ALG_QUICK, OP_COMPARISON) && i <= j)
        {
            while (util_inc_op(ALG_QUICK, OP_COMPARISON) &&
                   i <= r &&
                   util_inc_op(ALG_QUICK, OP_INDEX) &&      // vec[i]
                   util_inc_op(ALG_QUICK, OP_COMPARISON) && // vec[i] <=
                   vec[i] <= pivot)
            {
                util_inc_op(ALG_QUICK, OP_ARITHMETIC); // i + 1
                util_inc_op(ALG_QUICK, OP_ASSIGNMENT); // i =
                ++i;
            }
            while (util_inc_op(ALG_QUICK, OP_COMPARISON) &&
                   j >= l &&
                   util_inc_op(ALG_QUICK, OP_INDEX) &&      // vec[j]
                   util_inc_op(ALG_QUICK, OP_COMPARISON) && // vec[j] >
                   vec[j] > pivot)
            {
                util_inc_op(ALG_QUICK, OP_ARITHMETIC); // j - 1
                util_inc_op(ALG_QUICK, OP_ASSIGNMENT); // j =
                j--;
            }

            util_inc_op(ALG_QUICK, OP_COMPARISON); // i < j
            if (i < j)
            {
                util_inc_op(ALG_QUICK, OP_INDEX);       // vec[i]
                util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // swap =
                swap = vec[i];

                util_inc_op(ALG_QUICK, OP_INDEX);       // vec[i]
                util_inc_op(ALG_QUICK, OP_INDEX);       // vec[j]
                util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // vec[i] =
                vec[i] = vec[j];

                util_inc_op(ALG_QUICK, OP_INDEX);       // vec[j]
                util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // vec[j] =
                vec[j] = swap;
            }
        }

        util_inc_op(ALG_QUICK, OP_INDEX);       // vec[l]
        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // swap =
        swap = vec[l];

        util_inc_op(ALG_QUICK, OP_INDEX);       // vec[l]
        util_inc_op(ALG_QUICK, OP_INDEX);       // vec[j]
        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // vec[l] =
        vec[l] = vec[j];

        util_inc_op(ALG_QUICK, OP_INDEX);       // vec[j]
        util_inc_op(ALG_QUICK, OP_ASSIGNMENT);  // vec[j] =
        vec[j] = swap;

        util_inc_op(ALG_QUICK, OP_FUNC_CALL);
        do_quick_sort(l, j-1, vec);
        util_inc_op(ALG_QUICK, OP_FUNC_CALL);
        do_quick_sort(j+1, r, vec);
    }
}

void alg_quick_sort(int sz, int *vec)
{
    do_quick_sort(0, sz-1, vec);
}

void alg_insertion_sort(int sz, int *vec)
{
    int i;
    int hole;
    int val;

    for (util_inc_op(ALG_INSERTION, OP_ASSIGNMENT), i = 1;
         util_inc_op(ALG_INSERTION, OP_COMPARISON) && i < sz;
         util_inc_op(ALG_INSERTION, OP_ARITHMETIC), // i + 1
         util_inc_op(ALG_INSERTION, OP_ASSIGNMENT), // i =
         ++i)
    {
        util_inc_op(ALG_INSERTION, OP_INDEX);       // vec[i];
        util_inc_op(ALG_INSERTION, OP_ASSIGNMENT);  // val =
        val = vec[i];

        util_inc_op(ALG_INSERTION, OP_ARITHMETIC);  // i - 1
        util_inc_op(ALG_INSERTION, OP_ASSIGNMENT);  // hole =
        hole = i - 1;

        while (util_inc_op(ALG_INSERTION, OP_COMPARISON) && hole >= 0 &&
               util_inc_op(ALG_INSERTION, OP_INDEX) &&      // vec[hole]
               util_inc_op(ALG_INSERTION, OP_COMPARISON) && // val <
               val < vec[hole])
        {
            util_inc_op(ALG_INSERTION, OP_INDEX);       // vec[hole]
            util_inc_op(ALG_INSERTION, OP_ARITHMETIC);  // hole + 1
            util_inc_op(ALG_INSERTION, OP_INDEX);       // vec[hole+1]
            util_inc_op(ALG_INSERTION, OP_ASSIGNMENT);  // vec[hole+1] =
            vec[hole+1] = vec[hole];

            util_inc_op(ALG_INSERTION, OP_ARITHMETIC);  // hole - 1
            util_inc_op(ALG_INSERTION, OP_ASSIGNMENT);  // hole =
            --hole;
        }

        util_inc_op(ALG_INSERTION, OP_ARITHMETIC);  // hole + 1
        util_inc_op(ALG_INSERTION, OP_INDEX);       // vec[hole+1]
        util_inc_op(ALG_INSERTION, OP_ASSIGNMENT);  // vec[hole+1] =
        vec[hole+1] = val;
    }
}

/* Generate deja la: http://oeis.org/A001595/b001595.txt */
const unsigned kLeonardoNumbers[] = {
    1u, 1u, 3u, 5u, 9u, 15u, 25u, 41u, 67u, 109u, 177u, 287u, 465u, 753u,
    1219u, 1973u, 3193u, 5167u, 8361u, 13529u, 21891u, 35421u, 57313u, 92735u,
    150049u, 242785u, 392835u, 635621u, 1028457u, 1664079u, 2692537u,
    4356617u, 7049155u, 11405773u, 18454929u, 29860703u, 48315633u, 78176337u,
    126491971u, 204668309u, 331160281u, 535828591u, 866988873u, 1402817465u,
    2269806339u, 3672623805u
};

void alg_smooth_sort(int sz, int *vec)
{
}

void ll_free(struct ll_node *list)
{
    struct ll_node *iter;

    while (util_inc_op(ALG_STRAND, OP_COMPARISON) && list)
    {
        util_inc_op(ALG_STRAND, OP_POINT);      // *list
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*list).next
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter =
        iter = list->next;

        util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // free
        util_inc_op(ALG_STRAND, OP_MEMORY);     // free
        free(list);

        util_inc_op(ALG_STRAND, OP_ASSIGNMENT);
        list = iter;
    }
}

void ll_free_all(struct ll_node *list)
{
    util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // ll_free
    util_inc_op(ALG_STRAND, OP_POINT);      // *list
    util_inc_op(ALG_STRAND, OP_SELECT);     // (*list).next
    ll_free(list->next);

    util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // free
    util_inc_op(ALG_STRAND, OP_MEMORY);     // free
    free(list);
}

struct ll_node *ll_merge(struct ll_node *a, struct ll_node *b)
{
    util_inc_op(ALG_STRAND, OP_COMPARISON); // !a
    if (!a)
        return b;

    util_inc_op(ALG_STRAND, OP_COMPARISON); // !b
    if (!b)
        return a;

    if (util_inc_op(ALG_STRAND, OP_POINT) &&        // *a
        util_inc_op(ALG_STRAND, OP_SELECT) &&       // (*a).val
        util_inc_op(ALG_STRAND, OP_POINT) &&        // *b
        util_inc_op(ALG_STRAND, OP_SELECT) &&       // (*b).val
        util_inc_op(ALG_STRAND, OP_COMPARISON) &&   // a->val < b->val
        a->val < b->val)
    {
        util_inc_op(ALG_STRAND, OP_POINT);      // *a
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*a).next
        util_inc_op(ALG_STRAND, OP_POINT);      // *a
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*a).next
        util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // ll_merge
        a->next = ll_merge(a->next, b);
        return a;
    }

    util_inc_op(ALG_STRAND, OP_POINT);      // *b
    util_inc_op(ALG_STRAND, OP_SELECT);     // (*b).next
    util_inc_op(ALG_STRAND, OP_POINT);      // *b
    util_inc_op(ALG_STRAND, OP_SELECT);     // (*b).next
    util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // ll_merge
    b->next = ll_merge(b->next, a);
    return b;
}

void alg_strand_sort(int sz, int *vec)
{
    struct ll_node *unsorted;
    struct ll_node *sublist;
    struct ll_node *sorted;
    struct ll_node *iter_u;
    struct ll_node *iter_s;
    int n_sz;

    // Santinele
    util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // malloc
    util_inc_op(ALG_STRAND, OP_MEMORY);     // malloc
    util_inc_op(ALG_STRAND, OP_SIZEOF);
    util_inc_op(ALG_STRAND, OP_ASSIGNMENT);
    unsorted = malloc(sizeof(struct ll_node));

    util_inc_op(ALG_STRAND, OP_POINT);      // *unsorted
    util_inc_op(ALG_STRAND, OP_SELECT);     // (*unsorted).next
    util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // unsorted->next = NULL
    unsorted->next = NULL;


    util_inc_op(ALG_STRAND, OP_FUNC_CALL);
    util_inc_op(ALG_STRAND, OP_MEMORY);
    util_inc_op(ALG_STRAND, OP_SIZEOF);
    util_inc_op(ALG_STRAND, OP_ASSIGNMENT);
    sublist = malloc(sizeof(struct ll_node));

    util_inc_op(ALG_STRAND, OP_POINT);      // *sublist
    util_inc_op(ALG_STRAND, OP_SELECT);     // (*sublist).next
    util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // sublist->next = NULL
    sublist->next = NULL;

    util_inc_op(ALG_STRAND, OP_FUNC_CALL);
    util_inc_op(ALG_STRAND, OP_MEMORY);
    util_inc_op(ALG_STRAND, OP_SIZEOF);
    util_inc_op(ALG_STRAND, OP_ASSIGNMENT);
    sorted = malloc(sizeof(struct ll_node));

    util_inc_op(ALG_STRAND, OP_POINT);      // *sorted
    util_inc_op(ALG_STRAND, OP_SELECT);     // (*sorted).next
    util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // sorted->next = NULL
    sorted->next = NULL;

    util_inc_op(ALG_STRAND, OP_ASSIGNMENT);
    iter_u = unsorted;

    for (util_inc_op(ALG_STRAND, OP_ASSIGNMENT), n_sz = 0;
         util_inc_op(ALG_STRAND, OP_COMPARISON) && n_sz < sz;
         util_inc_op(ALG_STRAND, OP_ARITHMETIC),    // n_sz + 1
         util_inc_op(ALG_STRAND, OP_ASSIGNMENT),    // n_sz =
         ++n_sz)
    {
        util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // malloc
        util_inc_op(ALG_STRAND, OP_MEMORY);     // malloc
        util_inc_op(ALG_STRAND, OP_SIZEOF);
        util_inc_op(ALG_STRAND, OP_POINT);      // *iter_u
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_u).next
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_u->next =
        iter_u->next = malloc(sizeof(struct ll_node));

        util_inc_op(ALG_STRAND, OP_POINT);      // *iter_u
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_u).next
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_u =
        iter_u = iter_u->next;

        util_inc_op(ALG_STRAND, OP_POINT);      // *iter_u
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_u).val
        util_inc_op(ALG_STRAND, OP_INDEX);      // vec[n_sz]
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_u->val =
        iter_u->val = vec[n_sz];
    }

    util_inc_op(ALG_STRAND, OP_POINT);      // *iter_u
    util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_u).next
    util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_u =
    iter_u->next = NULL;

    // unsorted nu e gol.
    while (util_inc_op(ALG_STRAND, OP_POINT) &&     // *unsorted
           util_inc_op(ALG_STRAND, OP_SELECT) &&    // (*unsorted).next
           unsorted->next)
    {
        util_inc_op(ALG_STRAND, OP_POINT);      // *sublist
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*sublist).next
        util_inc_op(ALG_STRAND, OP_POINT);      // *unsorted
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*unsorted).next
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // sublist->next =
        sublist->next = unsorted->next;

        util_inc_op(ALG_STRAND, OP_POINT);      // *unsorted
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*unsorted).next
        util_inc_op(ALG_STRAND, OP_POINT);      // *unsorted
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*unsorted).next
        util_inc_op(ALG_STRAND, OP_POINT);      // *((*unsorted).next)
        util_inc_op(ALG_STRAND, OP_SELECT);     // *((*unsorted).next).next
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // sublist->next =
        unsorted->next = unsorted->next->next;

        util_inc_op(ALG_STRAND, OP_POINT);      // *sublist
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*sublist).next
        util_inc_op(ALG_STRAND, OP_POINT);      // *((*sublist).next)
        util_inc_op(ALG_STRAND, OP_SELECT);     // *((*sublist).next).next
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // sublist->next->next =
        sublist->next->next = NULL;

        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_s =
        util_inc_op(ALG_STRAND, OP_POINT);      // *sublist
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*sublist).next
        iter_s = sublist->next;

        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_u =
        iter_u = unsorted;

        while (util_inc_op(ALG_STRAND, OP_COMPARISON) &&    // iter_u
               iter_u &&
               util_inc_op(ALG_STRAND, OP_POINT) &&         // *iter_u
               util_inc_op(ALG_STRAND, OP_SELECT) &&        // (*iter_u).next
               util_inc_op(ALG_STRAND, OP_COMPARISON) &&    // iter_u->next
               iter_u->next)
        {
            util_inc_op(ALG_STRAND, OP_POINT);      // *iter_u
            util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_u).next
            util_inc_op(ALG_STRAND, OP_POINT);      // *((*iter_u).next)
            util_inc_op(ALG_STRAND, OP_SELECT);     // (*((*iter_u).next)).val
            util_inc_op(ALG_STRAND, OP_POINT);      // *iter_s
            util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_s).val
            util_inc_op(ALG_STRAND, OP_COMPARISON); // iter_u->next->val >=

            if (iter_u->next->val >= iter_s->val)
            {
                util_inc_op(ALG_STRAND, OP_POINT);      // *iter_s
                util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_s).next
                util_inc_op(ALG_STRAND, OP_POINT);      // *iter_u
                util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_u).next
                util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_s->next =
                iter_s->next = iter_u->next;

                util_inc_op(ALG_STRAND, OP_POINT);      // *iter_u
                util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_u).next
                util_inc_op(ALG_STRAND, OP_POINT);      // *iter_u
                util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_u).next
                util_inc_op(ALG_STRAND, OP_POINT);      // *((*iter_u).next)
                util_inc_op(ALG_STRAND, OP_SELECT);     // (*((*iter_u).next)).next
                util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_s->next =
                iter_u->next = iter_u->next->next;

                util_inc_op(ALG_STRAND, OP_POINT);      // *iter_s
                util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_s).next
                util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_s =
                iter_s = iter_s->next;

                util_inc_op(ALG_STRAND, OP_POINT);      // *iter_s
                util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_s).next
                util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_s->next =
                iter_s->next = NULL;
            }
            else
            {
                util_inc_op(ALG_STRAND, OP_POINT);      // *iter_u
                util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_u).next
                util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_u =
                iter_u = iter_u->next;
            }
        }

        util_inc_op(ALG_STRAND, OP_POINT);      // *sorted
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*isorted).next
        util_inc_op(ALG_STRAND, OP_POINT);      // *sublist
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*sublist).next
        util_inc_op(ALG_STRAND, OP_POINT);      // *sorted
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*sorted).next
        util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // ll_merge
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // sorted->next =

        sorted->next = ll_merge(sublist->next, sorted->next);
    }

    util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // n_sz =
    n_sz = 0;

    util_inc_op(ALG_STRAND, OP_POINT);      // *sorted
    util_inc_op(ALG_STRAND, OP_SELECT);     // (*sorted).next
    util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_s =
    iter_s = sorted->next;
    while (util_inc_op(ALG_STRAND, OP_COMPARISON) && iter_s)
    {
        util_inc_op(ALG_STRAND, OP_POINT);      // *iter_s
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_s).val
        util_inc_op(ALG_STRAND, OP_INDEX);      // vec[n_sz]
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // vec[n_sz] =
        vec[n_sz] = iter_s->val;

        util_inc_op(ALG_STRAND, OP_POINT);      // *iter_s
        util_inc_op(ALG_STRAND, OP_SELECT);     // (*iter_s).next
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // iter_s =
        iter_s = iter_s->next;

        util_inc_op(ALG_STRAND, OP_ARITHMETIC); // n_sz+1
        util_inc_op(ALG_STRAND, OP_ASSIGNMENT); // n_sz =
        ++n_sz;
    }

    // Free memory.

    util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // free
    util_inc_op(ALG_STRAND, OP_MEMORY);     // free
    free(unsorted);

    util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // free
    util_inc_op(ALG_STRAND, OP_MEMORY);     // free
    free(sublist);

    util_inc_op(ALG_STRAND, OP_FUNC_CALL);  // ll_free_all
    ll_free_all(sorted);
}

void alg_cocktail_sort(int sz, int *vec)
{
    int i;
    int begin;
    int end;
    int swap;
    int n_sz;

    util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // begin =
    begin = -1;

    util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC);   // sz - 2
    util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // end =
    end = sz - 2;

    do
    {
        util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // n_sz =
        n_sz = 0;

        util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC);   // begin+1
        util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // begin =
        ++begin;

        for (util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT), i = begin;
             util_inc_op(ALG_COCKTAIL, OP_COMPARISON) && i <= end;
             util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC),  // i+1
             util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT),  // i =
             ++i)
        {
            util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i]
            util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC);   // i+1
            util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i+1]
            util_inc_op(ALG_COCKTAIL, OP_COMPARISON);   // vec[i] > vec[i+1]
            if (vec[i] > vec[i+1])
            {
                util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i]
                util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // swap =
                swap = vec[i];

                util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i]
                util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC);   // i+1
                util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i+1]
                util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // vec[i] =
                vec[i] = vec[i+1];

                util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC);   // i+1
                util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i+1]
                util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // vec[i+1] =
                vec[i+1] = swap;

                util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // n_sz =
                n_sz = 1;
            }
        }

        util_inc_op(ALG_COCKTAIL, OP_COMPARISON);   // n_sz == 0
        if (n_sz == 0)
            break;

        util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // n_sz =
        n_sz = 0;

        util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC);   // end-1
        util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // end =
        --end;

        for (util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT), i = end;
             util_inc_op(ALG_COCKTAIL, OP_COMPARISON) && i >= begin;
             util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC),  // i-1
             util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT),  // i =
             --i)
        {
            util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i]
            util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC);   // i+1
            util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i+1]
            util_inc_op(ALG_COCKTAIL, OP_COMPARISON);   // vec[i] > vec[i+1]
            if (vec[i] > vec[i+1])
            {
                util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i]
                util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // swap =
                swap = vec[i];

                util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i]
                util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC);   // i+1
                util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i+1]
                util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // vec[i] =
                vec[i] = vec[i+1];

                util_inc_op(ALG_COCKTAIL, OP_ARITHMETIC);   // i+1
                util_inc_op(ALG_COCKTAIL, OP_INDEX);        // vec[i+1]
                util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // vec[i+1] =
                vec[i+1] = swap;

                util_inc_op(ALG_COCKTAIL, OP_ASSIGNMENT);   // n_sz =
                n_sz = 1;
            }
        }
    } while (util_inc_op(ALG_COCKTAIL, OP_COMPARISON) && n_sz != 0);
}
