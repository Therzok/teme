//
//  test.h
//  TemaAA
//
//  Created by Marius Ungureanu on 17/11/13.
//  Copyright (c) 2013 Marius Ungureanu. All rights reserved.
//

#ifndef TemaAA_test_h
#define TemaAA_test_h

/*
 * Ruleaza testerul.
 * sz: dimensiunea vectorului
 * lim: daca sa limitam la 100 sau nu
 * gen_type: tipul generat - util_gen_type
 */
void test_run(int sz, int lim, int gen_type);

#endif
