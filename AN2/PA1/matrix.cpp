//
//  matrix.cpp
//  PA1
//
//  Created by Marius Ungureanu on 27/03/14.
//  Copyright (c) 2014 Marius Ungureanu. All rights reserved.
//

#include <algorithm>
#include <fstream>
#include <vector>

using namespace std;

const int mod = 40009;
#define MOD(x) ((x) % mod)
typedef vector<vector<int> > matrix;

// Helpers:

// Identity matrix.
void m_ones(matrix &mat)
{
	for (int i = 0; i < mat.size(); ++i)
		mat[i][i] = 1;
}

// Matrix multiplication
void m_multiply(matrix &a, matrix b)
{
    int size = a.size();

    matrix res(size, vector<int>(size));

    for (int i = 0; i < size; ++i)
        for (int j = 0; j < size; ++j)
            for (int k = 0; k < size; ++k)
                res[i][j] = MOD(res[i][j]) + MOD(MOD(a[i][k]) * MOD(b[k][j]));

    a = res;
}

// Matrix multiplication with vector.
void m_multiply_v(matrix &mat, vector<int> &v)
{
    int size = mat.size();
    vector<int> res(size, 0);

    for (int i = 0; i < size; ++i)
        for (int j = 0; j < v.size(); ++j)
            res[i] = MOD(res[i] + MOD(mat[i][j]) * MOD(v[j]));
    
    v = res;
}

// Matrix exponential
void m_pow(matrix mat, int exp, matrix &result)
{
    int size = mat.size();

    m_ones(result);

    while (exp) {
        // Odd exponent.
        if (exp % 2 == 1) {
            m_multiply(result, mat);
            --exp;
        } else {
            m_multiply(mat, mat);
            exp /= 2;
        }
    }
}

// Really big pow.
void pow_ull(unsigned long long &num, int exp)
{
    unsigned long long res = 1;

    while (exp) {
        if (exp % 2 == 1)
            res = MOD(res * MOD(num));
        num = MOD(num * num);
        exp /= 2;
    }
    num = res;
}

// Input wrapper.
void read_input(const char *input, int &n, int &m, int &k)
{
    ifstream fin;
    fin.open(input);
    fin >> n >> m >> k;
    fin.close();
}

// Output wrapper.
void write_output(const char *output, unsigned long long sum)
{
    ofstream fout;
    fout.open(output);
    fout << sum << '\n';
    fout.close();
}

void run_test(const char *input, const char *output)
{
    // Original input file.
    matrix mat;
    int n;
    int m;
    int k;

    // Result.
    matrix result;
    vector<int> pows;
    unsigned long long sum = 0;
    int i;

    read_input(input, n, m, k);

    mat.resize(k + 1, vector<int>(k + 1));
    result = mat;

    pows.resize(k + 1, 0);

    // 1 on last row.
    for (i = 0; i <= k; ++i)
        mat[k][i] = 1;

    // 1 above main diagonal.
    for (i = 0; i < k; ++i)
        mat[i][i+1] = 1;

    pows[1] = 1;
    for (i = 2; i <= k; ++i) {
        pows[i] = 0;
        for (int j = 0; j < i; ++j)
            pows[i] = pows[j] + MOD(pows[i]);
    }

    m_pow(mat, n - k + 1, result);

    m_multiply_v(result, pows);

    for (i = 0; i <= k; ++i)
        sum += pows[i];

    pow_ull(sum, m);

    write_output(output, sum);
}

int main(int argc, const char *argv[])
{
    std::ios::sync_with_stdio(false);
    
    run_test("date.in", "date.out");
    return 0;
}
