//
//  sauron.cpp
//  PA1
//
//  Created by Marius Ungureanu on 13/03/14.
//  Copyright (c) 2014 Marius Ungureanu. All rights reserved.
//

#include <algorithm>
#include <fstream>
#include <vector>

using namespace std;

typedef enum {
    Master,
    Slave
} OrcType;

// Master and Slave pair.
class ms_pair {
public:
    ms_pair(int m, int s) : master(m), slave(s), dif(m - s) {};

    int master;
    int slave;
    int dif;
};

// Input reading.
istream &operator>>(istream &stream, vector<ms_pair> &vector)
{
    int master;
    int slave;
    int n;

    stream >> n;
    vector.reserve(n);

    for (int i = 0; i < n; i++) {
        stream >> master >> slave;
        vector.push_back(ms_pair(master, slave));
    }

    return stream;
}

// Input wrapper.
void read_input(const char *input, vector<ms_pair> &orcs)
{
    ifstream fin;
    fin.open(input);
    fin >> orcs;
    fin.close();
}

// Output wrapper.
void write_output(const char *output, int sum,
                  vector<int> masters, vector<int> slaves)
{
    ofstream fout;
    fout.open(output);

    fout << sum << '\n';
    for (int i = 0; i < masters.size(); ++i)
        fout << masters[i] << ' ' << slaves[i] << '\n';
    fout.close();
}

const int inf = 123456789;

// Switch if any previous pair difference is bigger than current.
void max_pos(vector<ms_pair> &orcs, vector<OrcType> &marked, int end) {
    int max = -inf;
    int pos = -inf;

	for (int i = 0; i < end; ++i) {
		if (marked[i] == Slave || orcs[i].dif < max)
            continue;

        max = orcs[i].dif;
        pos = i;
    }

    if (orcs[end].dif < max) {
        marked[pos] = Slave;
        marked[end] = Master;
    }
}

void run_test(const char *input, const char *output)
{
    // Original input file.
    vector<ms_pair> orcs;
    vector<OrcType> marked;

    // Result.
    vector<int> masters;
    vector<int> slaves;
    int sum = 0;
    int i;

    read_input(input, orcs);
    marked.reserve(orcs.size());

    for (i = 0; i < orcs.size(); i += 2) {
        marked[i] = Slave;
        marked[i+1] = Master;

        // Greedy.
        max_pos(orcs, marked, i);
    }

    for (i = 0; i < orcs.size(); ++i) {
        if (marked[i] == Master) {
            sum += orcs[i].master;
            masters.push_back(i+1);
        } else {
            sum += orcs[i].slave;
            slaves.push_back(i+1);
        }
    }

    write_output(output, sum, masters, slaves);
}

int main(int argc, const char *argv[])
{
    std::ios::sync_with_stdio(false);
    
    run_test("date.in", "date.out");
    return 0;
}
