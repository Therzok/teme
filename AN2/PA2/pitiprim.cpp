//
//  pitiprim.cpp
//  PA2
//
//  Created by Marius Ungureanu on 19/04/14.
//  Copyright (c) 2014 Marius Ungureanu. All rights reserved.
//

#include <algorithm>
#include <cmath>
#include <fstream>
#include <unordered_map>
#include <vector>

typedef unsigned long long ull;
using namespace std;

// Input wrapper.
void read_input(const char *input, unsigned &n, vector<ull> &nums)
{
    ifstream fin;
    ull x;

    fin.open(input);
    fin >> n;
    nums.reserve(n);

    for (int i = 0; i < n; ++i) {
        fin >> x;
        nums.push_back(x);
    }

    fin.close();
}

// Output wrapper.
void write_output(const char *output, const vector<ull> &res)
{
    ofstream fout;
    fout.open(output);
    for (int i = 0, n = res.size(); i < n; ++i)
        fout << res[i] << '\n';
    fout.close();
}

// Map which contains all primes calculated until now.
unordered_map<ull, bool> primes;
bool prime(const ull &num) {
    unordered_map<ull, bool>::iterator itr = primes.find(num);
    if (itr != primes.end())
        return itr->second;

    // Even numbers. Don't cache uselessly.
    if ((num & 1) == 0)
		return false;

    // Calculate and store.
    int bound = sqrt(num);
	for (int i = 3; i <= bound; i += 2)
		if (num % i == 0)
			return primes[num] = false;

    return primes[num] = true;
}

void get_digits(ull num, vector<unsigned> &digits)
{
    bool pushed_2 = false;
    bool pushed_5 = false;

    digits.clear();

    while (num) {
        unsigned val = num % 10;
        num /= 10;

        // Only insert once.
        if (val == 2) {
            if (pushed_2)
                continue;

            pushed_2 = true;
            digits.push_back(val);
            continue;
        } else if (val == 5) {
            if (pushed_5)
                continue;

            pushed_5 = true;
        }

        // Don't push even numbers.
        if (val & 1)
            digits.push_back(val);
    }
}

ull bkt(ull val, const vector<unsigned> &digits, const int &size,
        unsigned digit_mask)
{
    ull ret = 0;
    ull copy;
    // Mask for digits in number.
    unsigned mask;

    // Mask for 0-9 digits.
    unsigned used = 0;

    for (unsigned i = 0; i < size; ++i) {
        if (used & (1 << digits[i]))
            continue;

        mask = 1 << i;
        if (digit_mask & mask)
            continue;

        copy = val + digits[i];
        used |= (1 << digits[i]);

        if (prime(copy))
            ret = max(bkt(copy * 10, digits, size, digit_mask | mask),
                      max(copy, ret));
    }

    return ret;
}

void run_test(const char *input, const char *output)
{
    // Result.
    vector<ull> res;
    vector<unsigned> digits(12);

    // Original input file.
    vector<ull> nums;
    unsigned n;

    read_input(input, n, nums);
    res.reserve(n);

    // Initialize primes.
    primes[0] = primes[1] = false;
    primes[2] = true;

    for (unsigned i = 0; i < n; ++i) {
        get_digits(nums[i], digits);
        sort(digits.begin(), digits.end());
        reverse(digits.begin(), digits.end());
        res.push_back(bkt(0, digits, digits.size(), 0));
    }

    write_output(output, res);
}

int main(int argc, const char *argv[])
{
    std::ios::sync_with_stdio(false);

    run_test("pitiprim.in", "pitiprim.out");
    return 0;
}
