//
//  scrisori.cpp
//  PA2
//
//  Created by Marius Ungureanu on 19/04/14.
//  Copyright (c) 2014 Marius Ungureanu. All rights reserved.
//

#include <fstream>
#include <queue>
#include <vector>
#include <unordered_set>

using namespace std;
typedef vector<unordered_set<int> > adjacent_matrix;

// Input wrapper.
void read_input(const char *input, int &n, int &k, int &a, int &b,
                adjacent_matrix &routes)
{
    ifstream fin;
    fin.open(input);
    fin >> n >> k >> a >> b;
    routes.resize(n + 1, unordered_set<int>());

    int x, y;
    for (int i = 0; i < k; ++i) {
        fin >> x >> y;
        routes[x].insert(y);
        routes[y].insert(x);
    }

    fin.close();
}

// Output wrapper.
void write_output(const char *output, const int &res)
{
    ofstream fout;
    fout.open(output);
    fout << res << '\n';
    fout.close();
}

int traverse(const int &a, const int &b, const int &n,
             adjacent_matrix &routes)
{
    // Result.
    vector<int> costs(n + 1, a > b ? a : b);
    vector<bool> visited(n + 1);
    queue<int> q;

    // Check if crows are more favourable.
    int favourable = a > b ? b : a;
    unordered_set<int>::const_iterator end;
    int dist;

    costs[1] = 0;
    visited[1] = true;
    q.push(1);

    while (!q.empty()) {
        int current = q.front();
        q.pop();
        visited[current] = true;

        end = routes[current].cend();
        for (int k = 1; k <= n; ++k) {
            dist = (routes[current].find(k) == end) ? a : b;

            if (dist == favourable && !visited[k]) {
                q.push(k);
                visited[k] = true;
            }

            costs[k] = min(costs[current] + dist, costs[k]);
        }
    }
    return costs[n];
}

void run_test(const char *input, const char *output)
{
    // Original input file.
    int n;
    int k;
    int a;
    int b;
    adjacent_matrix routes;

    read_input(input, n, k, a, b, routes);

    write_output(output, traverse(a, b, n, routes));
}

int main(int argc, const char *argv[])
{
    std::ios::sync_with_stdio(false);
    
    run_test("scrisori.in", "scrisori.out");
    return 0;
}
