//
//  communication.cpp
//  ChessEngine
//

#include <cstring>
#include <ios>

#include "communication.hpp"

using namespace std;

char Communication::buf[] = {};

void Communication::initialize() {
    // Stream speed boost.
    ios_base::sync_with_stdio(false);
    cout.setf(ios::unitbuf);
    cin.setf(ios::unitbuf);

    // Receive xboard command and send empty response.
    receive();
    send("\n");

    for (;;) {
        // End of xboard initialization.
        if (strcmp("protover 2", receive()) != 0)
            continue;

        // TODO: Check san=1
        // Name, force XBoard to send "MOVE", cancel interrupt signal.
        send("feature myname=\"Echipa\" usermove=1 sigint=0\n");
        send("feature done=1\n");
        break;
    }
}
