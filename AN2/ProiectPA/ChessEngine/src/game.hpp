//
//  game.hpp
//  ChessEngine
//

#ifndef __ChessEngine__game__
#define __ChessEngine__game__

#include "global.hpp"
#include "chessboard.hpp"
#include "communication.hpp"

class Game {
public:
    Game() : turn(PC_END), engine(PC_END), running(true), playing(false) {};

    // Game runnable.
    void run();

private:
    // Variables
    Communication com;
    ChessBoard board;
    PieceColor turn;
    PieceColor engine;
    bool running;
    bool playing;

    // Functions

    // Calculate ChessBoard move and send it.
    void move();

    // Parse what XBoard sent.
    void parse(const char *cmd);
};

#endif /* defined(__ChessEngine__game__) */
