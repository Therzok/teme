//
//  communication.hpp
//  ChessEngine
//

#ifndef __ChessEngine__communication__
#define __ChessEngine__communication__

#include <iostream>

class Communication {
public:
    // Functions
    // Initializes XBoard features and IO speedups.
    void initialize();

    // Gets an XBoard command.
    inline char *receive() {
        std::cin.getline(buf, sizeof(buf));
        return buf;
    };

    // Sends an XBoard command.
    inline void send(const char *cmd) { std::cout << cmd; };

private:
    // Speedup buffer.
    static char buf[40];
};

#endif /* defined(__ChessEngine__communication__) */
