//
//  chessboard.hpp
//  ChessEngine
//

#ifndef __ChessEngine__chessboard__
#define __ChessEngine__chessboard__

#include <vector>

#include "global.hpp"

// Constants.
const int initial_board[BOARD_SIZE][BOARD_SIZE] = {
    {
        PF_ROOK, PF_KNIGHT,
        PF_BISHOP, PF_QUEEN,
        PF_KING, PF_BISHOP,
        PF_KNIGHT, PF_ROOK
    },
    {
        PF_PAWN, PF_PAWN,
        PF_PAWN, PF_PAWN,
        PF_PAWN, PF_PAWN,
        PF_PAWN, PF_PAWN
    },
    {},
    {},
    {},
    {},
    {
        PF_PAWN | PF_BLACK, PF_PAWN | PF_BLACK,
        PF_PAWN | PF_BLACK, PF_PAWN | PF_BLACK,
        PF_PAWN | PF_BLACK, PF_PAWN | PF_BLACK,
        PF_PAWN | PF_BLACK, PF_PAWN | PF_BLACK
    },
    {
        PF_ROOK | PF_BLACK, PF_KNIGHT | PF_BLACK,
        PF_BISHOP | PF_BLACK, PF_QUEEN | PF_BLACK,
        PF_KING | PF_BLACK, PF_BISHOP | PF_BLACK,
        PF_KNIGHT | PF_BLACK, PF_ROOK | PF_BLACK
    }
};

class Move;

class ChessBoard {
public:
    ChessBoard();

    // Functions

    // Reset game to initial.
    void initialize(PieceColor color = PC_END);

    // Check is initialized.
    bool isInitialized() { return m_initialized; }

    // Execute Engine move.
    void move(Move *m);

    // Execute XBoard move.
    void move(const char *cmd);

    // AI work to grab a move.
    const char *think();

    // Gets the piece flags at (i, j).
    inline int getPiece(int i, int j) { return board[i][j]; }

private:
    // Variables

    // Board uses PF_FLAG for type (1 bit) and color (1 bit).
    int board[BOARD_SIZE][BOARD_SIZE];
    bool m_initialized;

    // TODO: Replace with linked list.
    // Current pieces list.
    std::vector<std::vector<std::pair<int, int> > > piece_list;

    // Engine color.
    PieceColor m_color;

    // Functions

    // Checks if color is in check.
    bool inCheck(PieceColor pc, Move *m);

    // Generates a moves.
    Move *generateMove(PieceColor pc, int srcI, int srcJ, int dstI, int dstJ,
                      PieceFlag promotionType = PF_EMPTY);

    // Get a favourable piece to move.
    Move *getFavourable();

    // Algorithms.
    Move *algoPawn(int i, int j);
    Move *algoKnight(int i, int j);
    Move *algoBishop(int i, int j);
    Move *algoRook(int i, int j);
    Move *algoKing(int i, int j);
};

#endif /* defined(__ChessEngine__chessboard__) */
