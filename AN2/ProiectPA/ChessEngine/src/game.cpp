//
//  game.cpp
//  ChessEngine
//

#include <cstdlib>
#include <ctime>

#include "chessboard.hpp"
#include "communication.hpp"
#include "game.hpp"

void Game::run() {
    srand(time(NULL));
    com.initialize();
    board.initialize(engine);

    // Runnable.
    while (running)
        parse(com.receive());
}

void Game::move() {
    const char *response;

    response = board.think();

    // If we don't have a move, resign.
    if (response == NULL)
        com.send("resign\n");
    else
        com.send(response);
}

void Game::parse(const char *cmd) {
    switch (cmd[0]) {
        case 'n':   // new - Reset chessboard to initial.
            playing = true;
            turn = PC_WHITE;
            engine = PC_BLACK;
            board.initialize(engine);
            break;

        case 'f':   // force
            playing = false;
            break;

        case 'g':   // go
            playing = true;
            engine = turn;
            board.initialize(engine);
            if (engine == PC_WHITE)
                move();
            break;

        case 'w':   // white
            turn = PC_WHITE;
            engine = PC_BLACK;
            board.initialize(engine);
            break;

        case 'b':   // black
            turn = PC_BLACK;
            engine = PC_WHITE;
            board.initialize(engine);
            break;

        case 'r':   // resign
            // Don't do anything on receive.
            break;

        case 'u':   // usermove
            board.move(cmd);
            turn = engine;
            move();
            turn = engine == PC_BLACK ? PC_WHITE : PC_BLACK;
            break;

        case 'q':   // quit
            running = false;
            break;
        default:
            break;
    }
}
