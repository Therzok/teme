//
//  move.hpp
//  ChessEngine
//

#ifndef __ChessEngine__move__
#define __ChessEngine__move__

#include <string>

#include "global.hpp"

class Move {
public:
    // XBoard to Engine.
    Move(const char *cmd);

    // Engine to XBoard.
    Move(int srcI, int srcJ, int dstI, int dstJ,
         PieceFlag promotionType = PF_EMPTY);

    inline int const &getSrcI() const { return m_srcI; }
    inline int const &getSrcJ() const { return m_srcJ; }
    inline int const &getDstI() const { return m_dstI; }
    inline int const &getDstJ() const { return m_dstJ; }
    inline PieceFlag const &getPromotionType() const { return m_promotionType; }

    // Get XBoard command representation.
    inline const char *getRepresentation() const { return m_str.c_str(); }

private:
    int m_srcI;
    int m_srcJ;
    int m_dstI;
    int m_dstJ;
    PieceFlag m_promotionType;
    std::string m_str;
};

#endif /* defined(__ChessEngine__move__) */
