//
//  move.cpp
//  ChessEngine
//

#include "move.hpp"

// Treat \0 as ' '. Quick hack to get start of command text.
const int move_start = sizeof("usermove");

Move::Move(const char *cmd) {
    // J coordinate is char - 'a'.
    // I coordinate is char - '1'.

    m_srcJ = cmd[move_start] - 'a';
    m_srcI = cmd[move_start + 1] - '1';
    m_dstJ = cmd[move_start + 2] - 'a';
    m_dstI = cmd[move_start + 3] - '1';

    // Extra character yields promotion.
    switch (cmd[move_start + 4]) {
        case 'n':   // Unsure.
            m_promotionType = PF_KNIGHT;
            break;
        case 'b':
            m_promotionType = PF_BISHOP;
            break;
        case 'r':
            m_promotionType = PF_ROOK;
            break;
        case 'q':
            m_promotionType = PF_QUEEN;
            break;
        case '\0':
        default:
            m_promotionType = PF_EMPTY;
            break;
    }
}

Move::Move(int srcI, int srcJ, int dstI, int dstJ, PieceFlag promotionType) :
           m_srcI(srcI), m_srcJ(srcJ), m_dstI(dstI), m_dstJ(dstJ),
           m_promotionType(promotionType)
{
    // "move " - 5 - Command text.
    // "ijij" - 4 - Coordinates.
    // "p" - 1 - Promotion.
    // "\n\0" - 2 - End.
    m_str.reserve(5 + 4 + 1 + 2 + m_promotionType != PF_EMPTY);

    m_str.assign("move ");

    // J coordinate is char + 'a'.
    // I coordinate is char + '1'.
    m_str.push_back(m_srcJ + 'a');
    m_str.push_back(m_srcI + '1');
    m_str.push_back(m_dstJ + 'a');
    m_str.push_back(m_dstI + '1');

    // Add extra character in case of promotion.
    switch (m_promotionType) {
        case PF_KNIGHT:
            m_str.push_back('n');
            break;
        case PF_BISHOP:
            m_str.push_back('b');
            break;
        case PF_ROOK:
            m_str.push_back('r');
            break;
        case PF_QUEEN:
            m_str.push_back('q');
            break;
        default:
            break;
    }
    
    m_str.push_back('\n');
    m_str.push_back('\0');
}