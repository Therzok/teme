//
//  global.hpp
//  ChessEngine
//

#ifndef ChessEngine__global__
#define ChessEngine__global__

// Chess board size.
const int BOARD_SIZE = 8;

// Used for piece types.
typedef enum {
    PT_PAWN    = 0,
    PT_KNIGHT,
    PT_BISHOP,
    PT_ROOK,
    PT_QUEEN,
    PT_KING,
    PT_EMPTY,
    PT_MOVED,
    PT_TRIED,
    PT_BLACK               // Only used for flag building.
} PieceType;

// Flag in board for piece type and color.
typedef enum {
    PF_EMPTY   = 0,                 // Do not use &. Use == instead.
    PF_PAWN    = 1 << PT_PAWN,
    PF_KNIGHT  = 1 << PT_KNIGHT,
    PF_BISHOP  = 1 << PT_BISHOP,
    PF_ROOK    = 1 << PT_ROOK,
    PF_QUEEN   = 1 << PT_QUEEN,
    PF_KING    = 1 << PT_KING,

    // Functionality
    PF_CASTLING = PF_ROOK | PF_KING,
    PF_MOVED    = 1 << PT_MOVED,

    // Color
    PF_BLACK   = 1 << PT_BLACK
} PieceFlag;

// Piece color that is used when checking board.
typedef enum {
    PC_WHITE,
    PC_BLACK,
    PC_END
} PieceColor;

// Movement types for chess pieces.
typedef enum {
    PD_UP = 1,
    PD_DOWN = -1,
    PD_LEFT = -1,
    PD_RIGHT = 1,
} PieceDirection;

#endif
