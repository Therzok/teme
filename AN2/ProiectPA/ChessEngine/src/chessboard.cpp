//
//  chessboard.cpp
//  ChessEngine
//

#include <algorithm>
#include <cstdlib>
#include <cstring>

#include "chessboard.hpp"
#include "move.hpp"

using namespace std;

ChessBoard::ChessBoard() : m_initialized(false) {
    piece_list.push_back(vector<pair<int, int> >());
    piece_list.push_back(vector<pair<int, int> >());
}

PieceColor getColor(int piece) {
    // No piece, we don't have color.
    if (piece == PF_EMPTY)
        return PC_END;

    return piece & PF_BLACK ? PC_BLACK : PC_WHITE;
}

void ChessBoard::initialize(PieceColor color) {
    m_color = color;
    if (!m_initialized) {
        memcpy(board, initial_board, sizeof(board));
        m_initialized = true;
    }

    for (int i = 0; i < BOARD_SIZE; ++i) {
        for (int j = 0; j < BOARD_SIZE; ++j) {
            int piece = board[i][j];
            if (piece == PF_EMPTY)
                continue;

            PieceColor pc = getColor(piece);
            if ((piece & PF_KING) == PF_KING)
                piece_list[pc].insert(piece_list[pc].begin(), make_pair(i, j));
            else
                piece_list[pc].push_back(make_pair(i, j));
        }
    }
}

// Applies Move m to board.
void applyMove(int board[8][8], Move *m) {
    // Move piece.
    board[m->getDstI()][m->getDstJ()] = board[m->getSrcI()][m->getSrcJ()];

    // Promote if we have promotion type.
    if (m->getPromotionType() != PF_EMPTY)
        board[m->getDstI()][m->getDstJ()] =
            (board[m->getSrcI()][m->getSrcJ()] & PF_BLACK) |
            m->getPromotionType();

    // Unset source.
    board[m->getSrcI()][m->getSrcJ()] = PF_EMPTY;
}

void ChessBoard::move(Move *m) {
    PieceColor pc1 = getColor(board[m->getSrcI()][m->getSrcJ()]);
    PieceColor pc2 = getColor(board[m->getDstI()][m->getDstJ()]);

    // If not empty.
    if (pc2 != PC_END)
        piece_list[pc2].erase(remove(piece_list[pc2].begin(),
                                     piece_list[pc2].end(),
                                     make_pair(m->getDstI(), m->getDstJ())),
                              piece_list[pc2].end());

    replace(piece_list[pc1].begin(), piece_list[pc1].end(),
            make_pair(m->getSrcI(), m->getSrcJ()),
            make_pair(m->getDstI(), m->getDstJ()));

    applyMove(board, m);
    delete m;
}

void ChessBoard::move(const char *cmd) {
    Move *m = new Move(cmd);

    // Castling.
    if (((m->getSrcI() == 0 && m->getDstI() == 0) ||
        (m->getSrcI() == 7 && m->getDstI() == 7)) &&
        m->getSrcJ() == 4 && (m->getDstJ() == 6 || m->getDstJ() == 2)) {
        // King from src(i, j) to dst(i, j).
        // if dst == 6
        //  - Rook from src(i, 7) to dst(i, 5).
        // if dst == 2
        //  - Rook from src(i, 0) to dst(i, 3).

        if (m->getDstJ() == 6)
            move(new Move(m->getSrcI(), 7, m->getDstI(), 5));
        else // if (m->getDstJ() == 2)
            move(new Move(m->getSrcI(), 0, m->getDstI(), 3));
    }
    move(m);
}

Move *ChessBoard::getFavourable() {
    vector<pair<int, int> > copy(piece_list[m_color]);
    Move *m = NULL;

    // O(n) worst case.
    while (!copy.empty() && m == NULL) {
        int pos = rand() % copy.size();
        int i = copy[pos].first;
        int j = copy[pos].second;

        int piece = board[i][j];
        if ((piece & PF_PAWN) == PF_PAWN)
            m = algoPawn(i, j);
        else if ((piece & PF_KNIGHT) == PF_KNIGHT)
            m = algoKnight(i, j);
        else if ((piece & PF_BISHOP) == PF_BISHOP)
            m = algoBishop(i, j);
        else if ((piece & PF_ROOK) == PF_ROOK)
            m = algoRook(i, j);
        else if ((piece & PF_QUEEN) == PF_QUEEN) {
            m = algoRook(i, j);
            if (m == NULL)
                m = algoBishop(i, j);
        } else if ((piece & PF_KING) == PF_KING)
            m = algoKing(i, j);

        copy.erase(remove(copy.begin(), copy.end(), make_pair(i, j)),
                   copy.end());
    }
    return m;
}

const char *ChessBoard::think() {
    Move *m = getFavourable();
    if (m == NULL)
        return NULL;

    const char *cmd = m->getRepresentation();
    move(m);
    return cmd;
}

#define isInBounds(x) (x >= 0 && x < BOARD_SIZE)
bool checkEnemy(PieceColor c1, PieceColor c2)
{
    return c2 != PC_END && c2 != c1;
}

bool checkAlly(PieceColor c1, PieceColor c2)
{
    return c1 == c2;
}

// Algorithms:
static const int pawn_dst[] = {
    1 * PD_LEFT,
    1 * PD_RIGHT
};

static const int knight_dst[][2] = {
    { 1 * PD_UP,    2 * PD_LEFT },
    { 2 * PD_UP,    1 * PD_LEFT },
    { 1 * PD_UP,    2 * PD_RIGHT },
    { 2 * PD_UP,    1 * PD_RIGHT },
    { 1 * PD_DOWN,  2 * PD_LEFT },
    { 2 * PD_DOWN,  1 * PD_LEFT},
    { 1 * PD_DOWN,  2 * PD_RIGHT },
    { 2 * PD_DOWN,  1 * PD_RIGHT }
};

static const int bishop_dst[][2] = {
    { 1 * PD_UP,    1 * PD_LEFT },
    { 1 * PD_DOWN,  1 * PD_LEFT },
    { 1 * PD_UP,    1 * PD_RIGHT },
    { 1 * PD_DOWN,  1 * PD_RIGHT }
};

static const int rook_dst[][2] = {
    { 1 * PD_UP,    0 },
    { 1 * PD_DOWN,  0 },
    { 0,            1 * PD_RIGHT },
    { 0,            1 * PD_LEFT }
};

static const int king_dst[][2] = {
    { 1 * PD_UP,    1 * PD_LEFT },
    { 1 * PD_UP,    0 },
    { 1 * PD_UP,    1 * PD_RIGHT },
    { 0,            1 * PD_RIGHT },
    { 1 * PD_DOWN,  1 * PD_RIGHT },
    { 1 * PD_DOWN,  0 },
    { 1 * PD_DOWN,  1 * PD_LEFT },
    { 0,            1 * PD_LEFT }
};

// Gets direction for pawns based on color.
PieceDirection getPawnDirection(PieceColor pc) {
    return (pc == PC_BLACK) ? PD_DOWN : PD_UP;
}

Move *ChessBoard::algoPawn(int i, int j) {
    Move *m = NULL;
    PieceColor pc = getColor(board[i][j]);
    PieceDirection dir = getPawnDirection(pc);
    PieceFlag pf = PF_EMPTY;
    int p;

    // If we're on top or bottom, promote.
    if (i + dir == 0 || i + dir == BOARD_SIZE - 1)
        pf = PF_QUEEN;

    // If check if we can capture diagonally.
    for (int k = 0; k < 2; ++k) {
        if (!isInBounds(j + pawn_dst[k]))
            continue;

        if (checkEnemy(pc, getColor(board[i + dir][j + pawn_dst[k]]))) {
            m = generateMove(pc, i, j, i + dir, j + pawn_dst[k], pf);
            if (m != NULL)
                return m;
        }
    }

    // If it's an empty spot.
    p = board[i + dir][j];
    if (p == PF_EMPTY) {
        m = generateMove(pc, i, j, i + dir, j, pf);
        if (m != NULL)
            return m;
    }

    return m;
}

Move *ChessBoard::algoKnight(int i, int j) {
    Move *m = NULL;
    PieceColor pc = getColor(board[i][j]);
    int dst_i;
    int dst_j;
    int p;

    // If check if we can capture diagonally.
    for (int k = 0; k < 8; ++k) {
        dst_i = i + knight_dst[k][0];
        dst_j = j + knight_dst[k][1];
        if (!isInBounds(dst_i) || !isInBounds(dst_j))
            continue;

        p = board[dst_i][dst_j];
        if (p == PF_EMPTY || checkEnemy(pc, getColor(p))) {
            m = generateMove(pc, i, j, dst_i, dst_j);
            if (m != NULL)
                return m;
        }
    }
    return m;
}

Move *ChessBoard::algoBishop(int i, int j) {
    Move *m = NULL;
    PieceColor pc = getColor(board[i][j]);
    int dst_i;
    int dst_j;
    int p;

    for (int k = 0; k < 4; ++k) {
        for (int l = 1; l < BOARD_SIZE; ++l) {
            dst_i = i + bishop_dst[k][0] * l;
            dst_j = j + bishop_dst[k][1] * l;
            if (!isInBounds(dst_i) || !isInBounds(dst_j))
                break;

            p = board[dst_i][dst_j];
            if (checkAlly(pc, getColor(p)))
                break;

            bool stop = false;
            if (p == PF_EMPTY || (stop = checkEnemy(pc, getColor(p)))) {
                m = generateMove(pc, i, j, dst_i, dst_j);
                if (m != NULL)
                    return m;
                else if (stop)
                    break;
            }
        }
    }
    return m;
}

Move *ChessBoard::algoRook(int i, int j) {
    Move *m = NULL;
    PieceColor pc = getColor(board[i][j]);
    int dst_i;
    int dst_j;
    int p;

    for (int k = 0; k < 4; ++k) {
        for (int l = 1; l < BOARD_SIZE; ++l) {
            dst_i = i + rook_dst[k][0] * l;
            dst_j = j + rook_dst[k][1] * l;
            if (!isInBounds(dst_i) || !isInBounds(dst_j))
                break;

            p = board[dst_i][dst_j];
            if (checkAlly(pc, getColor(p)))
                break;

            bool stop = false;
            if (p == PF_EMPTY || (stop = checkEnemy(pc, getColor(p)))) {
                m = generateMove(pc, i, j, dst_i, dst_j);
                if (m != NULL)
                    return m;
                else if (stop)
                    break;
            }
        }
    }
    return m;
}

Move *ChessBoard::algoKing(int i, int j) {
    Move *m = NULL;
    PieceColor pc = getColor(board[i][j]);
    int dst_i;
    int dst_j;
    int p;

    for (int k = 0; k < 8; ++k) {
        dst_i = i + king_dst[k][0];
        dst_j = j + king_dst[k][1];
        if (!isInBounds(dst_i) || !isInBounds(dst_j))
            continue;

        p = board[dst_i][dst_j];
        if (p == PF_EMPTY || checkEnemy(pc, getColor(p))) {
            m = generateMove(pc, i, j, dst_i, dst_j);
            if (m != NULL)
                return m;
        }
    }
    return m;
}

Move *ChessBoard::generateMove(PieceColor pc, int srcI, int srcJ, int dstI,
                               int dstJ, PieceFlag promotionType) {
    Move *m = new Move(srcI, srcJ, dstI, dstJ, promotionType);
    if (inCheck(pc, m)) {
        delete m;
        m = NULL;
    }

    return m;
}

bool ChessBoard::inCheck(PieceColor pc, Move *m) {
    int copy[BOARD_SIZE][BOARD_SIZE];
    int i = piece_list[pc][0].first;
    int j = piece_list[pc][0].second;
    int dst_i;
    int dst_j;
    int p;

    // If we're moving king.
    if (m->getSrcI() == i && m->getSrcJ() == j) {
        i = m->getDstI();
        j = m->getDstJ();
    }

    memcpy(copy, board, sizeof(int[BOARD_SIZE][BOARD_SIZE]));
    applyMove(copy, m);

    // Check pawns.
    PieceDirection dir = getPawnDirection(pc);
    for (int k = 0; k < 2; ++k) {
        dst_i = i + dir;
        dst_j = j + pawn_dst[k];
        if (!isInBounds(dst_j))
            continue;

        p = copy[dst_i][dst_j];
        if ((p & PF_PAWN) == PF_EMPTY)
            continue;

        if (p != PF_EMPTY && checkEnemy(pc, getColor(p)))
            return true;
    }

    // Check knights.
    for (int k = 0; k < 8; ++k) {
        dst_i = i + knight_dst[k][0];
        dst_j = j + knight_dst[k][1];
        if (!isInBounds(dst_i) || !isInBounds(dst_j))
            continue;

        p = copy[dst_i][dst_j];
        if ((p & PF_KNIGHT) == PF_EMPTY)
            continue;

        if (p != PF_EMPTY && checkEnemy(pc, getColor(p)))
            return true;
    }

    // Check bishop, rook, queen.
    for (int k = 0; k < 4; ++k) {
        for (int l = 1; l < BOARD_SIZE; ++l) {
            dst_i = i + bishop_dst[k][0] * l;
            dst_j = j + bishop_dst[k][1] * l;
            if (!isInBounds(dst_i) || !isInBounds(dst_j))
                break;

            p = copy[dst_i][dst_j];
            if (checkAlly(pc, getColor(p)))
                break;

            if ((p & (PF_BISHOP | PF_QUEEN)) == PF_EMPTY)
                continue;

            if (p != PF_EMPTY && checkEnemy(pc, getColor(p)))
                return true;
        }

        for (int l = 1; l < BOARD_SIZE; ++l) {
            dst_i = i + rook_dst[k][0] * l;
            dst_j = j + rook_dst[k][1] * l;
            if (!isInBounds(dst_i) || !isInBounds(dst_j))
                break;

            p = copy[dst_i][dst_j];
            if (checkAlly(pc, getColor(p)))
                break;

            if ((p & (PF_ROOK | PF_QUEEN)) == PF_EMPTY)
                continue;

            if (p != PF_EMPTY && checkEnemy(pc, getColor(p)))
                return true;
        }
    }

    // Check king.
    for (int k = 0; k < 8; ++k) {
        dst_i = i + king_dst[k][0];
        dst_j = j + king_dst[k][1];
        if (!isInBounds(dst_i) || !isInBounds(dst_j))
            continue;

        p = board[dst_i][dst_j];
        if ((p & PF_KING) == PF_EMPTY)
            continue;

        if (p != PF_EMPTY && checkEnemy(pc, getColor(p)))
            return true;
    }
    
    return false;
}
