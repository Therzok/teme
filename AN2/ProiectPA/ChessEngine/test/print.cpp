//
//  print.cpp
//  ChessEngine
//

#include <iostream>

#include "print.hpp"
#include "../src/chessboard.hpp"

using namespace std;

void print(ChessBoard &board, const char *message)
{
#ifdef PRINT
#define RESET "\033[0m"
#define CYAN "\033[36m"
#define GRAY "\033[37m"
    cout << message << '\n';
    for (int i = BOARD_SIZE - 1; i >= 0; --i) {
        for (int j = 0; j < BOARD_SIZE; ++j) {
            int p = board.getPiece(i, j);
            if (p == PF_EMPTY)
                cout << GRAY;
            else if ((p & PF_BLACK) == PF_EMPTY)
                cout << CYAN;

            if (has_bit(p, PF_PAWN))
                cout << 'P';
            else if (has_bit(p, PF_KNIGHT))
                cout << 'N';
            else if (has_bit(p, PF_BISHOP))
                cout << 'B';
            else if (has_bit(p, PF_ROOK))
                cout << 'R';
            else if (has_bit(p, PF_QUEEN))
                cout << 'Q';
            else if (has_bit(p, PF_KING))
                cout << 'K';
            else
                cout << ' ';
            cout << RESET << ' ';
        }
        cout << '\n';
    }
#endif
}
