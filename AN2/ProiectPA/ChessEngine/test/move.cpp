//
//  move.cpp
//  ChessEngine
//

#include <cstring>

#include "catch.hpp"
#include "../src/global.hpp"
#include "../src/move.hpp"

TEST_CASE("Tests construction of various moves", "Move::Move") {
    SECTION("Test pawn movement") {
        Move m("usermove e2e4");
        REQUIRE(m.getSrcI() == 1);
        REQUIRE(m.getDstI() == 3);
        REQUIRE(m.getSrcJ() == 4);
        REQUIRE(m.getDstJ() == 4);
    }
    SECTION("Test knight movement") {
        Move m("usermove b1c3");
        REQUIRE(m.getSrcI() == 0);
        REQUIRE(m.getDstI() == 2);
        REQUIRE(m.getSrcJ() == 1);
        REQUIRE(m.getDstJ() == 2);
    }
    SECTION("Test pawn promotion") {
        Move m("usermove e7e8q");
        REQUIRE(m.getSrcI() == 6);
        REQUIRE(m.getDstI() == 7);
        REQUIRE(m.getSrcJ() == 4);
        REQUIRE(m.getDstJ() == 4);
        REQUIRE(m.getPromotionType() == PF_QUEEN);
    }
    SECTION("Test pawn promotion knight") {
        Move m("usermove e7e8n");
        REQUIRE(m.getPromotionType() == PF_KNIGHT);
    }
    SECTION("Test construct representation") {
        Move m(1, 4, 3, 4);
        REQUIRE(strcmp(m.getRepresentation(), "move e2e4\n") == 0);
    }
    SECTION("Test construct with promotion") {
        Move m(6, 4, 7, 4, PF_QUEEN);
        REQUIRE(strcmp(m.getRepresentation(), "move e7e8q\n") == 0);
    }
}