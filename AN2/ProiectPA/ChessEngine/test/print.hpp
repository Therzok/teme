//
//  print.h
//  ChessEngine
//

#ifndef __ChessEngine__print__
#define __ChessEngine__print__

// Uncomment to print board.
#define PRINT

class ChessBoard;

void print(ChessBoard &board, const char *message);

#define has_bit(x, bit) (x & bit) == bit

#endif /* defined(__ChessEngine__print__) */
