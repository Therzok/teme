//
//  chessboard.cpp
//  ChessEngine
//

#include "catch.hpp"
#include "print.hpp"
#include "../src/chessboard.hpp"
#include <vector>

TEST_CASE("Tests ChessBoard getPiece movement", "ChessBoard::getPiece") {
    ChessBoard board;
    board.initialize();
    for (int i = 0; i < BOARD_SIZE; ++i)
        for (int j = 0; j < BOARD_SIZE; ++j) {
            int piece = board.getPiece(i, j);
            if (i < 2)
                REQUIRE((piece & PF_BLACK) == PF_EMPTY);
            else if (i >= 6)
                REQUIRE(has_bit(piece, PF_BLACK));
        }
}


TEST_CASE("Tests ChessBoard move", "ChessBoard::move") {
    ChessBoard board;
    board.initialize();

    SECTION("Normal movement") {
        print(board, "Initial");
        board.move("usermove e2e4");
        REQUIRE(has_bit(board.getPiece(3, 4), PF_PAWN));
        REQUIRE(board.getPiece(1, 4) == PF_EMPTY);
        print(board, "Moved pawn from e2 to e4");
    }
    SECTION("Promotion movement") {
        print(board, "Initial");
        board.move("usermove e2e8q");   // Illegal.
        REQUIRE(has_bit(board.getPiece(7, 4), PF_QUEEN));
        REQUIRE(board.getPiece(1, 4) == PF_EMPTY);
        print(board, "e2 pawn is now queen in e8");
    }
}
