module AIEmpty where

import Board
import Data.List
import Data.Ord
import Interactive

{-
    Tip de date pentru a un nod de tip (Board, NumarZerouri).
-}
data Result = MkResult { board :: Board
                       , zeroes :: Int }
    deriving Eq

{-
    Calculeaza numarul de zero-uri din tabla de joc.
-}
countZeros :: Board -> Result
countZeros b = MkResult b num
              where list = concat $ rows b
                    num = sum $ map fromEnum $ map (== 0) list
{-
    Aplica toate functiile de miscare.
-}
moves :: Board -> [Board]
moves b = filter (/= b) [ moveLeft b
                        , moveRight b
                        , moveUp b
                        , moveDown b]

{-
    Întoarce tabla rezultată din aplicarea acelei mutări care maximizează
    numărul de celule libere.
-}
move :: Board -> Board
move b = board $ maximumBy (comparing zeroes) $ map countZeros (moves b)

{-
    Urmărește pas cu pas evoluția jocului, conform strategiei implementate.
-}
userMode :: IO ()
userMode = ai move

