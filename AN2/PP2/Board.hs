{-
    Tabla de joc și mutările posibile.

    Modulul exportă numai funcțiile enumerate mai jos, ceea ce înseamnă
    că doar acestea vor fi vizibile din alte module. Justificarea vizează
    prevenirea accesului extern la structura obiectelor 'Board', și a eventualei
    coruperi a consistenței interne a acestora.
-}
module Board
    ( Board
    , build
    , rows
    , score
    , initialize
    , placeRandomCell
    , moveUp
    , moveDown
    , moveLeft
    , moveRight
    , isWon
    , isLost
    ) where

import System.Random
import Data.List

{-
    Definiți tipul 'Board', astfel încât să rețină informație despre tabla
    de joc și despre scorul curent.

    Observați că viitorii constructori de date și eventualele câmpuri pe care
    le veți specifica nu vor apărea în lista de funcții exportate de modul
    (vezi explicația de la începutul fișierului).
-}
data Board = MkBoard { board :: [[Int]]
                     , points :: Int }
   deriving Eq

{-
    Instanțiați clasa 'Show' cu tipul 'Board'. Exemplu de reprezentare:

       . |    4 |    2 |    4
       2 |    . |    4 |   32
       . |    . |    8 |  128
       8 |   32 |   64 |    8
    Score: 1216
-}

{-
    Transforma un string intr-un string left padded cu spatii,
    avand maxim 4 caractere.
-}
padString :: String -> String
padString x = (take needed $ repeat ' ') ++ y
            where y = case x of
                          "0" -> "."
                          _ -> x
                  needed = 4 - length y

{-
    Transforma matricea de valori in matrice de stringuri left padded.
-}
toPaddedStrings :: [[Int]] -> [[String]]
toPaddedStrings = map $ map (padString . show)

{-
    Intercaleaza pipe-uri. Pretty-pretty print.
-}
displayPoints :: [[String]] -> [String]
displayPoints = map (intercalate "| ")

{-
    Afisarea jocului.
-}
instance Show Board where
    show a = unlines $ gameboard ++ ["Score: " ++ s]
            where gameboard = displayPoints . toPaddedStrings $ rows a
                  s = show (score a)
{-
    Construiește o tablă de joc pe baza unei configurații, furnizate pe linii,
    și a unui scor.
-}
build :: [[Int]] -> Int -> Board
build = MkBoard

{-
    Întoarce configurația tablei de joc.
-}
rows :: Board -> [[Int]]
rows = board

{-
    Întoarce scorul curent al tablei de joc.
-}
score :: Board -> Int
score = points

{-
    Inlocuieste o valoare intr-o lista la pozitia data.
-}
preplace :: [a] -> a -> Int -> [a]
preplace list val pos = before ++ [val] ++ after
                        where before = take pos list
                              after = drop (pos + 1) list
{-
    Alege o pozitie in lista facand modulo lungime.
-}
position :: [a] -> Int -> Int
position list num = num `mod` length list

{-
    Intoarce 4 daca numarul e impar, 2 daca e par.
-}
getRandomValue :: Int -> Int
getRandomValue val
  | odd val = 4
  | otherwise = 2

{-
    Cauta prima celula aleatorie libera.
-}
findRandomFree :: RandomGen g => Board -> g -> (Int, Int, g)
findRandomFree b gen
  | cell == 0 = (posr, posc, gen'')
  | otherwise = findRandomFree b gen''
      where r = rows b
            (randposr, gen') = next gen   -- Get random row position.
            (randposc, gen'') = next gen' -- Get random column position.
            posr = position r randposr    -- Mod random row position to board size.
            row = head $ drop posr r      -- Get the row.
            posc = position row randposc  -- Mod random column position to row size.
            cell = head $ drop posc row   -- Get cell at position.

{-
    Plasează aleator o nouă celulă pe tabla de joc.

    Aveți grijă să nu modificați celulele existente!
-}
placeRandomCell :: RandomGen g => Board -> g -> (Board, g)
placeRandomCell b gen = (build r' s, gen'')
                          where r = rows b
                                s = score b
                                (val, gen') = next gen
                                val' = getRandomValue val     -- Get cell value to add.
                                (posr, posc, gen'') = findRandomFree b gen'
                                row = head $ drop posr r      -- Get row to modify.
                                row' = preplace row val' posc  -- Modified row.
                                r' = preplace r row' posr      -- Modified board.
{-
    Tabla initiala 4x4 cu 0-uri.
-}
initial = build (replicate 4 $ replicate 4 0) 0

{-
    Generează aleator o tablă de joc cu două celule ocupate.
-}
initialize :: RandomGen g => g -> (Board, g)
initialize gen = (b'', gen'')
                  where b = initial
                        (b', gen') = placeRandomCell b gen
                        (b'', gen'') = placeRandomCell b' gen'

{-
    Calculeaza scorul facut la miscarea curenta.
-}
summer :: [Int] -> Int
summer row = case row of
              [] -> 0
              [a] -> 0
              _ -> sum + summer remaining'
              where val = head row
                    remaining = tail row
                    next = head remaining
                    (sum, remaining')
                      | val == next = (val + next, tail remaining)
                      | otherwise = (0, remaining)
{-
    Face merge la celulele adiacente cu aceeasi valoare.
-}
merger :: [Int] -> [Int]
merger row = case row of
              [] -> []
              [a] -> row
              _ -> [merged] ++ merger remaining'
              where val = head row
                    remaining = tail row
                    next = head remaining
                    (merged, remaining')
                      | val == next = (val + next, tail remaining)
                      | otherwise = (val, remaining)

{-
    Elimina zerourile, face merge la celule si calculeaza noul scor.
-}
moveLeft' :: [[Int]] -> Int -> Board
moveLeft' r s = build r' s'
            where dropped = map (filter (/= 0)) r
                  merged = map merger dropped
                  s' = s + (sum $ map summer dropped)
                  r' = map (\x -> take 4 $ x ++ repeat 0) merged

{-
    Cele patru mutări posibile: stânga, dreapta, sus, jos.

    Acestea sunt singurele funcții care modifică scorul.
-}
moveLeft :: Board -> Board
moveLeft b = moveLeft' r s
          where r = rows b
                s = score b
{-
    Wrapper pentru celelalte functii pentru a nu duplica cod.

    Este necesara varianta cu 2 functii, fiindca moveDown are o ordine a
    operatiilor ce trebuie respectata.
-}
moveOther' :: ([[Int]] -> [[Int]]) -> ([[Int]] -> [[Int]]) -> Board -> Board
moveOther' f1 f2 b = res'
                where r = rows b
                      s = score b
                      b' = build (f1 r) s
                      res = moveLeft b'
                      r' = rows res
                      s' = score res
                      res' = build (f2 r') s'

{-
    Wrapper pentru celelalte functii pentru a nu duplica cod.
-}
moveOther :: ([[Int]] -> [[Int]]) -> Board -> Board
moveOther f b = moveOther' f f b

moveUp :: Board -> Board
moveUp = moveOther $ transpose

moveRight :: Board -> Board
moveRight = moveOther $ map reverse

moveDown :: Board -> Board
moveDown = moveOther' ((map reverse) . transpose) (transpose . (map reverse))

{-
    Cauta o valoare in tabla de joc.
-}
findValue :: Int -> Board -> Bool
findValue val = foldl (||) False . map (== val) . concat . rows

{-
    Întoarce 'True' dacă tabla conține o configurație câștigătoare,
    i.e. există cel puțin o celulă cu 2048.
-}
isWon :: Board -> Bool
isWon = findValue 2048

{-
    Verifica daca merger a facut vreo schimbare.
-}
findAdj :: [[Int]] -> Bool
findAdj a = (map merger a) /= a

{-
    Întoarce 'True' dacă tabla conține o configurație în care jucătorul pierde,
    i.e. nu există nicio celulă liberă, și nici nu există celule vecine egale,
    pe orizontală sau verticală.
-}
isLost :: Board -> Bool
isLost b = not $ (findValue 0 b)
              || (findAdj rleft)  -- left
              || (findAdj rright) -- right
              || (findAdj rup)    -- up
              || (findAdj rdown)  -- down
  where rleft = rows b
        rright = map reverse rleft
        rup = transpose rleft
        rdown = transpose rright

