#ifndef _helper_h_
#define _helper_h_

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define LEN_TRANS   1024
#define LEN_NORMAL  256
#define LEN_SMALL   40
#define LEN_PAYLOAD 140

#define max(a, b) ((a > b) ? a : b)
void die()
{
    perror("");
    exit(1);
}

typedef enum {
    MT_CONNECT,
    MT_QUIT,
    MT_LIST_CLIENTS,
    MT_INFO_CLIENT,
    MT_SHARE_FILE,
    MT_UNSHARE_FILE,
    MT_GET_SHARE,
    MT_INFO_FILE,
    MT_GET_FILE,
    MT_ERROR,
    MT_DISCONNECT,
    MT_TRANS_FILE,
    MT_STATUS,
    MT_UNKNOWN,
    MT_ALL
} MsgType;

typedef struct 
{
	int type;
	short port;
	char client[LEN_SMALL];
	char payload[LEN_PAYLOAD];
} msg;

typedef struct
{
	long long size;
	char client_name[LEN_SMALL];
	char file_name[LEN_SMALL];
} filetype;

typedef struct 
{
	time_t timestamp;
	int socket;
	short port;
	char IP[LEN_SMALL / 2];
	char name[LEN_SMALL];
} infoclient;

#endif
