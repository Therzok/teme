#include "helper.h"

/* Client configuration */
char client_name[LEN_SMALL];
char local_dir[LEN_SMALL];
FILE *flog;

/* Globals */
struct timeval tout = { 0, 100 };
infoclient info[LEN_SMALL];
msg pkt;

char buffer[LEN_NORMAL];
int sending;
fd_set read_fds;
int fd_max;
int socksend, sockpas, sockrecv, sockfd;
int send_fd, write_fd;
int infoit;

int console_null(msg pkt, char *arg1, char *arg2)
{
    return 1;
}

int console_pong(msg pkt, char *arg1, char *arg2)
{
    strcpy(pkt.client, client_name);
    memcpy(buffer, &pkt, sizeof(buffer));
    send(sockfd, buffer, sizeof(buffer), 0);
    return 1;
}

int console_pong_last(msg pkt, char *arg1, char *arg2)
{
    return !console_pong(pkt, arg1, arg2);
}

int console_pong_arg(msg pkt, char *arg1, char *arg2)
{
    strcpy(pkt.payload, arg1);
    return console_pong(pkt, arg1, arg2);
}

int console_handle_share(msg pkt, char *arg1, char *arg2)
{
    char tmp[LEN_NORMAL];
    struct stat st;
    filetype ft;

    strcpy(tmp, local_dir);
    strcat(tmp, arg1);

    if (stat(tmp, &st) == -1) {
        fprintf(flog, "-2: Fisier inexistent: %s.\n", tmp);
        printf("-2: Fisier inexistent: %s.\n", tmp);
    } else {
        fprintf(flog, "Shared: (%s).\n", tmp);
        printf("Shared: (%s).\n", tmp);

        strcpy(ft.file_name, arg1);
        ft.size = st.st_size;
        strcpy(ft.client_name, client_name);

        memcpy(pkt.payload, &ft, sizeof(pkt.payload));
        return console_pong(pkt, arg1, arg2);
    }
    return 1;
}

int console_handle_get_file(msg pkt, char *arg1, char *arg2)
{
    char tmp[LEN_NORMAL];
    struct stat st;

    strcpy(tmp, local_dir);
    strcat(tmp, arg2);

    sprintf(pkt.payload, "%s %s", arg1, arg2);

    if (stat(tmp, &st) == -1)
        console_pong(pkt, arg1, arg2);
    else {
        fprintf(flog, ">File (%s) exists. Overwrite? y/n\n", arg2);
        printf("File (%s) exists. Overwrite? y/n\n", arg2);

        fgets(tmp, LEN_NORMAL, stdin);
        if (strcmp(tmp, "y\n") == 0)
            console_pong(pkt, arg1, arg2);

        fprintf(flog, ">File overwritten: %s\n", tmp);
    }
    return 1;
}

int (*console_handlers[MT_ALL])(msg pkt, char *arg1, char *arg2) = {
    &console_null,              /* MT_CONNECT */
    &console_pong_last,         /* MT_QUIT */
    &console_pong,              /* MT_LIST_CLIENTS */
    &console_pong_arg,          /* MT_INFO_CLIENT */
    &console_handle_share,      /* MT_SHARE_FILE */
    &console_pong_arg,          /* MT_UNSHARE_FILE */
    &console_pong_arg,          /* MT_GET_SHARE */
    &console_pong_arg,          /* MT_INFO_FILE */
    &console_handle_get_file,   /* MT_GET_FILE */
    &console_null,              /* MT_ERROR */
    &console_null,              /* MT_DISCONNECT */
    &console_null,              /* MT_TRANS_FILE */
    &console_null,              /* MT_STATUS */
    &console_null               /* MT_UNKOWN */
};

MsgType parse_console(char *msg)
{
    if (!strcmp(msg, "listclients"))
        return MT_LIST_CLIENTS;
    if (!strcmp(msg, "infoclient"))
        return MT_INFO_CLIENT;
    if (!strcmp(msg, "sharefile"))
        return MT_SHARE_FILE;
    if (!strcmp(msg, "unsharefile"))
        return MT_UNSHARE_FILE;
    if (!strcmp(msg, "getshare"))
        return MT_GET_SHARE;
    if (!strcmp(msg, "infofile"))
        return MT_INFO_FILE;
    if (!strcmp(msg, "getfile"))
        return MT_GET_FILE;
    if (!strcmp(msg, "quit"))
        return MT_QUIT;
    return MT_UNKNOWN;
}

int handle_console()
{
    char *arg1;
    char *arg2;

    fgets(buffer, LEN_NORMAL, stdin);

    fprintf(flog, ">%s", buffer);
    buffer[strlen(buffer) - 1] = '\0';

    pkt.type = parse_console(strtok(buffer, " "));
    arg1 = strtok(NULL, " ");
    arg2 = arg1 != NULL ? strtok(NULL, " ") : NULL;
    return !(*console_handlers[pkt.type])(pkt, arg1, arg2);
}

void handle_send()
{
    char tmp[LEN_NORMAL] = {};
    struct sockaddr_in cli_addr;
    const int clilen = sizeof(cli_addr);

    sending = 1;

    socksend = accept(sockpas, (struct sockaddr *)&cli_addr, (socklen_t*)&clilen);
    if (socksend < 0)
        die();
    else {
        recv(socksend, buffer, sizeof(buffer), 0);
        memcpy(&pkt, buffer, sizeof(pkt));
        sprintf(tmp, "%s%s", local_dir, pkt.payload);
        send_fd = open(tmp, O_RDONLY);
    }
}

void handle_receive()
{
    char buffer_file[LEN_TRANS] = {};
    int copiat = recv(sockrecv, buffer_file, sizeof(buffer_file), 0);
    if (copiat < 0)
        return;

    write(write_fd, buffer_file, copiat);
    if (copiat == sizeof(buffer_file))
        return;

    printf("File received successfully.\n");
    FD_CLR(sockrecv, &read_fds);
    close(write_fd);
    close(sockrecv);
    write_fd = 0;
}

void try_send()
{
    char buffer_file[LEN_TRANS] = {};
    if (sending == 0)
        return;

    int copiat = read(send_fd, buffer_file, sizeof(buffer_file));
    if (copiat < 0)
        die();

    send(socksend, buffer_file, copiat, 0);
    if (copiat != 0)
        return;

    printf("File sent succesfully.\n");
    sending = 0;
    close(send_fd);
}

int server_null(msg pkt)
{
    return 1;
}

int server_pong(msg pkt)
{
    fprintf(flog, "%s\n", pkt.payload);
    printf("%s\n", pkt.payload);
    return 1;
}

int server_pong_last(msg pkt)
{
    return !server_pong(pkt);
}

int server_info_client(msg pkt)
{
    char tmp[LEN_SMALL] = {};
    infoclient c;
    memcpy(&c, pkt.payload, sizeof(c));
    struct tm *t = localtime(&c.timestamp);
    strftime(tmp, sizeof(tmp), "%T", t);

    fprintf(flog, "Client (%s):\n", c.name);
    fprintf(flog, "- IP: %s\n", c.IP);
    fprintf(flog, "- Joined: %s\n", tmp);

    printf("Client:\n");
    printf("- Name: %s\n", c.name);
    printf("- IP: %s\n", c.IP);
    printf("- Joined: %s\n", tmp);
    memcpy(&info[infoit], &c, sizeof(c));
    ++infoit;
    return 1;
}

int server_trans_file(msg pkt)
{
    struct sockaddr_in cl_addr;
    char tmp[LEN_NORMAL] = {};
    char copy[LEN_PAYLOAD] = {};
    strcpy(copy, pkt.payload);
    char *clientname = strtok(copy, " ");
    char *filename = strtok(NULL, " ");
    int k;

    strcpy(pkt.payload, filename);

    for (k = 0; k < infoit; k++) {
        if (strcmp(clientname, info[k].name) != 0)
            continue;

        inet_aton(info[k].IP, &cl_addr.sin_addr);
        cl_addr.sin_port = htons(info[k].port);
        cl_addr.sin_family = AF_INET;
        break;
    }

    fd_max = max(sockrecv, fd_max);
    FD_SET(sockrecv, &read_fds);

    if (connect(sockrecv, (struct sockaddr*)&cl_addr, sizeof(cl_addr)) < 0)
        die();

    fprintf(flog, "Connected to client %s\n", clientname);
    printf("Connected to client %s\n", clientname);

    strcpy(tmp, local_dir);
    strcat(tmp, filename);

    if (write_fd) {
        close(write_fd);
        write_fd = 0;
    }
    write_fd = open(tmp, O_WRONLY | O_CREAT, 0644);

    memcpy(buffer, &pkt, sizeof(buffer));
    send(sockrecv, buffer, sizeof(buffer), 0);
    return 1;
}

int (*server_handlers[MT_ALL])(msg pkt) = {
    &server_null,           /* MT_CONNECT */
    &server_null,           /* MT_QUIT */
    &server_pong,           /* MT_LIST_CLIENTS */
    &server_info_client,    /* MT_INFO_CLIENT */
    &server_null,           /* MT_SHARE_FILE */
    &server_pong,           /* MT_UNSHARE_FILE */
    &server_pong,           /* MT_GET_SHARE */
    &server_pong,           /* MT_INFO_FILE */
    &server_null,           /* MT_GET_FILE */
    &server_pong,           /* MT_ERROR */
    &server_pong_last,      /* MT_DISCONNECT */
    &server_trans_file,     /* MT_TRANS_FILE */
    &server_null,           /* MT_STATUS */
    &server_null            /* MT_UNKOWN */
};

int handle_server()
{
    recv(sockfd, buffer, sizeof(buffer), 0);
    memcpy(&pkt, buffer, sizeof(pkt));

    return !(*server_handlers[pkt.type])(pkt);
}

void run()
{
    fd_set tmp_fds;
    int i;
    int done = 0;

    FD_ZERO(&tmp_fds);

    while (!done) {
        try_send();

        tmp_fds = read_fds;
        if (select(fd_max + 1, &tmp_fds, NULL, NULL, &tout) == -1)
            die();

        for (i = 0; i <= fd_max; ++i) {
            if (FD_ISSET(i, &tmp_fds)) {
                if (i == 0)
                    done = handle_console();
                else if (i == sockfd)
                    done = handle_server();
                else if (i == sockpas)
                    handle_send();
                else if (i == sockrecv)
                    handle_receive();
            }
        }
    }
    fclose(flog);
}

int main(int argc, char **argv)
{
    if (argc != 6) {
        perror("Usage ./client <name> <dir> <port> <ip_srv> <port_srv>.");
        exit(1);
    }

    struct sockaddr_in serv_addr = {};
    struct sockaddr_in cl_addr = {};

    int server_port = atoi(argv[5]);
    int client_port = atoi(argv[3]);

    // Parse arguments.
    strcpy(client_name, argv[1]);
    strcpy(local_dir, argv[2]);

    // Add trailing slash.
    if (local_dir[strlen(local_dir) - 1] != '/')
        strcat(local_dir, "/");

    sockpas = socket(AF_INET, SOCK_STREAM, 0);
    socksend = socket(AF_INET, SOCK_STREAM, 0);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    fd_max = max(sockpas, sockfd);

    // Common config.
    serv_addr.sin_family = cl_addr.sin_family = AF_INET;

    // Server config.
    serv_addr.sin_port = htons(server_port);
    inet_aton(argv[4], &serv_addr.sin_addr);

    // Client config.
    cl_addr.sin_port = htons(client_port);
    cl_addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(sockpas, (struct sockaddr *)&cl_addr, sizeof(cl_addr)) < 0)
        die();

    listen(sockpas, 1);
    FD_ZERO(&read_fds);
    FD_SET(0, &read_fds);
    FD_SET(sockfd, &read_fds);
    FD_SET(sockpas, &read_fds);

    if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
        die();

    // Connection message.
    msg init;
    strcpy(init.client, client_name);
    init.type = MT_CONNECT;
    init.port = client_port;
    console_pong(init, NULL, NULL);
    
    // Open logging.
    char logf[LEN_SMALL + 4];
    sprintf(logf, "%s.log", client_name);
    flog = fopen(logf, "w");
    
    run();
    
    close(sockfd);
    return 0;
}

