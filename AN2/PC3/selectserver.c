#include "helper.h"

/* Globals */
char buffer[LEN_NORMAL];
msg pkt;
filetype ft[100];
infoclient info[40];
fd_set read_fds;
int fti;
int fd_max;
int sockfd;
int k;

void remove_file(int l)
{
    if (l >= fti)
        return;

    for (; l < fti; l++)
        memcpy(&ft[l], &ft[l + 1], sizeof(ft[l]));
    --fti;
}

void remove_info(int l)
{
    if (l >= k)
        return;

    for (; l < k; l++)
        memcpy(&info[l], &info[l + 1], sizeof(info[l]));

    k--;
}

void format_bytes(long long bytes, char *buf)
{
    const char bytes_fmt[5][3] = {
        "B", "KB", "MB", "GB", "TB"
    };

    char fmt[LEN_NORMAL] = {};
    int k;

    for (k = 0; k < 4; ++k) {
        if (bytes < 1024)
            break;

        bytes /= 1024;
    }

    sprintf(fmt, "%s %d%s\n", buf, (int)bytes, bytes_fmt[k]);
    sprintf(buf, "%s", fmt);
}

int console_null(msg pkt)
{
    return 1;
}

int console_quit(msg pkt)
{
    int i;

    pkt.type = MT_DISCONNECT;
    strcpy(pkt.payload, "Server has quit connection.");
    memcpy(buffer, &pkt, sizeof(buffer));

    for (i = 0; i < k; i++) {
        send(info[i].socket, buffer, sizeof(buffer), 0);
        close(info[i].socket);
    }

    return 0;
}

int console_status(msg pkt)
{
    int i;
    if (k != 0)
        for (i = 0; i < k; i++) {
            printf("Client (%s) %i:\n", info[i].name, i);
            printf("- IP: %s\n", info[i].IP);
            printf("- Port: %d\n", info[i].port);
        }
    else
        printf("No active connections.\n");
    return 1;
}

int (*console_handlers[MT_ALL])(msg pkt) = {
    &console_null,              /* MT_CONNECT */
    &console_quit,              /* MT_QUIT */
    &console_null,              /* MT_LIST_CLIENTS */
    &console_null,              /* MT_INFO_CLIENT */
    &console_null,              /* MT_SHARE_FILE */
    &console_null,              /* MT_UNSHARE_FILE */
    &console_null,              /* MT_GET_SHARE */
    &console_null,              /* MT_INFO_FILE */
    &console_null,              /* MT_GET_FILE */
    &console_null,              /* MT_ERROR */
    &console_null,              /* MT_QUIT_CONNECTION */
    &console_null,              /* MT_TRANS_FILE */
    &console_status,            /* MT_STATUS */
    &console_null               /* MT_UNKOWN */
};

MsgType parse_console(char *msg)
{
    if (!strcmp(msg, "quit\n"))
        return MT_QUIT;
    if (!strcmp(msg, "status\n"))
        return MT_STATUS;
    return MT_UNKNOWN;
}

int handle_console()
{
    fgets(buffer, LEN_NORMAL, stdin);

    return !(*console_handlers[parse_console(buffer)])(pkt);
}

void handle_new_connection()
{
    struct sockaddr_in cli_addr;
    const int clilen = sizeof(cli_addr);
    time_t t = time(NULL);

    int newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, (socklen_t *)&clilen);
    if (newsockfd < 0)
        die();

    FD_SET(newsockfd, &read_fds);
    strcpy(info[k].IP, inet_ntoa(cli_addr.sin_addr));
    info[k].timestamp = t;
    info[k].socket = newsockfd;
    info[k++].port = ntohs(cli_addr.sin_port);

    fd_max = max(newsockfd, fd_max);
    printf("New client:\n");
    printf("- IP: %s\n", inet_ntoa(cli_addr.sin_addr));
    printf("- Port: %d\n", ntohs(cli_addr.sin_port));
    printf("- Socket: %d\n", newsockfd);
}

void client_null(int i, msg pkt)
{
}

void client_send(int i, msg pkt)
{
    memcpy(buffer, &pkt, sizeof(buffer));
    send(i, buffer, sizeof(buffer), 0);
}

void client_connect(int i, msg pkt)
{
    int ok = 0;
    int l;

    for (l = 0; l < k; l++) {
        if (strcmp(pkt.client, info[l].name) != 0)
            continue;

        FD_CLR(i, &read_fds);

        pkt.type = MT_DISCONNECT;
        strcpy(pkt.payload, "-4: Eroare la conectare.");
        client_send(i, pkt);
        ok = 1;

        remove_info(l);
    }

    if (ok == 0)
        for (l = 0; l < k; l++) {
            if (i != info[l].socket)
                continue;

            strcpy(info[l].name, pkt.client);
            info[l].port = pkt.port;
            break;
        }
}

void client_quit(int i, msg pkt)
{
    int l;

    FD_CLR(i, &read_fds);

    for (l = 0; l < k; l++) {
        if (i != info[l].socket)
            continue;
        break;
    }

    remove_info(l);
}

void client_list_clients(int i, msg pkt)
{
    char tmp[LEN_NORMAL] = {};
    int j;

    for (j = 0; j < k; ++j) {
        strcat(tmp, info[j].name);
        strcat(tmp, "\n");
    }

    strcpy(pkt.payload, tmp);
    client_send(i, pkt);
}

void client_info_client(int i, msg pkt)
{
    char tmp[LEN_NORMAL] = {};
    int ok = 0;
    int l;

    for (l = 0; l < k; l++) {
        if (strcmp(pkt.payload, info[l].name) != 0)
            continue;

        ok = 1;
        memcpy(tmp, &info[l], sizeof(tmp));
        break;
    }

    if (ok == 0) {
        strcpy(tmp, "-1: Client inexistent.");
        pkt.type = MT_ERROR;
    }

    memcpy(pkt.payload, tmp, sizeof(pkt.payload));
    client_send(i, pkt);
}

void client_share_file(int i, msg pkt)
{
    memcpy(&ft[fti], pkt.payload, sizeof(ft[fti]));
    ++fti;
}

void client_unshare_file(int i, msg pkt)
{
    char tmp[LEN_NORMAL] = {};
    int ok = 0;
    int l;

    printf("%s\n", pkt.client);
    for (l = 0; l < fti; l++) {
        printf("%s\n", ft[l].client_name);
        if (strcmp(pkt.payload, ft[l].file_name) != 0 ||
            strcmp(pkt.client, ft[l].client_name) != 0)
            continue;

        sprintf(tmp, "Unshared: (%s).", ft[l].file_name);
        ok = 1;

        remove_file(l);
        break;
    }

    if (ok == 0) {
        pkt.type = MT_ERROR;
        strcpy(tmp, "-2: Fisier inexistent.");
    }

    strcpy(pkt.payload, tmp);
    client_send(i, pkt);
}

void client_get_share(int i, msg pkt)
{
    char tmp[LEN_NORMAL] = {};
    int ok = 0;
    int l;

    for (l = 0; l < fti; l++) {
        if (strcmp(pkt.payload, ft[l].client_name) != 0)
            continue;

        strcat(tmp, ft[l].file_name);
        format_bytes(ft[l].size, tmp);
        ok = 1;
    }

    if (ok == 0)
        strcpy(tmp, "No shared files.\n");

    strcpy(pkt.payload, tmp);
    client_send(i, pkt);
}

void client_info_file(int i, msg pkt)
{
    char tmp[LEN_NORMAL] = {};
    int ok = 0;
    int l;

    for (l = 0; l < fti; l++) {
        if (strcmp(pkt.payload, ft[l].file_name) != 0)
            continue;

        strcat(tmp, ft[l].client_name);
        strcat(tmp, " ");
        strcat(tmp, ft[l].file_name);
        format_bytes(ft[l].size, tmp);
        ok = 1;
    }

    if (ok == 0) {
        strcpy(tmp, "-2: Fisier inexistent.");
        pkt.type = MT_ERROR;
    }

    strcpy(pkt.payload, tmp);
    client_send(i, pkt);
}

void client_get_file(int i, msg pkt)
{
    char tmp[LEN_NORMAL] = {};
    int ok = 0;
    int l;
    char *clname = strtok(pkt.payload, " ");
    char *filename = strtok(NULL, " ");

    for (l = 0; l < k; l++) {
        if (strcmp(clname, ft[l].client_name) != 0 ||
            strcmp(filename, ft[l].file_name) != 0)
            continue;

        strcat(tmp, ft[l].client_name);
        strcat(tmp, " ");
        strcat(tmp, ft[l].file_name);
        format_bytes(ft[l].size, tmp);
        ok = 1;

        pkt.type = MT_TRANS_FILE;
        break;
    }

    if (ok == 0) {
        strcpy(tmp, "Client not found.");
        pkt.type = MT_ERROR;
    }

    strcpy(pkt.payload, tmp);
    client_send(i, pkt);
}

void (*client_handlers[MT_ALL])(int i, msg pkt) = {
    &client_connect,            /* MT_CONNECT */
    &client_quit,               /* MT_QUIT */
    &client_list_clients,       /* MT_LIST_CLIENTS */
    &client_info_client,        /* MT_INFO_CLIENT */
    &client_share_file,         /* MT_SHARE_FILE */
    &client_unshare_file,       /* MT_UNSHARE_FILE */
    &client_get_share,          /* MT_GET_SHARE */
    &client_info_file,          /* MT_INFO_FILE */
    &client_get_file,           /* MT_GET_FILE */
    &client_null,               /* MT_ERROR */
    &client_null,               /* MT_QUIT_CONNECTION */
    &client_null,               /* MT_TRANS_FILE */
    &client_null,               /* MT_STATUS */
    &client_null                /* MT_UNKOWN */
};

void handle_client(int i)
{
    int n;

    memset(buffer, 0, LEN_NORMAL);
    n = recv(i, buffer, sizeof(buffer), 0);
    if (n > 0) {
        memcpy(&pkt, buffer, sizeof(pkt));
        (*client_handlers[pkt.type])(i, pkt);
    } else {
        if (n < 0)
            die();
        else
            printf("Socket %d disconnected.\n", i);

        FD_CLR(i, &read_fds); // scoatem din multimea de citire socketul pe care
        close(i);
    }
}

void run()
{
    fd_set tmp_fds;	//multime folosita temporar
    FD_ZERO(&tmp_fds);
    int done = 0;

    while (!done) {
		tmp_fds = read_fds;
		if (select(fd_max + 1, &tmp_fds, NULL, NULL, NULL) == -1)
			die();
		int i;
		for (i = 0; i <= fd_max; i++) {
			if (FD_ISSET(i, &tmp_fds)) {
				if (i == sockfd)
                    handle_new_connection();
				else if (i == 0)
                    done = handle_console();
                else
                    handle_client(i);
            }
        }
    }
}

int main(int argc, char **argv)
{
    if (argc != 2) {
        perror("Usage ./server <port>.");
        exit(1);
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in serv_addr = {};
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[1]));

    if (sockfd < 0)
        die();

    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr)) < 0)
        die();

    listen(sockfd, 5);

    FD_ZERO(&read_fds);
    FD_SET(sockfd, &read_fds);
    FD_SET(0, &read_fds);
    fd_max = sockfd;
    
    run();
    close(sockfd);
    
    return 0;
}


