#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lib.h"
#include "custom.h"

#define HOST    "127.0.0.1"
#define PORT    10000

#define PROG    "sender"
#define TIMEOUT 1000

// Returns a random value between 1 and 60.
int generate_size() {
    return rand () % 60 + 1;
}

void payload_to_frame(msg *t, frame *f) {
    // ACK contine numai seq_no si checksum.
    f->seq_no = t->payload[0];                  // 0
    f->checksum = t->payload[1];           // sz + 1
}

void frame_to_payload(frame *f, msg *t, int sz) {
    // memcpy t->payload, f nu va merge.
    // Va copia adresa sirului continut de f->payload.

    t->payload[0] = f->seq_no;                  // 0
    for (int i = 0; i < sz; ++i)
        t->payload[1 + i] = f->payload[i];      // 1 ... sz

    t->payload[1 + sz] = f->checksum;           // sz + 1

    t->len = 1 + 1 + sz;
}

void frame_init(frame *f, unsigned char seq_no, char buf[60], int size) {
    f->seq_no = seq_no;
    f->payload = malloc(size * sizeof(char));
    memcpy(f->payload, buf, size);
    f->checksum = checksum(f, size);
}

int main(int argc, char **argv) {
    msg t, *o;
    frame f;
    int size;
    unsigned char seq_no = 1;
    char buf[60];

    // Pentru o randomizare mai buna.
    srand(time(NULL));
    init(HOST, PORT);

    FILE *fil = fopen(argv[1], "rb");
    if (fil == NULL)
        perror("Fisier de intrare invalid.");

    for (;;) {
        // Daca am terminat de citit, am terminat de trimis.
        if (feof(fil)) {
            // Trimite pachet de sfarsit.
            f.checksum = f.seq_no = SEQ_NO_LAST;
            frame_to_payload(&f, &t, 0);
            log_message(&f, PROG, 0, 1, 0, -1);
            send_message(&t);

            // Asteapta ultimul ACK.
            recv_message(&t);
            payload_to_frame(&t, &f);
            log_message(&f, PROG, 0, 0, 1, -1);
            break;
        }

        // Generam o dimensiune intre 1 si 60.
        size = generate_size();

        // Setam dimensiunea la cat am citit, pentru cand sunt mai putine date.
        size = fread(buf, sizeof(char), size, fil) / sizeof(char);

        // Trimitem mesajul cat timp nu e trimis cum trebuie.
        do {
            // Initializam frame.
            frame_init(&f, seq_no, buf, size);

            // Cream un payload.
            frame_to_payload(&f, &t, size);

            do {
                // Punem in log actiunea si trimitem.
                log_message(&f, PROG, size, 1, 0, -1);

                send_message(&t);

                // Log mesaj de timeout.
                log_message(NULL, PROG, 0, 0, 0, 10);

                // Asteptam ACK si punem in log.
                o = receive_message_timeout(TIMEOUT);
            } while (o == NULL);

            free(f.payload);

            payload_to_frame(o, &f);
            log_message(&f, PROG, 0, 0, 1, -1);
        } while (f.seq_no != seq_no);

        ++seq_no;
    }

    fclose(fil);
    return 0;
}
