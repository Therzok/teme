#include <string.h>
#include <stdio.h>
#include <time.h>
#include "custom.h"

#define LOG_FILE "log.txt"

char checksum(frame *f, int sz) {
    char check = f->seq_no;
    for (int i = 0; i < sz; ++i)
        check ^= f->payload[i];
    return check;
}

void log_message(frame *f, char *p, int paysize, int send, int ack, int tout) {
    FILE *fil;
    struct tm *t;
    time_t start;
    char buf[22];

    time(&start);
    t = localtime(&start);
    fil = fopen(LOG_FILE, "a");

    strftime(buf, 22, "[%d-%m-%Y %H:%M:%S] ", t);

    fprintf(fil, "%s", buf);
    fprintf(fil, "[%s] ", p);

    if (tout != -1) {
        fprintf(fil, "Am pornit cronometrul (timeout este de %dms)\n", tout);
        fclose(fil);
        return;
    } else
        fprintf(fil, "Am %s urmatorul %s:\n",
                    send ? "trimis" : "primit",
                    ack ? "ACK" : "pachet");

    fprintf(fil, "Seq No: %u\n", f->seq_no);
    if (paysize)
        fprintf(fil, "Payload: %.*s\n", paysize, f->payload);
    fprintf(fil, "Checksum: %d%d%d%d%d%d%d%d\n",
                (f->checksum & 0x80) ? 1 : 0,   // Bit 8
                (f->checksum & 0x40) ? 1 : 0,   // Bit 7
                (f->checksum & 0x20) ? 1 : 0,   // Bit 6
                (f->checksum & 0x10) ? 1 : 0,   // Bit 5
                (f->checksum & 0x08) ? 1 : 0,   // Bit 4
                (f->checksum & 0x04) ? 1 : 0,   // Bit 3
                (f->checksum & 0x02) ? 1 : 0,   // Bit 2
                (f->checksum & 0x01) ? 1 : 0);  // Bit 1
    fprintf(fil, "---------------------------------------------------------\n");
    fclose(fil);
}

void log_printf(char *text)
{
    FILE *fil = fopen(LOG_FILE, "a");
    fprintf(fil, text);
    fclose(fil);
}

