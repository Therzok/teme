#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "lib.h"
#include "custom.h"

#define HOST "127.0.0.1"
#define PORT 10001

#define PROG "receiver"

int payload_to_frame(msg *t, frame *f) {
    int sz = t->len - 1 - 1;

    // memcpy f, t->payload nu va merge fiindca
    // f->payload nu este neaparat NUL terminated.

    f->seq_no = t->payload[0];                  // 0
    f->payload = malloc(sz * sizeof(char));
    memcpy(f->payload, t->payload + 1, sz);     // 1 ... sz
    f->checksum = t->payload[1 + sz];           // sz + 1

    return sz;
}

void frame_to_payload(frame *f, msg *t) {
    // ACK contine numai seq_no si checksum.
    t->payload[0] = f->seq_no;                  // 0
    t->payload[1] = f->checksum;                // 1

    t->len = 1 + 1;
}

void send_ack(msg *t, frame *o) {
    frame f;

    f.seq_no = o->seq_no;
    f.checksum = o->checksum;
    f.payload = NULL;

    log_message(&f, PROG, 0, 1, 1, -1);
    frame_to_payload(&f, t);
    send_message(t);
}

int main(int argc, char **argv) {
    msg r;
    frame f;
    int done = 0;
    int size;
    char check;
    int timeout_done = 0;
    init(HOST, PORT);

    FILE *fil = fopen(argv[1], "wb");
    if (fil == NULL)
        perror("Fisier iesire invalid.");

    while (!done) {
        // Asteptam pachet.
        recv_message(&r);
        size = payload_to_frame(&r, &f);
        log_message(&f, PROG, size, 0, 0, -1);

        // Verificam checksum. Scriem in fisier daca e corect.
        check = checksum(&f, size);
        if (f.checksum != check) {
            --f.seq_no;
            f.checksum = f.seq_no;
            log_printf("Eroare in checksum.\n");
        } else
            fwrite(f.payload, sizeof(char), size, fil);

        free(f.payload);

        done = f.seq_no == SEQ_NO_LAST;

        // Intarziem primul raspuns.
        if (!timeout_done) {
            timeout_done = 1;
            continue;
        }

        // Trimitem ack.
        send_ack(&r, &f);
    }

    fclose(fil);
    return 0;
}
