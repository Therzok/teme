#!/bin/bash

SPEED=1
DELAY=1
LOSS=0
CORRUPT=50
INPUT=input.txt
OUTPUT=output.txt

# Generate random 1kb file.
rm $INPUT $OUTPUT log.txt &> /dev/null
dd if=/dev/random of=$INPUT bs=1024 count=1 &> /dev/null

killall link 2> /dev/null
killall recv 2> /dev/null
killall send 2> /dev/null

./link_emulator/link speed=$SPEED delay=$DELAY loss=$LOSS corrupt=$CORRUPT &> /dev/null &
sleep 1
./recv $OUTPUT &
sleep 1

./send $INPUT
