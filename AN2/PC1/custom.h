#ifndef CUSTOM
#define CUSTOM

#define SEQ_NO_LAST 255

typedef struct {
	unsigned char seq_no;
	char *payload;
	char checksum;
} frame;

char checksum(frame *f, int sz);
void log_message(frame *f, char *p, int paysize, int send, int ack, int tout);
void log_printf(char *text);

#endif

