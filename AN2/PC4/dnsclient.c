#include <arpa/inet.h>
#include <ctype.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "dns_message.h"

// Array constants.
#define LEN_BUF         512
#define LEN_SHORT       256

// Configuration constants.
#define SERVER_MAX      64
#define CONF_FILE       "dns_servers.conf"
#define MESSAGE_FILE    "message.log"
#define LOG_FILE        "dns.log"
#define IGNORE_CHAR     '#'
#define DNS_PORT        53
#define QCLASS_INTERNET 1

// Variables.
char servers[SERVER_MAX][LEN_SHORT];
int servers_known = -1;
struct timeval timeout = { 5, 0 };
char *domain;
char *qname;
unsigned short qtype;
int used_server;
FILE *mlog;
FILE *flog;


// Q-handlers.
int qtype_handled(unsigned short type)
{
    return type == A || type == MX || type == NS || type == CNAME ||
    type == SOA || type == TXT || type == PTR;
}

const char *get_qtype_text(unsigned short type)
{
    switch (type) {
        case A:
            return "A";
        case MX:
            return "MX";
        case NS:
            return "NS";
        case CNAME:
            return "CNAME";
        case SOA:
            return "SOA";
        case TXT:
            return "TXT";
        case PTR:
            return "PTR";
        default:
            return "";
    }
}

void init_qtype(const char *text)
{
    if (!strcmp(text, "A"))
        qtype = A;
    else if (!strcmp(text, "MX"))
        qtype = MX;
    else if (!strcmp(text, "NS"))
        qtype = NS;
	else if (!strcmp(text, "CNAME"))
        qtype = CNAME;
	else if (!strcmp(text, "SOA"))
        qtype = SOA;
	else if (!strcmp(text, "TXT"))
        qtype = TXT;
	else if (!strcmp(text, "PTR"))
        qtype = PTR;
    else
        qtype = -1;
}

void init_qname(const char *text)
{
    char *copy;
    char *token;
    int pos = 0;
    int size;
    unsigned p1, p2, p3, p4;
    int is_ip = sscanf(text, "%3u.%3u.%3u.%3u", &p1, &p2, &p3, &p4) == 4;

    copy = malloc(strlen(text) + strlen(".IN-ADDR.ARPA") + 1);
    if (is_ip)
        sprintf(copy, "%u.%u.%u.%u.IN-ADDR.ARPA", p4, p3, p2, p1);
    else
        strcpy(copy, text);

    domain = malloc(strlen(text) + strlen(".IN-ADDR.ARPA") + 1);
    strcpy(domain, copy);

    qname = malloc(strlen(copy) + 2);

    for (token = strtok(copy, "."); token != NULL; token = strtok(NULL, ".")) {
        size = strlen(token);

        qname[pos++] = size;
        strcpy(qname + pos, token);
        pos += size;
    }

    free(copy);
}

void reverse_qname(unsigned char **ans, unsigned char *buf, int *count, unsigned char *name)
{
	unsigned int poz = 0;
	int jumped = 0;
	int i;
    unsigned char *ptr = *ans;

    *count = 1;

	while (*ptr != 0) {
		if (*ptr >= 192) {
			ptr = buf + (((*ptr) - 192) << 8) + *(ptr + 1) - 1;
			jumped = 1;
		} else
			name[poz++] = *ptr;

		++ptr;
        (*count) += !jumped;
	}

	name[poz] = '\0';
    (*count) += jumped;

	for (i = 0; i < strlen((const char *)name); name[i++] = '.') {
		poz = name[i];
        memmove(name + i, name + i + 1, poz);
        i += poz;
	}
	name[i] = '\0';

    *ans += *count;
}

// Handlers.
void handle_null(dns_rr_t *answer, unsigned char **ans, unsigned char *buf, int count)
{
}

void handle_address(dns_rr_t *answer, unsigned char **ans, unsigned char *buf, int count)
{
    unsigned short len = ntohs(answer->rdlength);
    unsigned char *ptr = *ans;
    unsigned short k;

    for (k = 0; k < len; ++k) {
        fprintf(flog, "%d", ptr[k] + (ptr[k] < 0 ? LEN_SHORT : 0));

        if (k < len - 1)
            fprintf(flog, ".");
    }
    fprintf(flog, "\n");

    *ans += 2 * count;
}

void handle_parse_qname(dns_rr_t *answer, unsigned char **ans, unsigned char *buf, int count)
{
    unsigned char name[LEN_SHORT];
    reverse_qname(ans, buf, &count, name);
    fprintf(flog, "%s\n", name);
}

// ntoh pe 32 biti.
int get_value(unsigned char **ans)
{
    unsigned short k;
    int val = 0;
    for (k = 0; k < 4; ++k) {
        val <<= 8;
        val |= *((*ans)++);
    }
    return val;
}

void handle_start_of_authority(dns_rr_t *answer, unsigned char **ans, unsigned char *buf, int count)
{
    unsigned char aname[LEN_SHORT];
    unsigned char amail[LEN_SHORT];
    reverse_qname(ans, buf, &count, aname);
    reverse_qname(ans, buf, &count, amail);
    fprintf(flog, "%s\t%s\t%d\t%d\t%d\t%d\t%d\n", aname, amail,
            get_value(ans), get_value(ans), get_value(ans), get_value(ans), get_value(ans));
    //      serial          refresh         retry           expiration      minimum
}

void handle_mail_exchange(dns_rr_t *answer, unsigned char **ans, unsigned char *buf, int count)
{
    unsigned char name[LEN_SHORT];
    short preference;
    preference = *(++(*ans));
    ++(*ans);

    reverse_qname(ans, buf, &count, name);
    fprintf(flog, "%d\t%s\n", preference, name);
}

void handle_text(dns_rr_t *answer, unsigned char **ans, unsigned char *buf, int count)
{
    unsigned short len = ntohs(answer->rdlength);
    unsigned char name[LEN_SHORT];

    unsigned short k;
    for (k = 0; k < len - 1; ++k)
        name[k] = (*ans)[k + 1];

    name[len - 1] = '\0';
    *ans += len;
    fprintf(flog, "%s\n", name);
}

void (*response_handlers[17])(dns_rr_t *, unsigned char **, unsigned char *, int) = {
    handle_null,
    handle_address,                 /* A */
    handle_parse_qname,             /* NS */
    handle_null,
    handle_null,
    handle_parse_qname,             /* CNAME */
    handle_start_of_authority,      /* SOA */
    handle_null,
    handle_null,
    handle_null,
    handle_null,
    handle_null,
    handle_parse_qname,             /* PTR */
    handle_null,
    handle_null,
    handle_mail_exchange,           /* MX */
    handle_text                     /* TXT */
};

void parse_answer(unsigned char *buf, int count, unsigned char **ans, char *section)
{
    unsigned char name[LEN_SHORT];
    dns_rr_t *answer;
    unsigned short type;
    unsigned short class;
    int i;
    int rcount;

    if (count <= 0)
        return;

    fprintf(flog, "\n;; %s SECTION:\n", section);

    for (i = 0; i < count; ++i) {
        memset(name, 0, sizeof(name));
        reverse_qname(ans, buf, &rcount, name);

        answer = (dns_rr_t*)*ans;
        *ans += sizeof(dns_rr_t);

        type = ntohs(answer->type);
        class = ntohs(answer->class);

        if (qtype_handled(type))
            fprintf(flog, "%s\t%s\t%s\t", name, class == 1 ? "IN" : "", get_qtype_text(type));

        *ans -= rcount;
        if (qtype_handled(type))
            response_handlers[type](answer, ans, buf, rcount);
    }
}

void parse(unsigned char *buf, char **argv)
{
    dns_header_t *header = (dns_header_t *)buf;
    int rr_answer_count = ntohs(header->ancount);
    int rr_nameserver_count = ntohs(header->nscount);
    int rr_additional_count = ntohs(header->arcount);

    unsigned char *ans = buf + sizeof(dns_header_t) + strlen(domain) + sizeof(dns_question_t) + 2;

    if (rr_answer_count > 0 || rr_nameserver_count > 0 || rr_additional_count > 0)
        fprintf(flog, "; %s - %s %s\n", servers[used_server], argv[1], argv[2]);

    parse_answer(buf, rr_answer_count, &ans, "ANSWER");
    parse_answer(buf, rr_nameserver_count, &ans, "AUTHORITY");
    parse_answer(buf, rr_additional_count, &ans, "ADDITIONAL");

    fprintf(flog, "\n\n");
}

// Common data for address and socket descriptor.
void init_vals(struct sockaddr_in *addr, int *fd, fd_set *read_fds)
{
    *fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (*fd < 0)
        perror("Socket descriptor failure.");

    addr->sin_family = AF_INET;
    addr->sin_port = htons(DNS_PORT);

    FD_ZERO(read_fds);
    FD_SET(*fd, read_fds);
}

// Initialize struct values.
void init_message(dns_header_t *header, dns_question_t *question)
{
    // PID should be unique per system.
    header->id = (unsigned short)htons(0);
    // One question.
    header->qdcount = htons(1);
    // Recursion.
    header->rd = 1;

    // Question type value.
    question->qtype = htons(qtype);

    // Question class value.
    question->qclass = htons(QCLASS_INTERNET);
}

// Try querying a server. If we succeed, then parse the response.
void query_servers(unsigned char *buf)
{
    struct sockaddr_in addr_serv;
    fd_set read_fds;
    socklen_t sz;
    int sfd;
    int i;

    int qname_offset = sizeof(dns_header_t);
    int question_offset = qname_offset + strlen(qname) + 1;
    int end_offset = question_offset + sizeof(dns_question_t);

    dns_header_t *header = (dns_header_t *)buf;
    dns_question_t *question = (dns_question_t *)(buf + question_offset);

    sz = sizeof(struct sockaddr_in);

    for (used_server = 0; used_server < servers_known; ++used_server) {
        // Reset local variables.
        memset(buf, 0, LEN_BUF);
        init_vals(&addr_serv, &sfd, &read_fds);

        // Set address to try on.
        inet_aton(servers[used_server], (struct in_addr *)&addr_serv.sin_addr.s_addr);

        // Init the message.
        init_message(header, question);
        memcpy(buf + qname_offset, qname, strlen(qname) + 1);

        for (i = 0; i < LEN_BUF; ++i)
            fprintf(mlog, "%02X ", buf[i]);
        fprintf(mlog, "\n");

        if (sendto(sfd, buf, end_offset, 0, (struct sockaddr *)&addr_serv, sz) < 0) {
            printf("Send failure");
            goto cleanup;
        }

        if (select(sfd + 1, &read_fds, NULL, NULL, &timeout) <= 0) {
            printf("Timeout or error on server (%s).\n", servers[used_server]);
            goto cleanup;
        }

        memset(buf, 0, LEN_BUF);
        if (recvfrom(sfd, buf, LEN_BUF, 0, (struct sockaddr*)&addr_serv, &sz) < 0) {
            printf("Response failure.\n");
            goto cleanup;
        }

        close(sfd);
        break;

    cleanup:
        close(sfd);
    }
}

// Configuration reading.
void read_servers()
{
    char buf[LEN_BUF];
    FILE *f = fopen(CONF_FILE, "r");

    while (fgets(buf, LEN_BUF, f) != NULL) {
        // Empty line.
        if (buf[0] == '\0')
            continue;
        // Ignore line.
        if (buf[0] == IGNORE_CHAR)
            continue;

        // Trim \n.
        buf[strlen(buf) - 1] = '\0';
        strcpy(servers[++servers_known], buf);
    }

    fclose(f);
}

int main(int argc, char **argv)
{
    unsigned char buf[LEN_BUF];

    if (argc != 3) {
        printf("Usage: ./dnsclient domain query_type");
        return 1;
    }

    // Initialize variables from arguments.
    init_qname(argv[1]); // domain
    init_qtype(argv[2]); // query-type

    // Open file descriptors.
    mlog = fopen(MESSAGE_FILE, "a");
    flog = fopen(LOG_FILE, "a");

    // Read config file.
    read_servers();

    // Try a query.
    query_servers(buf);

    // Parse response.
    parse(buf, argv);

    free(domain);
    free(qname);
    fclose(flog);
    fclose(mlog);
    return 0;
}

